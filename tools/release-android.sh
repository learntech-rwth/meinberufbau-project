#!/usr/bin/env bash

#export MODE=production
export HOST_URI=https://mbb.elearn.rwth-aachen.de/api/v1/

cp tools/signing/mbb.jks platforms/android/
cp tools/signing/release-signing.properties platforms/android/

cordova build android --release
rm mbb.apk
mv platforms/android/app/build/outputs/apk/release/app-release.apk mbb.apk
