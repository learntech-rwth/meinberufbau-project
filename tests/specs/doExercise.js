const find = require("../lib/PromiseSelector");
const $ = find.one;

const functions = require("./functions");

describe("do exercise", () => {
    jasmine.DEFAULT_TIMEOUT_INTERVAL = 30 * 1000;

    let baseUrl = null;
    beforeAll(async () => {
        if (browser.config.baseUrl.trim() !== "") {
            baseUrl = browser.config.baseUrl;
        } else {
            baseUrl = await browser.getUrl();
        }

        browser.setTimeout({
            implicit: 5000
        });
    });

    beforeEach(async function () {
        await browser.url(baseUrl);

        await browser.waitUntil(async () => {
            let element = $("#main-content");
            return await element.isDisplayed()
        });
        await $(".activated=Mauerwerksbau").click();
    });

    it("normal exercise", async function () {

        await functions.login(browser, "testuser@mbb.de", "123456789");

        await $(".exercise-tile-content=My ex").click();

        expect(await $("#article-name").getText()).toEqual("My ex");
        expect(await $(".exercise-description").getText()).toEqual("des");
        expect(await $(".exercise-image").getAttribute("src")).toEqual("https://upload.wikimedia.org/wikipedia/commons/3/36/Stadtpfarrkirche_Sankt_Peter.jpg");
        expect(await $(".exercise-image-description").getText()).toEqual("img");
        expect(await $(".exercise-understood-exercise-question").getText()).toEqual("Welchen Wert sollst du berechnen?");
        expect(await $("span=Länge der Mauer").isDisplayed()).toBeTruthy();
        expect(await $("span=Breite der Mauer").isDisplayed()).toBeTruthy();
        expect(await $("span=Anzahl der Steine").isDisplayed()).toBeTruthy();
        expect(await $("span=Höhe der Mauer").isDisplayed()).toBeTruthy();
        await $("span=Länge der Mauer").click();
        await $(".start-exercise-button").click();

        //Number section
        await $("[name=num-whole-stone]").setValue("1");
        await $("[name=num-three-quarter-stone]").setValue("0");
        await $("[name=num-half-stone]").setValue("6");
        await $("[name=num-quarter-stone]").setValue("5");
        await $("[name=num-stones-sideways]").setValue("2");
        await $("[name=num-joint]").setValue("14");

        await $("button=Überprüfen").click();
        expect(await $(".toast .message").getText()).toEqual("Mindestens eine Nummer ist falsch.");

        await $("[name=num-half-stone]").setValue("7");
        await $("button=Überprüfen").click();
        // await browser.pause(700);

        //Multiply-section
        await $("[name=multiplier-num-whole-stone]").setValue("1");
        await $("[name=length-whole-stone]").setValue("240");
        await $("[name=combined-length-whole-stone]").setValue("240");

        await $("[name=multiplier-num-half-stone]").setValue("7");
        await $("[name=length-half-stone]").setValue("115");
        await $("[name=combined-length-half-stone]").setValue("805");

        await $("[name=multiplier-num-quarter-stone]").setValue("5");
        await $("[name=length-quarter-stone]").setValue("52,5");
        await $("[name=combined-length-quarter-stone]").setValue("262,5");

        await $("[name=multiplier-num-stones-sideways]").setValue("2");
        await $("[name=length-stones-sideways]").setValue("115");
        await $("[name=combined-length-stones-sideways]").setValue("230");

        await $("[name=multiplier-num-joint]").setValue("14");
        await $("[name=length-joint]").setValue("10");
        await $("[name=combined-length-joint]").setValue("140");

        //Addition section
        await $("[name=combined-whole-stone-length]").setValue("240");
        await $("[name=combined-half-stone-length]").setValue("805");
        await $("[name=sum-half-stone]").setValue("1045");

        await $("[name=combined-quarter-stone-length]").setValue("262,5");
        await $("[name=sum-quarter-stone]").setValue("1307,5");

        await $("[name=combined-stones-sideways-length]").setValue("230");
        await $("[name=sum-stones-sideways]").setValue("1537,5");

        await $("[name=combined-joint-length]").setValue("140");
        await $("[name=sum-joint]").setValue("1677,5");

        await browser.pause(1000);
        expect(await $(".done-section h2").getText()).toEqual("Du hast die Aufgabe erfolgreich gelöst. Gut gemacht!");
    });
});