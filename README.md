# Mein Beruf Bau

## Setup
Es wird erwartet, dass Cordova, NPM und Node installiert sind, sowie eine
MySQL/Maria-DB existiert.

Folgende Befehle ausführen zum installieren:
> cordova prepare

> npm install 

Normalerweise führt `cordova prepare` auch `npm install` aus. Allerdings werden 
hier ein paar Packages gelöscht, welche per github-URL hinzugefügt werden.
Aus diesem Grund muss danach noch einmal `npm install` ausgeführt werden.

Sowohl der Server als auch der Client können durch Umgebungsvariablen 
angepasst werden. Um diese leicht setzen zu können, können diese in einer `.env`-
Datei gesetzt werden. Dazu einfach eine Datei mit dem Namen `.env` im Root
des Projektes anlegen (ja, nur `.env`, ohne Namen vor dem `.`). Dort
können dann zeilenweise Variablen angegeben werden. Ein Beispiel:
> MYSQL_USER=root  
> MYSQL_PASSWORD=123456

Während alle Variablen Default-Werte haben, sollten viele angepasst werden.
Für eine Übersicht für Variablen für Server und Client, siehe weiter unten in 
der jeweiligen Sektion. Wahrscheinlich muss der MySQL-Benutzer und das Passwort
angepasst werden, damit der Server überhaupt starten kann.

Der Server kann gestartet werden durch
> npm run server

Der Client kann mit einem der folgenden Befehle gestartet werden:
> npm run "run browser"

> cordova run browser
 
Das kompiliert den Client und startet diesen. Ist der Client gestartet, so kann
durch
> npm run "prepare browser"

oder
>cordova prepare browser

dieser erneut kompiliert werden, ohne dass der Webserver des Clients neu 
gestartet werden muss.

Für die Kompilierung und Ausführung auf einem Android-Gerät lauten 
die Befehle 
> npm run "run android"

oder 
> cordova run android --device
 
Falls kein Gerät angeschlossen ist, kann durch das weglassen von `--device` 
die App auch auf einem Emulator ausgeführt werden.

Außerdem kann dieses Projekt noch mit den Befehlen
> docker-compose build

und 
> docker-compose up 

als Docker-compose-Image kompiliert und gestartet werden. Hierbei wird 
lediglich die Webversion gestartet, nicht die Android-Version.

## Datenbank
Es wird `typeorm` als Object-relational-Mapping-Bibliothek eingesetzt. Diese 
wandelt Typescript-Klassen in entsprechende Datenbank-Tabellen um. Generell kann
hier die Datenbank im Hintergrund leicht gegen eine andere Datenbank ausgetauscht
werden. Dadurch kann auf Server-Seite eine MySQL-Datenbank und 
auf Client-Seite eine SQLite-DB genutzt werden. Während dabei bei iOS und Android
die nativen SQLite-DBs genutzt werden können, wird im Browser dieses durch 
SQLite.js bewerkstelligt.

Unter `src/shared/model/` sind die Models zu finden. Für das Aufsetzen der DB
gibt es Migrationens-Klassen, welche unter `src/shared/model/migrations/` liegen.
Nicht alle Migrations sind für Client und Server. Aus diesem Grund sind diese
noch einmal in drei Unterordner nuterteilt, nämlich Server, Client und Shared. 

Generell sind die Daten nur auf dem Server zuerst eingetragen. Der Client
lädt bei jedem Starten der App alle Änderungen herunter und speichert diese.
Da das für so ziemlich jedes Model gilt, ist das bereits in dem npm-modul
`cordova-sites-easy-sync` ausgelagert. 

## Server
### Variablen
- `PORT` (Default: 3000),
- `MYSQL_HOST` (Default: "localhost""),
- `MYSQL_PORT` (Default: "3306"),
- `MYSQL_USER` (Default: "root"),
- `MYSQL_PASSWORD` (Default: ""),
- `MYSQL_DATABASE` (Default: "mbb"),
- `JWT_SECRET` (Default: "mySecretioöqwe78034hjiodfu80ä^", wird zum Verschlüsseln des Javascript-Web-Token genutzt. Sollte für Produktions-Umgebung auf jedenfall geändert werden. Wird der Wert geändert, werden alle Token ungültig (Alle User werden ausgeloggt))
- `PEPPER` (Default: "mySecretPepper", wird zum Hashen der Passwörter verwendet. Sollte für Produktionsumgebung auf jedenfall geändert werden. Wird der Wert geändert, werden alle Passwörter ungültig!)


### Aufbau
Der Startcode des Servers ist in `src/server/server.ts`. Dieser ist komplett in
Typescript geschrieben. Als Backend wird express genutzt. Die Routes für Express
werden dabei in `src/server/routes.ts` definiert. 

Unter `src/server/logic/` sind dabei weitere Routen und die eigentliche Logik. 
Die Logik ist dabei in verschiedene Klassen aufgeteilt. Allerdings ist auch
nur die Logik definiert, die sich von dem Standard-Fall, dem Speichern und 
Synchronisieren unterscheidet. Auch ist der Login und die Registration bereits
dank dem Modul `cordova-sites-user-management` umgesetzt. 

## Client
### Variablen
Variablen werden generell aus der `.env`-Datei ausgelesen und über
WebPack dann in die Dateien eingefügt. Das bedeutet, dass diese Variablen
zur Kompilierung gesetzt werden müssen.

- `HOST` (Default: IP des Build-Computers, Adresse des Servers, inklusive http/https, aber ohne Port)
- `REQUEST_PORT` (Default: `PORT` (Falls `PORT` nicht gesetzt: 3000), Port auf dem der Client den Server versucht zu erreichen)
- `MODE` (Default: "development", Für Production auf "production" stellen)

### Aufbau
#### Seiten und Fragmente
Der Client ist anfangs in Javascript geschrieben und dann nach und nach in 
Typescript umgewandelt worden. Diese Umwandlung ist leider noch nicht
ganz fertig. 

Zusätzlich dazu wird ein Bundler, Webpack benutzt. Die Config-Datei
für diesen Bundler ist unter `webpack.config.js` zu finden.

Generell ist die App in verschiedene Seiten aufgeteilt. Diese Seiten finden
sich in `src/client/js/Sites`. Jede Seite hat außerdem eine entsprechende
HTML-Datei, welche in der Klasse selber importiert wird. Diese finden sich
unter `src/client/html/sites`. Generell können Event-Listener nicht direkt
im HTML-Code angefügt werden. Diese müssen in der entsprechenden Klasse
mit per `addEventListener`-Methode des HTML-Elements hinzugefügt werden.

Eine Seite kann folgende Funktionen überschreiben:
- `onConstruct(args)`
- `onViewLoaded()`
- `onStart(args)`
- `onPause()`
- `onDestroy()`

Diese werden auch in dieser Reihenfolge aufgerufen. `OnConstruct(args)` wird 
aufgerufen, sobald die entsprechende Klasse erstellt wird. `onViewLoaded()` wird
ausgeführt, sobald `onConstruct` beendet ist und zusätzlich die View, die 
entsprechende HTML-Datei geladen wurde. Dadurch ist `onViewLoaded` in der Lage,
die geladene HTML-Datei zu manipulieren, bevor diese dem Benutzer angezeigt wird.

`onStart` wird ausgeführt, sobald die Seite dann auch dem Benutzer angezeigt
wird, während `onPause` ausgeführt wird, sobald die Seite nicht mehr angezeigt
wird. Da immer nur eine Seite gleichzeitig angezeigt werden kann, wird `onPause`
auch aufgerufen, wenn eine neue Seite gestartet wird, ohne dass die alte Seite 
beendet wird. `onPause` kann einen Return-Wert zurückgeben. Dieser wird dann
an `onStart` beim nächsten Aufruf weitergegeben. 

Außerdem ist es möglich, ein Dictionary als Start-Parameter an eine Seite 
mitzugeben. Dieses wird als Parameter an `onConstruct` mitgegeben. Zusätzlich
dazu gibt es Deep-Links. Diese können hinzugefügt werden, wie es bei 
`src/client/js/Sites/ShowExerciseSite.ts` am Ende der Datei durch 

> App.addInitialization((app) => {  
>    app.addDeepLink("exercise", ShowExerciseSite);  
> });  

gemacht wird. Das sagt im Grunde genommen aus, dass wenn die App mit dem 
Query-Parameter `s=exercise` gestartet wird, diese Seite gestartet werden soll.
Die Start-Parameter können dann ebenfalls per Query-Parameter übergeben werden.

Um auf Elemente einer View innerhalb der Seite zugreifen zu können, soll die 
Methode `findBy(query, all?, asPromise?)` genutzt werden. Durch query muss
ein CSS-Selektor angegeben werden. All und asPromise sind optionale Parameter,
welche beide anfangs auf false gestellt sind.
Mit all auf true, wird ein Array mit allen Elementen zurückgegeben. Mit 
as Promise auf true wird ein Promise zurückgegeben, welches dann das oder die 
Elemente zurückgibt. Will man bereits in `onConstruct()` auf ein Element
zugreifen, muss man hier das als Promise benutzen. Man achte aber darauf, dass
nicht auf das Abschluss des Promises gewartet wird, da sonst ein Deadlock 
entstehen kann. In den anderen vier Methoden kann `findBy` ohne promise
genutz werden, um direkt ein Element zu erhalten. Der Vorteil zu
`document.querySelector` ist, dass `findBy auch funktioniert, wenn die Seite
noch nicht oder nicht mehr angezeigt wird.
 
Neben den Seiten gibt es auch Fragmente. Diese haben wie Seiten auch eine View
und die genannten Funktionen. Anders als Seiten müssen diese allerdings
einer Seite hinzugefügt werden. Dafür hat die Seite die Funktion
`addFragment(viewQuery, fragment)`, welche einen CSS-Selector als ViewQuery und
ein Fragment übergeben bekommt. Die View des Fragments wird dann an das Element
hinter dem ViewQuery hinzugefügt. 

Durch Fragmente können Funktionalitäten, die häufiger gebraucht werden, nur 
einmal programmiert und mehrmals verwendet werden.

#### App-Klasse
Eine spezielle Klasse ist die App-Klasse. Es sollte nur eine Instanz in der
kompletten App geben. In diesem Fall ist diese am Ende von der Datei 
`src/client/js/script.js` zu finden. Diese Instanz hat die Funktion
`start(StartSeite)`. Diese Funktion merkt die Seite, welche
über den Deep-Link angegeben wurde als Startseite vor. Falls keine angegeben 
wurde, wird die übergebene Seite als Startseite gemerkt. 

Die Funktion `ready(listener)` führt den Listener aus, sobald die App
gestartet wurde. Bevor eine App allerdings gestartet wird, werden alle 
Initialisierungen abgeschlossen. Um eine Initialisierung anzugeben, wird die 
statische Methode `App.addInitialization(listener)` ausgeführt. Der Listener
kann ein Promise zurückgeben und erst wenn alle listeners durchlaufen sind,
wird die Startseite gestartet. Im Normalfall werden über diese Listeners 
Deep-Links hinzugefügt. Außerdem sind in `src/client/js/script.js` durch
diese Methode ein paar Initialisierungen getätigt, wie z.B. der Translator,
die default-Navigation und das Synchronisieren von Models.

#### Navigation
Die Navigation ist in zwei unterschiedliche Bereiche aufgeteilt. Einmal 
in die Footer- und einmal in die Header-Navigation. Für die Footer-Navigation
ist das Fragment `src/client/js/Fragments/FooterFragment.ts` zuständig. 
Dieses ist leicht verständlich, da die Footer-Navigation nicht aufwändig ist.

Die Header-Navigation ist hingegen schon ein wenig aufwendiger. Deswegen
wird hier auf das Menü von dem npm-modul `cordova-sites` zurückgegriffen. 
Die einzelnen Punkte sind MenuActions. Jede MenuAction hat einen Titel,
einen Listener, eine Order und eine show-class (CSS-Klasse). 

Die Menüpunkte werden der Order nach sortiert und im Header angezeigt.
Dabei gibt es neben dem normalen Header-Menü noch ein aufklappbares 
Burger-Menu. Durch die show-classe wird angegeben, wann die Elemente
aus der Header-Leiste in das Burger-Menü verschoben werden. Dazu werden die 
Breakpoint-CSS-Klassen von Foundation genutzt. 

Wird auf eine MenuAction geklickt, so wird der angegebene Listener ausgeführt.

Die MenuActions werden durch ein NavbarFragment aus `cordova-sites` verwaltet.
Da es viele Actions gibt, welche in allen Seiten vorkommen, gibt es hier
die Möglichkeit, Standardactions zu definieren. Das wird in der 
Initialisierung in `src/client/js/script.js` getan. 

Zusätzlich dazu kann jede Seite, die die Klasse `MenuSite` aus `cordova-sites`
ableitet (also alle Seiten in diesem Projekt) auch die Funktion 
`onCreateMenu(menu)` überladen. Dem übergebenen Menü können weitere MenuActions
hinzugefügt werden, um so ein Seitenspezifisches Menü zu erstellen.

#### Translation
Unter `src/client/translations/` liegen json-Dateien mit Übersetzungen.
Diese werden in `src/client/js/script.js` inkludiert und dem Translator 
übergeben. 

Der Translator stammt aus `cordova-sites` und bietet ein paar interessante
Methoden. Zum einen `translate`, worüber ein einfacher String übersetzt werden
kann. Zum anderen `makePersistentTranslation`, wo auch ein String übergeben 
und übersetzt wird. Allerdings wird ein HTML-Element zurückgegeben, welches
diesen String hat. Wird das Element dem DOM hinzugefügt und dann die Sprache
gewechselt, ändert sich auch die Übersetzung in dem Element. 

Durch `addTranslationCallback` kann ein Listener hinzugefügt werden, der
immer aufgerufen wird, wenn die Übersetzungen geupdatet werden, also wenn
z.B. die Sprache geändert wird. 

Durch die Funktion `updateTranslation(baseElement?)` können alle Übersetzungen
innerhalb eines HTML-Elementes erneut übersetzt werden. Wird kein baseElement
angegeben, werden alle Übersetzungen im DOM ersetzt.  
Diese Funktion wird genutzt, wenn eine View angezeigt wird, um die Elemente
zu übersetzen. Dank dieser Funktion können Übersetzungen auch direkt in HTML
hinzugefügt werden. So wird der Text jedes Elements mit der Klasse `translation`
übersetzt. 

Der Translator hat eine "fallback"-Language. Ist eine Übersetzung in einer 
Sprache nicht vorhanden, wird auf diese fallback-Sprache zurückgegriffen.
Ist die Übersetzung auch dort nicht vorhanden, wird einfach der zu übersetzende
Begriff zurückgegeben. 

Außerdem ist es möglich, ein Array mit Text als Parameter für die Übersetzung
anzugeben. In der Übersezung selber müssen die Parameter mit {0}, {1}, bzw
{<array-index>} gekennzeichnet sein. So wird z.b. "Deine Punktzahl ist {0}" 
zu "Deine Punktzahl ist 512" übersetzt, falls 512 der erste Parameter im Array
ist. 

Auch in HTML können Parameter angegeben werden. Dafür muss das Parameter-Array in 
JSON-Notation in dem Attribut data-translation-args angegeben werden.

Groß- und Kleinschreibung in den Keys für die Translation ist egal. Außerdem
werden fehlende Übersetzungen mit >>key<< markiert. Das wird, wenn die Variabe
`MODE` auf `production` gestellt wird, auch ausgeschaltet.

#### Synchronisation
In `src/client/js/script.js` werden die unterschiedlichen Models synchronisiert.
Dabei passiert das blockierend, wenn noch keine Daten vorhanden sind. Ansonsten
werden die Models im Hintergrund heruntergeladen. Für dieses Feature wird die
Klasse `SyncJob` genutzt und es werden auch nur veränderte Models 
heruntergeladen. Da es sein kann, dass offline ein Fortschritt entsteht. Dieser
wird auch hier hochgeladen, falls dieser noch nicht auf dem Server gespeichert 
ist.

#### User-Management

Neben den normalen MenuActions gibt es auch noch Actions aus
`cordova-sites-user-management`. Dieses Modul führt ein ACL ein. Man kann
nun auch Actions erstellen, welche nur angezeigt werden, wenn der Benutzer eine
gewisse Rolle hat. Über diese Funktion werden Login und Logout-Menüpunkte 
angezeigt. 

Außerdem gibt es hier auch Seiten, mit denen man leicht überprüfen kann, ob
ein Benutzer die benötigten Rechte hat, um die Seiten aufzurufen. 

#### Storybook
Die Nodes sind im Normalfall in diesem Repository definiert. Diese werden in
der Datei `src/client/js/script.js` hinzugefügt und im Ordner 
`src/client/js/Storybook` definiert. Zusätzlich dazu wird in 
`src/client/js/script.js` ebenfalls die Sprechgeschwindigkeit für Audios
gesetzt. Dafür wird dort ein Listener gesetzt, welcher für jeden 
Zeit-Update des Audio-Elements aufgerufen wird. 

Es gibt außer den gesetzten Nodes noch weitere vordefinierte. Wenn der selbe
Key genutzt wird, werden diese überschrieben. Allerdings gibt es noch weitere,
welche nicht benutzt werden sollen. In der Seite `EditStorybookSite` werden
die Menüeintrage aus dem Gamebook-Editor-Menü entfernt, am Ende der 
`onViewLoaded`-Funktion. Da die Menüeinträge entfernt werden, können die 
entsprechenden Nodes nicht mehr hinzugefügt werden.

Um den Editor zu bearbeiten, muss das Storybook-Projekt angepasst werden.
In dem Projekt gibt es im Ordner `src/editor` die Klassen des Editors. 
Interessant dürften die Klassen `GBEditor` und `GBAttributeEditor` sein.

Wichtig ist zu erwähnen, dass der Storybook-Ediotr alles HTML im Code selbst
definiert und nicht auf vorgefertigte HTML-Dateien zurückgreift.
