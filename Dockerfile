# Use an official Python runtime as a parent image
FROM node:14

# Creating www file and setting workdir to /app
WORKDIR /app/www
#WORKDIR /app/platforms
#WORKDIR /app/plugins
WORKDIR /app

#Set request uri
ENV HOST_URI .
ENV MODE production
#ENV PORT 3050
#ENV HOST "./"
ENV JWT_SECRET gj03480fn2ßrv94nc0y8043j

# Install global dependencies
RUN \
    apt update && apt upgrade -y; \
    apt install git -y; \
    apt install mysql-client -y; \
    npm install -g cordova@9.0.0

# Install waiting script for mysql
ADD https://github.com/ufoscout/docker-compose-wait/releases/download/2.5.0/wait /wait
RUN chmod +x /wait

COPY package.json config.xml package-lock.json /app/
RUN mkdir -p /app/www/

RUN npm run "prepare browser";
RUN npm cache clear --force
RUN npm install;


# Copy the current directory contents into the container at /app
COPY . /app

RUN \
    npm run "prepare browser"; \
    rm -rf /app/server/public; \
    mv /app/platforms/browser/www /app/src/server/public

# Make port 3050 available to the world outside this container
EXPOSE 3050

# Run app after waiting on mysql when the container launches
CMD /wait && npm run server
