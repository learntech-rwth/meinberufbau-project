import {GBRoomNode} from "ba-mbb-digitalstorybook/dist/common/Node/GBRoomNode";
import {GBLang} from "ba-mbb-digitalstorybook/dist/language/GBLang";
import {NodeHelper} from "./NodeHelper";
import {Translator} from "cordova-sites";

export class FormulaNode extends GBRoomNode {

    constructor() {
        super();
        this.AddAttribute("roomSimpleLanguage", {
            label: GBLang.tl("room simple language", true) as string,
            type: "string",
            textarea: true
        });
        this.AddAttribute("formula", {
            type: "string",
            label: GBLang.tl("formula", true) as string,
        });
        this.AddAttribute("hideFormulaLabels", {
            type: "boolean",
            label: GBLang.tl("hide formula labels", true) as string,
        });
        this.AddAttribute("values", {
            label: GBLang.tl("values", true) as string,
            type: "list",
            listType: "fields",
            fields: [
                {"label": "Value"},
                {"label": "Value Label"},
                {
                    "label": "Is Number",
                    "checkbox": true,
                },
            ]
        });

        this.AddOutput("Richtig");
        // this.AddOutput("Falsch");

        this.On("values", values => this.update(values));
    }

    Render(select: { (output: number): void }, scope: { [p: string]: any }, renderTarget: HTMLElement): void {

        renderTarget.innerText = "";
        NodeHelper.addRoom(this, renderTarget, scope)

        const formula = this.GetAttribute("formula").trim();
        const values = this.GetAttribute("values");

        const formulaHtmlString = "<td>" + formula.replace(/\[([0-9]+)]/g, (string, index) => {
            return "</td><td>" + values[index][1] + "</td><td>";
        }) + "</td>";
        const formulaInputHtmlString = "<td>" + formula.replace(/\[([0-9]+)]/g, (string, index) => {
            const inputElement = document.createElement("input");
            inputElement.dataset["result"] = values[index][0].trim();
            inputElement.dataset["index"] = index;
            if (values[index][2] === true) {
                inputElement.type = "number";
            } else {
                inputElement.type = "text";
            }
            return "</td><td>" + inputElement.outerHTML + "</td><td>";
        }) + "</td>";

        const formulaTableRow = document.createElement("tr");
        formulaTableRow.innerHTML = formulaHtmlString;

        const formulaInputTableRow = document.createElement("tr");
        formulaInputTableRow.innerHTML = formulaInputHtmlString;

        const formulaTable = document.createElement("table");
        formulaTable.appendChild(formulaInputTableRow);
        if (!this.GetAttribute("hideFormulaLabels")){
            formulaTable.appendChild(formulaTableRow);
        }
        formulaTable.classList.add("formula-table");

        const button = document.createElement("button");
        button.appendChild(Translator.makePersistentTranslation("continue"));

        button.addEventListener("click", () => {
            const errors = [];
            formulaInputTableRow.querySelectorAll("input").forEach(input => {
                if (input.dataset["result"].trim() !== input.value.trim()) {
                    errors.push(input.dataset["index"]);
                }
            });

            if (errors.length === 0) {
                select(0);
            } else {
                errors.sort();
                select(parseInt(errors[0])+1);
            }
        })

        renderTarget.appendChild(formulaTable);
        renderTarget.appendChild(button);
    }

    private update(values: any) {
        let diff = values.length+1 - this.ListAttributeLength("outputTargets");
        if (diff > 0) {
            while (diff-- > 0) {
                this.AddOutput();
            }
        } else {
            while (diff++ < 0) {
                this.RemoveLastOutput();
            }
        }
        const names = ["Richtig"];
        names.push(...values.map((val, i) => ((false && Array.isArray(val) && val.length >= 2 && val[1])?val[1]:i)+" falsch"));
        this.SetAttribute("outputTitles", names);
    }
}
