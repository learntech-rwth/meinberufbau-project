import {GBCustomNode} from "ba-mbb-digitalstorybook/dist/common/Node/Templates/GBCustomNode";
import {NodeHelper} from "./NodeHelper";
import {GBRoomNode} from "ba-mbb-digitalstorybook/dist/common/Node/GBRoomNode";
import {GBLang} from "ba-mbb-digitalstorybook/dist/language/GBLang";

export class CustomNode extends GBRoomNode {

    constructor() {
        super();
        this.AddAttribute("roomSimpleLanguage", {
            label: GBLang.tl("room simple language", true) as string,
            type: "string",
            textarea: true
        });
        this.AddAttribute("inputs", {
            label: GBLang.tl("inputTypes", true) as string,
            type: "list",
            listType: "select",
            selectChoices: ["String", "Number", "Boolean"]
        });
        this.AddAttribute("labels", {
            label: GBLang.tl("inputLabels", true) as string,
            type: "list",
            listType: "fields",
            fields: [
                {"label": "Label"},
                {"label": "Label - easy speech"},
            ]
        });
        this.AddAttribute("submit", {
            label: GBLang.tl("submitText", true) as string,
            type: "string"
        });
        this.AddOutput("Output");
    }

    Render(
        select: (output: number) => void,
        scope: { [key: string]: any },
        renderTarget: HTMLElement
    ): void {
        renderTarget.innerText = "";
        NodeHelper.addRoom(this, renderTarget, scope)

        let inputs = this.GetAttribute("inputs") as string[];
        let labels = this.GetAttribute("labels") as string[];
        let ans: { [key: number]: any } = {};
        inputs.forEach((type, index) => {
            let input = document.createElement("input");
            if (type == "Number") {
                input.type = "number";
                input.value = "0";
                ans[index] = 0;
            } else if (type == "Boolean") {
                input.type = "checkbox";
                ans[index] = false;
            } else {
                input.type = "text";
                ans[index] = "";
            }
            let label = document.createElement("label");
            if (labels[index] !== undefined) {

                let labelText = labels[index];
                let simpleSpeechLabelText = null;
                if (Array.isArray(labelText)){
                    simpleSpeechLabelText = labelText[1];
                    labelText = labelText[0];
                }

                const text = document.createElement("span");
                text.appendChild(NodeHelper.createSimpleTranslation(labelText, simpleSpeechLabelText, scope));
                text.classList.add("textToSpeech")
                if (type == "Boolean") {
                    label.appendChild(input);
                    label.appendChild(document.createTextNode(" "));
                    label.appendChild(text);
                } else {
                    label.appendChild(text)
                    label.appendChild(document.createTextNode(": "));
                    label.appendChild(input);
                }
            } else {
                label.appendChild(input);
            }
            input.addEventListener("change", () => {
                if (type == "Boolean") {
                    ans[index] = input.checked;
                } else {
                    ans[index] = input.value;
                }
            });
            renderTarget.appendChild(label);
        });
        let submit = document.createElement("button");
        const buttonText = document.createElement("span");
        buttonText.innerHTML = this.GetAttribute("submit");
        buttonText.classList.add("textToSpeech")
        submit.appendChild(buttonText);
        submit.addEventListener("click", () => {
            scope.ans = ans;
            select(0);
        });
        renderTarget.appendChild(submit);
    }
}
