import {GBRoomNode} from "ba-mbb-digitalstorybook/dist/common/Node/GBRoomNode";
import {Translator} from "cordova-sites";
import {GBLang} from "ba-mbb-digitalstorybook/dist/language/GBLang";
import {NodeHelper} from "./NodeHelper";

export class NumbersNode extends GBRoomNode {

    constructor() {
        super();
        this.AddAttribute("roomSimpleLanguage", {
            label: GBLang.tl("room simple language", true) as string,
            type: "string",
            textarea: true
        });

        this.AddAttribute("Values", {
            label: GBLang.tl("values", true) as string,
            type: "list",
            listType: "fields",
            fields: [
                {"label": "Before Value"},
                {"label": "Before Value - easy speech"},
                {
                    "label": "Value",
                    // "type": "number",
                },
                {"label": "After Value"},
                {"label": "After Value - easy speech"},
            ]
        });
        this.AddOutput("Wahr");
        this.AddOutput("Falsch");
    }

    Render(select: { (output: number): void }, scope: { [p: string]: any }, renderTarget: HTMLElement): void {
        renderTarget.innerText = "";
        NodeHelper.addRoom(this, renderTarget, scope);

        const numbersContainer = document.createElement("div");

        let valueDefinitions = <string[][]>this.GetAttribute("Values");
        valueDefinitions.forEach(valueDefinition => {

            const simpleLanguageElements = [];

            if (valueDefinition.length === 5) {
                simpleLanguageElements.push(valueDefinition[1])
                simpleLanguageElements.push(valueDefinition[4]);
                valueDefinition = [
                    valueDefinition[0],
                    valueDefinition[2],
                    valueDefinition[3]
                ]
            }

            const valueRow = document.createElement("div");
            const beforeElement = document.createElement("span");
            beforeElement.appendChild(NodeHelper.createSimpleTranslation(valueDefinition[0], simpleLanguageElements[0], scope));
            beforeElement.classList.add("textToSpeech")

            const inputElement = document.createElement("input");
            inputElement.dataset["expected"] = valueDefinition[1];

            const afterElement = document.createElement("span");
            afterElement.classList.add("textToSpeech")
            afterElement.appendChild(NodeHelper.createSimpleTranslation(valueDefinition[2], simpleLanguageElements[1], scope))

            valueRow.appendChild(beforeElement);
            valueRow.appendChild(inputElement);
            valueRow.appendChild(afterElement);
            numbersContainer.appendChild(valueRow);
        })

        renderTarget.appendChild(numbersContainer);

        const continueButton = document.createElement("button");
        continueButton.classList.add("textToSpeech")
        continueButton.appendChild(GBLang.tl("continue") as Element);

        const inputs = numbersContainer.querySelectorAll("input");
        inputs.forEach(input => {
            input.addEventListener("input", () => {
                let allHaveValue = true;
                inputs.forEach(i => {
                    if (i.value.trim() === "") {
                        allHaveValue = false
                    }
                });
                continueButton.disabled = !allHaveValue;
            })
        })

        let allHaveValue = true;
        inputs.forEach(i => {
            if (i.value.trim() === "") {
                allHaveValue = false
            }
        });
        continueButton.disabled = !allHaveValue;

        continueButton.addEventListener("click", () => {
            let allValid = true;
            inputs.forEach(input => {
                if (input.value !== input.dataset["expected"]) {
                    allValid = false;
                }
            });
            if (allValid) {
                select(0);
            } else {
                select(1);
            }
        });
        renderTarget.appendChild(continueButton);

    }
}