import {GBLang} from "ba-mbb-digitalstorybook/dist/language/GBLang";
import {GBNode} from "ba-mbb-digitalstorybook/dist/common/Node/GBNode";
import {NodeHelper} from "./NodeHelper";
import {Translator} from "cordova-sites";
import {GBRoomNode} from "ba-mbb-digitalstorybook/dist/common/Node/GBRoomNode";
import {Helper} from "js-helper/dist/shared";
import {JsonHelper} from "js-helper/dist/shared/JsonHelper";
import {StorybookFragment} from "../../Fragments/ExerciseFragments/StorybookFragment";

export class ArbeitsplanNode extends GBRoomNode {

    constructor() {
        super();
        this.AddAttribute("roomSimpleLanguage", {
            label: GBLang.tl("room simple language", true) as string,
            type: "string",
            textarea: true
        });
        //this.AddAttribute("isMaterialliste", {
        //    type: "boolean",
        //    label: GBLang.tl("Als Materialliste", true) as string
        //});
        this.AddAttribute("entries", {
            label: GBLang.tl("Einträge", true) as string,
            type: "list",
            listType: "fields",
            fields: [
                {"label": "Eintrag"},
                {"label": "Eintrag - einfache Sprache"},
                {"label": "Wert (HH:MM oder in KG)"},
            ]
        });

        this.AddOutput("Richtig");
        this.AddOutput("Falsch");
    }

    Render(select: (output: number) => void, scope: { [p: string]: any }, renderTarget: HTMLElement) {
        renderTarget.innerText = "";
        NodeHelper.addRoom(this, renderTarget, scope)

        const decisions = this.GetAttribute("entries").map(e => e[0]);

        //TODO SimpleLanguage einbauen
        const decisionsSL = this.GetAttribute("entries").map(e => e[1]);

        const values = this.GetAttribute("entries").map(e => e[2]);

        const isMaterialliste = this.GetAttribute("isMaterialliste");

        const stepContainer = document.createElement("div");
        const stepTemplate = document.createElement("div");
        const stepNumber = document.createElement("span");
        const stepNameSelect = document.createElement("select");

        const stepValueInputContainer = document.createElement("span");
        const stepValueSubtractButton = document.createElement("button");
        const stepValueAddButton = document.createElement("button");
        const stepValueInput = document.createElement("input");
        const stepOpenButton = document.createElement("span");
        const contentContainer = document.createElement("div");

        stepValueSubtractButton.innerText = "-";
        stepValueInput.value = isMaterialliste ? "0,00" : "00:00";
        stepValueAddButton.innerText = "+";

        stepValueInputContainer.appendChild(stepValueSubtractButton);
        stepValueInputContainer.appendChild(stepValueInput);
        stepValueInputContainer.appendChild(stepValueAddButton);

        contentContainer.appendChild(stepNameSelect)
        contentContainer.appendChild(stepValueInputContainer);

        stepTemplate.appendChild(stepNumber);
        stepTemplate.appendChild(contentContainer);
        stepTemplate.appendChild(stepOpenButton);

        stepValueAddButton.classList.add("add-button");
        stepValueSubtractButton.classList.add("subtract-button");

        contentContainer.classList.add("content-container");
        stepTemplate.classList.add("plan-step")
        stepNumber.classList.add("index-number");
        stepTemplate.classList.add("closed");
        stepOpenButton.classList.add("open-button");
        stepValueInputContainer.classList.add("value-input-container")

        const defaultOption = document.createElement("option");
        defaultOption.value = "-";
        defaultOption.innerText = isMaterialliste ? "Material" : "Arbeitsschritt";
        stepNameSelect.appendChild(defaultOption);

        //Sort alphabetically
        const randomizedDecisions = JsonHelper.deepCopy(decisions).sort();

        randomizedDecisions.forEach(randomizedOption => {
            const option = document.createElement("option");
            option.value = randomizedOption;
            option.innerText = randomizedOption;
            stepNameSelect.appendChild(option);
        })

        // const selectOptions

        decisions.forEach((decision, index) => {
            let previousValue = null;

            const elem = <HTMLElement>stepTemplate.cloneNode(true);
            (<HTMLElement>elem.querySelector(".index-number")).innerText = (index + 1);
            elem.addEventListener("click", (e) => {
                if (e.target === elem || e.target === elem.querySelector(".open-button") || elem.classList.contains("closed")) {
                    elem.classList.toggle("closed")
                }
            });
            const select: HTMLSelectElement = elem.querySelector("select");
            select.addEventListener("change", () => {
                if (previousValue) {
                    stepContainer.querySelectorAll("option[value='" + previousValue + "']").forEach(elem => elem.classList.remove("hidden"));
                }

                if (select.value !== "-") {
                    previousValue = select.value;
                    stepContainer.querySelectorAll("option[value='" + previousValue + "']").forEach(elem => {
                        if (elem.parentNode !== select) {
                            elem.classList.add("hidden")
                        } else {
                            //Setzen der Attribute, damit es auch im Plan so bleibt
                            select.querySelectorAll("option").forEach(elem => elem.removeAttribute("selected"));
                            elem.setAttribute("selected", "selected");
                        }
                    });
                } else {
                    previousValue = null;
                }
            });

            const modifyMinutes = (minutesString, valueToAdd) => {
                const values = minutesString.split(":");
                let minutes = 0;
                if (values.length === 2) {
                    minutes = parseInt(values[0]) * 60 + parseInt(values[1]);
                } else if (values.length === 1) {
                    minutes = parseInt(values[0]);
                }
                minutes += valueToAdd;
                if (minutes < 0) {
                    minutes = 0;
                }

                const hours = Math.floor(minutes / 60);
                minutes %= 60;

                let text = "";
                if (hours < 10) {
                    text = "0";
                }
                text += hours;
                text += ":";
                if (minutes < 10) {
                    text += "0";
                }
                text += minutes;
                return text;
            };

            const input: HTMLInputElement = elem.querySelector("input");
            elem.querySelector(".add-button").addEventListener("click", () => {
                input.value = modifyMinutes(input.value, 15);
                input.dispatchEvent(new Event("change"));
            });
            elem.querySelector(".subtract-button").addEventListener("click", () => {
                input.value = modifyMinutes(input.value, -15);
                input.dispatchEvent(new Event("change"));
            });

            stepContainer.appendChild(elem);
        });

        renderTarget.appendChild(stepContainer);

        if (!isMaterialliste) {
            const resultContainer = document.createElement("div");
            const clockWidget = document.createElement("span");
            const nameWidget = GBLang.tl("duration in hours", false) as Element;
            const resultTimeWidget = document.createElement("span");

            resultContainer.appendChild(clockWidget);
            resultContainer.appendChild(nameWidget);
            resultContainer.appendChild(resultTimeWidget);

            resultContainer.classList.add("result-container");
            clockWidget.classList.add("result-clock");
            nameWidget.classList.add("result-name");
            resultTimeWidget.classList.add("result-time");

            resultTimeWidget.innerText = "00:00";

            renderTarget.appendChild(resultContainer);

            const inputs = stepContainer.querySelectorAll("input");
            inputs.forEach(input => {
                input.addEventListener("change", () => {
                    let summedTime = 0;
                    inputs.forEach(inp => {
                        const value = inp.value.split(":");
                        if (value.length === 2) {
                            summedTime += parseInt(value[0]) * 60 + parseInt(value[1]);
                        } else if (value.length === 1) {
                            summedTime += parseInt(value[0]);
                        }
                    });

                    const hours = Math.floor(summedTime / 60);
                    const minutes = summedTime % 60;

                    let text = "";
                    if (hours < 10) {
                        text = "0";
                    }
                    text += hours;
                    text += ":";
                    if (minutes < 10) {
                        text += "0";
                    }
                    text += minutes;

                    resultTimeWidget.innerText = text;
                });
            });
        }

        const proofButton = document.createElement("button");
        proofButton.appendChild(GBLang.tl("proof") as Element)

        const getMinutes = timeString => {
            let minutes = 0;
            const value = timeString.split(":");
            if (value.length === 2) {
                minutes += parseInt(value[0]) * 60 + parseInt(value[1]);
            } else if (value.length === 1) {
                minutes += parseInt(value[0]);
            }
            return minutes;
        }

        proofButton.addEventListener("click", () => {
            let res = true;
            stepContainer.querySelectorAll("select").forEach((select, i) => {
                if (select.value !== decisions[i]) {
                    res = false;
                }
            });
            stepContainer.querySelectorAll("input").forEach((input, i) => {
                if (!isMaterialliste) {
                    if (getMinutes(input.value) !== getMinutes(values[i])) {
                        res = false;
                    }
                } else if (input.value !== values[i]) {
                    res = false;
                }
            });

            stepContainer.querySelectorAll("option.hidden").forEach(hidden => hidden.classList.remove("hidden"));
            stepContainer.querySelectorAll(".plan-step").forEach(elem => elem.classList.add("closed"));

            proofButton.remove();

            if (res) {
                //Arbeitsplan ist richtig ausgefüllt, setze die Seite des Planes
                StorybookFragment.getInstance().setArbeitsplanSite();
                select(0);
            } else {
                select(1);
            }
        });

        renderTarget.appendChild(proofButton);
    }
}
