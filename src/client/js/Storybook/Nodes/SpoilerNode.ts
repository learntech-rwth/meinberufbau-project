import {GBRoomNode} from "ba-mbb-digitalstorybook/dist/common/Node/GBRoomNode";
import {GBLang} from "ba-mbb-digitalstorybook/dist/language/GBLang";
import {NodeHelper} from "./NodeHelper";

export class SpoilerNode extends GBRoomNode {

    constructor() {
        super();
        this.AddAttribute("roomSimpleLanguage", {
            label: GBLang.tl("room simple language", true) as string,
            type: "string",
            textarea: true
        });

        this.AddAttribute("spoilers", {
            label: GBLang.tl("spoilers", true) as string,
            type: "list",
            listType: "fields",
            fields: [
                {"label": "Heading"},
                {"label": "Heading - simple Speech"},
                {
                    "label": "Text",
                    textarea: true
                },
                {
                    "label": "Text - simple Speech",
                    textarea: true
                }
            ]
        });
        this.AddOutput("Output");
    }

    Render(select: { (output: number): void }, scope: { [p: string]: any }, renderTarget: HTMLElement): void {
        renderTarget.innerText = "";
        NodeHelper.addRoom(this, renderTarget, scope)

        const spoilerContainer = document.createElement("div");

        let spoilers = <string[][]>this.GetAttribute("spoilers");
        spoilers.forEach(s => {
            const simpleValues = [];
            if (s.length === 4){
                simpleValues.push(s[1]);
                simpleValues.push(s[3]);
                s = [
                    s[0],
                    s[2],
                ]
            }

            const spoilerElement = document.createElement("div");
            spoilerElement.classList.add("spoiler");
            const headingElement = document.createElement("p");
            headingElement.appendChild(NodeHelper.createSimpleTranslation(s[0], simpleValues[0], scope));
            headingElement.classList.add("spoiler-heading");
            headingElement.classList.add("textToSpeech")
            const textParagraph = document.createElement("p");
            textParagraph.appendChild(NodeHelper.createSimpleTranslation(s[1], simpleValues[1], scope));
            textParagraph.classList.add("spoiler-text");
            textParagraph.classList.add("textToSpeech")

            spoilerElement.appendChild(headingElement);
            spoilerElement.appendChild(textParagraph);
            spoilerContainer.appendChild(spoilerElement);

            headingElement.addEventListener("click", () => {
                if (spoilerElement.classList.contains("open")){
                    spoilerElement.classList.remove("open")
                }
                else {
                    spoilerElement.classList.add("open")
                }
            })
        });

        renderTarget.appendChild(spoilerContainer);

        const continueButton = document.createElement("button");
        continueButton.classList.add("textToSpeech")
        continueButton.appendChild(GBLang.tl("continue") as Element);
        continueButton.addEventListener("click", () => {
            select(0);
        });
        renderTarget.appendChild(continueButton);
    }
}