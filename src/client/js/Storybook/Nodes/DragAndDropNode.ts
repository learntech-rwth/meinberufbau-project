import {GBLang} from "ba-mbb-digitalstorybook/dist/language/GBLang";
import {NodeHelper} from "./NodeHelper";
import {Translator} from "cordova-sites";
import {GBRoomNode} from "ba-mbb-digitalstorybook/dist/common/Node/GBRoomNode";
import {DragHelper} from "js-helper/dist/client/Dragger/DragHelper";

export class DragAndDropNode extends GBRoomNode {

    constructor() {
        super();
        this.AddAttribute("roomSimpleLanguage", {
            label: GBLang.tl("room simple language", true) as string,
            type: "string",
            textarea: true
        });
        this.AddAttribute("dragAndDrop", {
            label: GBLang.tl("DragAndDrop", true) as string,
            type: "list",
            listType: "string",
        });
        this.AddAttribute("dragAndDropOrder", {
            type: "string",
            label: GBLang.tl("dragAndDrop order", true) as string,
        });
        this.AddAttribute("dragAndDropOrders", {
            type: "list",
            label: GBLang.tl("dragAndDrop orders alternative", true) as string,
            listType: "string"
        });

        this.AddOutput("Richtig");
        this.AddOutput("Alternative Formel");
        this.AddOutput("Falsch");
    }

    Render(select: (output: number) => void, scope: { [p: string]: any }, renderTarget: HTMLElement) {
        renderTarget.innerText = "";
        NodeHelper.addRoom(this, renderTarget, scope)

        const mainDropOrder = this.GetAttribute("dragAndDropOrder");
        const dragAndDropOrders = this.GetAttribute("dragAndDropOrders");

        const dragValues = this.GetAttribute("dragAndDrop");
        const dragIndexes = Object.keys(dragValues).sort(() => Math.random() - 0.5);
        const dragElements = {};


        const dragElementContainer = document.createElement("div");
        dragElementContainer.classList.add("drag-element-container")
        dragIndexes.forEach(index => {
            const dragElement = document.createElement("span");
            dragElement.classList.add("drag-element");
            dragElement.innerHTML = dragValues[index];
            // dragElement.dataset["index"] = index;
            dragElement.id = "drag-element-" + index;

            DragHelper.getInstance().draggable(dragElement).onStop((element, droppedUnto) => {
                if (droppedUnto.closest(".drag-element-container")) {
                    const parent = <HTMLElement>element.parentNode;
                    if (parent !== dragElementContainer) {
                        delete parent.dataset["childIndex"];
                        dragElementContainer.appendChild(element);
                    }
                } else {
                    const dropZone = <HTMLElement>droppedUnto.closest(".drop-zone");
                    if (dropZone) {
                        const childIndexBefore = dropZone.dataset["childIndex"];
                        const parentBefore = <HTMLElement>element.parentNode;
                        if (childIndexBefore && childIndexBefore !== index && dragElements[childIndexBefore]) {
                            const previousChild = dragElements[childIndexBefore];
                            parentBefore.insertBefore(previousChild, element);
                            // previousChild.remove();
                            parentBefore.dataset["childIndex"] = childIndexBefore;
                        } else {
                            delete parentBefore.dataset["childIndex"];
                        }

                        // element.remove();
                        dropZone.appendChild(element);
                        dropZone.dataset["childIndex"] = index;
                    }
                }
            })

            dragElementContainer.appendChild(dragElement);
            dragElements[index] = (dragElement);
        })

        // dragElementContainer.addEventListener("dragover", e => e.preventDefault());
        // dragElementContainer.addEventListener("drop", e => {
        //     e.preventDefault();
        //
        //     const index = e.dataTransfer.getData("text/plain");
        //     const element = dragElements[index];
        //     const parent = element.parentNode;
        //     if (parent !== dragElementContainer) {
        //         delete parent.dataset["childIndex"];
        //         dragElementContainer.appendChild(element);
        //     }
        // })

        const dropZone = document.createElement("span");
        dropZone.classList.add("drop-zone");
        dropZone.innerHTML = " ";

        const dropOrderString = mainDropOrder.replace(/\[([0-9]+)]/g, (string, index) => {
            const dropElement = <HTMLElement>dropZone.cloneNode(true);
            dropElement.dataset["index"] = index;
            return dropElement.outerHTML;
        });


        const dropElements = [];

        const dragTextElement = document.createElement("div");
        dragTextElement.innerHTML = dropOrderString;
        dragTextElement.classList.add("formel-container");
        (dragTextElement.querySelectorAll(".drop-zone") as NodeListOf<HTMLElement>).forEach(dropElement => {
            // dropElement.addEventListener("dragover", e => e.preventDefault());
            // dropElement.addEventListener("drop", e => {
            //     e.preventDefault();
            //     // @ts-ignore
            //     const index = e.dataTransfer.getData("text/plain");
            //     const element = dragElements[index];
            //     const childIndexBefore = dropElement.dataset["childIndex"];
            //     const parentBefore = element.parentNode;
            //     if (childIndexBefore && childIndexBefore !== index && dragElements[childIndexBefore]) {
            //         const previousChild = dragElements[childIndexBefore];
            //         parentBefore.insertBefore(previousChild, element);
            //         // previousChild.remove();
            //         parentBefore.dataset["childIndex"] = childIndexBefore;
            //     } else {
            //         delete parentBefore.dataset["childIndex"];
            //     }
            //
            //     // element.remove();
            //     dropElement.appendChild(element);
            //     dropElement.dataset["childIndex"] = index;
            // });
            dropElements.push(dropElement);
        })

        const button = document.createElement("button");
        button.appendChild(Translator.makePersistentTranslation("check"));

        button.addEventListener("click", () => {
            const orders = [];
            dragAndDropOrders.unshift(mainDropOrder)
            dragAndDropOrders.forEach(order => {
                const regex = RegExp(/\[([0-9]+)]/, 'g');
                let matches = [];
                const orderIndexes = [];
                while ((matches = regex.exec(order)) !== null) {
                    orderIndexes.push(matches[1]);
                }
                orders.push(orderIndexes);
            })

            const currentOrder = dropElements.map(element => element.dataset["childIndex"]);

            const solved = orders.some((order, index) => {
                if (order.length === currentOrder.length) {
                    if (order.every((val, i) => {
                        return parseInt(val) === parseInt(currentOrder[i])
                    })) {
                        if (index === 0) {
                            select(0);
                        } else {
                            select(1);
                        }
                        return true;
                    }
                }
                return false;
            })

            if (!solved) {
                select(2);
            }
        })

        renderTarget.appendChild(dragElementContainer);
        renderTarget.appendChild(dragTextElement);
        renderTarget.appendChild(button);
    }
}
