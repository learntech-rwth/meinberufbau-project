import {GBDecisionNode} from "ba-mbb-digitalstorybook/dist/common/Node/Templates/GBDecisionNode";
import {GBRender} from "ba-mbb-digitalstorybook/dist/common/GBRender";
import {GBLang} from "ba-mbb-digitalstorybook/dist/language/GBLang";
import {GBNode} from "ba-mbb-digitalstorybook/dist/common/Node/GBNode";
import {NodeHelper} from "./NodeHelper";
import {Translator} from "cordova-sites";
import {GBRoomNode} from "ba-mbb-digitalstorybook/dist/common/Node/GBRoomNode";

export class DecisionNode extends GBRoomNode {

    constructor() {
        super();
        this.AddAttribute("roomSimpleLanguage", {
            label: GBLang.tl("room simple language", true) as string,
            type: "string",
            textarea: true
        });
        this.AddAttribute("decisions", {
            label: GBLang.tl("Decisions", true) as string,
            type: "list",
            listType: "fields",
            fields: [
                {"label": "Decision"},
                {"label": "Decision - easy speech"},
            ]
        });
        this.AddAttribute("shuffle", {
            type: "boolean",
            label: GBLang.tl("shuffle", true) as string
        });
        this.AddAttribute("slider", {
            type: "boolean",
            label: GBLang.tl("slider", true) as string
        });

        this.On("decisions", decisions => this.Update(decisions));
    }

    Update(decisions) {
        let diff = decisions.length - this.ListAttributeLength("outputTargets");
        if (diff > 0) {
            while (diff-- > 0) {
                this.AddOutput();
            }
        } else {
            while (diff++ < 0) {
                this.RemoveLastOutput();
            }
        }
        this.SetAttribute("outputTitles", this.GetAttribute("decisions").map(names => names ? names[0] : ""));
        this.Trigger("outputTitles", decisions.map(names => names ? names[0] : ""), false);
    }

    Render(select: (output: number) => void, scope: { [p: string]: any }, renderTarget: HTMLElement) {
        renderTarget.innerText = "";
        NodeHelper.addRoom(this, renderTarget, scope)

        let decisions = this.GetAttribute("decisions");

        if (this.GetAttribute("slider")) {
            const row = document.createElement("div");
            row.classList.add("flex-container");

            const slider = document.createElement("div");
            slider.classList.add("grow");

            const inputSlider = document.createElement("input");
            inputSlider.type = "range";
            inputSlider.min = "0";
            inputSlider.step = "1";
            inputSlider.max = (decisions.length - 1).toString();
            inputSlider.value = "0";

            const sliderContainer = document.createElement("div");
            sliderContainer.classList.add("storybook-slider");
            sliderContainer.appendChild(inputSlider);

            const valueOutput = document.createElement("span");
            valueOutput.classList.add("storybook-slider-value-output");

            const rangeLabelArray = [];
            let currentIndex = 0;

            const rangeLabels = document.createElement("ul");
            rangeLabels.classList.add("storybook-slider-labels")

            decisions.forEach((decision, i) => {
                console.log("values", decision);
                let simpleValue = null;
                if (Array.isArray(decision)) {
                    simpleValue = decision[1];
                    decision = decision[0];
                }


                const rangeLabel = document.createElement("li");
                rangeLabel.appendChild(NodeHelper.createSimpleTranslation(decision, simpleValue, scope));
                rangeLabel.classList.add("textToSpeech");
                rangeLabel.addEventListener("click", () => {
                    inputSlider.value = i.toString();
                    inputSlider.dispatchEvent(new Event("input"));
                })
                rangeLabels.appendChild(rangeLabel);
                rangeLabelArray.push(rangeLabel);

                if (i === 0) {
                    rangeLabel.classList.add("active");
                    rangeLabel.classList.add("selected");
                }
            });

            inputSlider.addEventListener("input", () => {
                rangeLabelArray[currentIndex].classList.remove("active")
                for (let i = 0; i <= currentIndex; i++) {
                    rangeLabelArray[i].classList.remove("selected")
                }

                currentIndex = parseInt(inputSlider.value);
                rangeLabelArray[currentIndex].classList.add("active")
                for (let i = 0; i <= currentIndex; i++) {
                    rangeLabelArray[i].classList.add("selected")
                }

                valueOutput.innerHTML = rangeLabelArray[currentIndex].innerHTML;
            })
            Translator.addTranslationCallback(() => {
                valueOutput.innerHTML = rangeLabelArray[currentIndex].innerHTML;
            })

            slider.appendChild(sliderContainer);
            slider.appendChild(rangeLabels);
            row.appendChild(slider);
            row.appendChild(valueOutput);
            renderTarget.appendChild(row);

            const continueButton = document.createElement("button");
            continueButton.classList.add("textToSpeech")
            continueButton.appendChild(GBLang.tl("continue") as Element);
            continueButton.addEventListener("click", () => {
                select(parseInt(inputSlider.value));
            });
            renderTarget.appendChild(continueButton);

        } else {
            function createButton(decision: string|Array<string>, index: number) {
                let simpleValue = null;
                if (Array.isArray(decision)){
                    simpleValue = decision[1];
                    decision = decision[0];
                }

                let button = document.createElement("button");
                button.appendChild(NodeHelper.createSimpleTranslation(decision, simpleValue, scope));
                button.classList.add("textToSpeech");
                button.addEventListener("click", () => {
                    button.setAttribute("style", "background-color: lightblue");
                    button.prepend("pressed - ");
                    select(index);
                });
                renderTarget.appendChild(button);
            }
            if (this.GetAttribute("shuffle")) {
                GBNode.ShuffleForEach(decisions, createButton);
            } else {
                decisions.forEach(createButton);
            }
        }
    }
}
