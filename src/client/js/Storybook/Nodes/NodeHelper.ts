import {GBRender} from "ba-mbb-digitalstorybook/dist/common/GBRender";
import {Translator} from "cordova-sites";

export class NodeHelper{
    static addRoom(node, renderTarget, scope){
        const room = GBRender.GetRenderer().RenderRoom(
            node.GetAttribute("room"),
            scope
        );

        room.classList.add("textToSpeech");
        renderTarget.appendChild(room);

        if (node.HasAttribute("roomSimpleLanguage")){
            const attributeValue = node.GetAttribute("roomSimpleLanguage");
            if (attributeValue.trim() !== ""){
                const roomSimpleLanguage = GBRender.GetRenderer().RenderRoom(attributeValue, scope);
                roomSimpleLanguage.classList.add("textToSpeech");
                renderTarget.appendChild(roomSimpleLanguage);
                Translator.getInstance().addTranslationCallback(() => {
                    if (Translator.getInstance().getCurrentLanguage() === "de-sl"){
                        room.classList.add("hidden");
                        roomSimpleLanguage.classList.remove("hidden");
                    }
                    else {
                        room.classList.remove("hidden");
                        roomSimpleLanguage.classList.add("hidden");
                    }
                }, true);
            }
        }
    }

    static createSimpleTranslation(normalValue:string, simpleValue: string, scope?){

        if (scope){
            normalValue = GBRender.GetRenderer().SubstituteMath(normalValue, scope)
        }

        const otherElement = document.createElement("span");
        otherElement.innerHTML = normalValue;

        if (simpleValue && simpleValue.trim() !== ""){
            if (scope){
                simpleValue = GBRender.GetRenderer().SubstituteMath(simpleValue, scope)
            }

            const elem = document.createElement("span");
            elem.innerHTML = simpleValue;

            Translator.getInstance().addTranslationCallback(() => {
                if (Translator.getInstance().getCurrentLanguage() === "de-sl"){
                    otherElement.classList.add("hidden");
                    elem.classList.remove("hidden");
                }
                else {
                    otherElement.classList.remove("hidden");
                    elem.classList.add("hidden");
                }
            }, true)

            const container = document.createElement("span");
            container.appendChild(otherElement);
            container.appendChild(elem);
            return container;
        }
        return otherElement;
    }
}
