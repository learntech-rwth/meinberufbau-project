import {GBNode} from "ba-mbb-digitalstorybook/dist/common/Node/GBNode";
import {NodeHelper} from "./NodeHelper";
import {GBRoomNode} from "ba-mbb-digitalstorybook/dist/common/Node/GBRoomNode";
import {GBLang} from "ba-mbb-digitalstorybook/dist/language/GBLang";

export class MultipleChoiceNode extends GBRoomNode {

    constructor() {
        super();
        this.AddAttribute("roomSimpleLanguage", {
            label: GBLang.tl("room simple language", true) as string,
            type: "string",
            textarea: true
        });
        this.AddAttribute("true", {
            label: GBLang.tl("right", true) as string,
            type: "list",
            listType: "fields",
            fields: [
                {"label": "Answer"},
                {"label": "Answer - easy speech"},
            ]
        });
        this.AddAttribute("false", {
            label: GBLang.tl("wrong", true) as string,
            type: "list",
            listType: "fields",
            fields: [
                {"label": "Answer"},
                {"label": "Answer - easy speech"},
            ]
        });
        this.AddAttribute("single", {
            label: GBLang.tl("single", true) as string,
            type: "boolean"
        });
        this.AddAttribute("submit", {
                label: GBLang.tl("submitText", true) as string,
                type: "string"
            },
            GBLang.tl("submit", true)
        );
        this.AddOutput(GBLang.tl("correct", true) as string);
        this.AddOutput(GBLang.tl("incorrect", true) as string);
    }

    Render(select: (output: number) => void, scope: { [p: string]: any }, renderTarget: HTMLElement) {
        renderTarget.innerText = "";
        NodeHelper.addRoom(this, renderTarget, scope)

        const correct = this.GetAttribute("true");
        const incorrect = this.GetAttribute("false");

        const choices = correct.concat(incorrect);

        const result: boolean[] = new Array(choices.length);
        const single: boolean = this.GetAttribute("single");
        const submit = document.createElement("input");
        GBNode.ShuffleForEach(choices, (choice, index) => {
            let simpleChoiceText = null;
            if (Array.isArray(choice)) {
                simpleChoiceText = choice[1];
                choice = choice[0];
            }

            const input = document.createElement("input");
            input.id = "mc" + index;

            if (single) {
                input.setAttribute("type", "radio");
                input.setAttribute("name", "mc");
            } else {
                input.setAttribute("type", "checkbox");
            }

            const label = document.createElement("label");
            label.setAttribute("for", input.id);
            input.addEventListener("change", () => {
                result[index] = input.checked;
                submit.disabled = !result.reduce((prev, current) => prev || current, false);
            });
            label.appendChild(input);

            const labelTextElem = document.createElement("span");
            labelTextElem.classList.add("textToSpeech");
            labelTextElem.appendChild(
                NodeHelper.createSimpleTranslation(choice as string, simpleChoiceText, scope)
            );
            label.appendChild(labelTextElem);
            renderTarget.appendChild(label);
        });
        submit.setAttribute("type", "button");
        submit.setAttribute("value", this.GetAttribute("submit"));
        submit.addEventListener("click", () => {
            if (single) {
                let ans: { [key: number]: boolean; points: number } = {points: 0};
                for (let i = 0; i < result.length; ++i) {
                    ans[i] = result[i];
                    if (result[i] && i < correct.length) {
                        ans.points = 1;
                    } else if (result[i] && i >= correct.length) {
                        ans.points = -1;
                    }
                    scope.ans = ans;
                }
                if (ans.points == 1) select(0);
                else select(1);
            } else {
                let ans: { [key: number]: boolean; points: number } = {points: 0};
                for (let i = 0; i < result.length; ++i) {
                    ans[i] = result[i];
                    if (i < correct.length) {
                        if (result[i]) ans.points++;
                        else ans.points--;
                    } else {
                        if (result[i]) ans.points--;
                        else ans.points++;
                    }
                }
                scope.ans = ans;
                if (ans.points == choices.length) select(0);
                else select(1);
            }
        });
        submit.disabled = !result.reduce((prev, current) => prev || current, false);
        renderTarget.appendChild(submit);
    }
}
