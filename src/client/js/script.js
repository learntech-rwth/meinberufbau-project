import translationGerman from '../translations/de.json';
import translationGermanSimple from '../translations/de-sl.json';

import {
    App,
    MenuAction,
    Submenu,
    Translator,
    NavbarFragment,
    DataManager,
} from "cordova-sites/dist/client";
import {SyncJob} from "cordova-sites-easy-sync/dist/client";

//Importing TextSpeaker, so callback will be executed
import './TextSpeaker/TextSpeaker';
import "cordova-sites-user-management/dist/client/js/translationInit"
import "cordova-sites/dist/client/js/translationInit"

//Importieren von Flags für Sprachen. Bilder werden nur hinzugefügt, wenn diese auch referenziert werden.
//Da diese nicht direkt referenziert werden, hier einmal importieren, um diese hinzuzufügen
import "../img/flag_german.svg";
import "../img/flag_usa.svg";
import "../img/logo.png";

//Importing of Models
import {UserManager} from "cordova-sites-user-management/dist/client";
import {Exercise} from "../../shared/model/Exercise";
import {Course} from "../../shared/model/Course.ts";
import {Definition} from "../../shared/model/Definition.ts";
import {ExerciseProgress} from "../../shared/model/ExerciseProgress";

import {BaseDatabase} from "cordova-sites-database/dist/cordova-sites-database";
import {DeleteUserManagement1000000000000} from "cordova-sites-user-management/dist/shared/migrations/DeleteUserManagement";
import {SetupUserManagement1000000001000} from "cordova-sites-user-management/dist/shared/migrations/SetupUserManagement";
import {Setup1000000002000} from "../../shared/model/migrations/shared/Setup";
import {EasySyncClientDb, SetupEasySync1000000000500} from "cordova-sites-easy-sync/dist/client";
import {DefinitionImageNullable1000000005000} from "../../shared/model/migrations/shared/DefinitionImageNullable";
import {ErrorAction} from "cordova-sites/dist/client";
import {ExerciseProgressToPartialModel1000000006000} from "../../shared/model/migrations/client/ExerciseProgressToPartialModel";
import {Brackets, In} from "typeorm";
import {Helper} from "js-helper/dist/shared/Helper";
import {User} from "cordova-sites-user-management/dist/shared/v1/model/User";
import {WrongAnswer} from "../../shared/model/WrongAnswer";
import {AddWikiEntry1000000007000} from "../../shared/model/migrations/shared/AddWikiEntry";
import {WikiEntry} from "../../shared/model/WikiEntry";
import CKEditor from "@ckeditor/ckeditor5-build-classic";
import {ClearDatabaseJob} from "./ClearDatabase/ClearDatabaseJob";
import {GBNodeContainer} from "ba-mbb-digitalstorybook/dist/common/GBNodeContainer";
import {DecisionNode} from "./Storybook/Nodes/DecisionNode";
import {MultipleChoiceNode} from "./Storybook/Nodes/MultipleChoiceNode";
import {TextSpeaker} from "./TextSpeaker/TextSpeaker";
import {AddFileMediumMigration1000000011000} from "cordova-sites-easy-sync/dist/shared";
import {FileMedium} from "cordova-sites-easy-sync/dist/shared";
import {SpoilerNode} from "./Storybook/Nodes/SpoilerNode";
import {CustomNode} from "./Storybook/Nodes/CustomNode";
import {NumbersNode} from "./Storybook/Nodes/NumbersNode";
import {GBLang} from "ba-mbb-digitalstorybook/dist/language/GBLang";
import {DefinitionImageAsFileMedium1000000013000} from "../../shared/model/migrations/shared/DefinitionImageAsFileMedium";
import {DefinitionSimpleGerman1000000014000} from "../../shared/model/migrations/shared/DefinitionSimpleGerman";
import {DefinitionLongerText1000000015000} from "../../shared/model/migrations/shared/DefinitionLongerText";
import {Lexicon} from "./Lexicon";
import {DragAndDropNode} from "./Storybook/Nodes/DragAndDropNode";
import {FormulaNode} from "./Storybook/Nodes/FormulaNode";
import {LoginSite} from "cordova-sites-user-management/dist/client/js/Site/LoginSite";
import {RegistrationSite} from "cordova-sites-user-management/dist/client/js/Site/RegistrationSite";
import {StartUserSiteMenuAction} from "cordova-sites-user-management/dist/client/js/MenuAction/StartUserSiteMenuAction";
import {MbbLoginSite} from "./Sites/MbbLoginSite";
import {ChangeFontSizeMenu} from "./MenuActions/ChangeFontSizeMenu";
import {ChangeBgColorMenu} from "./MenuActions/ChangeBgColorMenu";
import {StartSite} from "./Sites/StartSite";
import {Intro} from "./Sites/Intro";
import {Impressum} from "./Sites/Impressum";
import {Tutorials} from "./Sites/Tutorials";
import {AddStageToExercises1000000017000} from "../../shared/model/migrations/shared/AddStageToExercises";
import {AddImageAndDescription1000000018000} from "../../shared/model/migrations/shared/AddImageAndDescription";
import "./Sites/LexiconSite";
import "./Sites/FileSite";
import "./Sites/FormulaSite";
import "./Sites/GbObjectsSite";
import {GBRender} from "ba-mbb-digitalstorybook/dist/common/GBRender";
import {ArbeitsplanNode} from "./Storybook/Nodes/ArbeitsplanNode";

window["JSObject"] = Object;
window["CKEditor"] = CKEditor;

//Disable Google Analytics for VideoJS
window["HELP_IMPROVE_VIDEOJS"] = false;

NavbarFragment.title = "";

//Update Storybook NodeTemplates
GBNodeContainer.setTemplate("Decision", DecisionNode);
GBNodeContainer.setTemplate("Multiple choice", MultipleChoiceNode);
GBNodeContainer.setTemplate("Custom", CustomNode);
GBNodeContainer.setTemplate("Spoiler", SpoilerNode);
GBNodeContainer.setTemplate("Numbers", NumbersNode);
GBNodeContainer.setTemplate("Drag and Drop", DragAndDropNode);
GBNodeContainer.setTemplate("Formula", FormulaNode);
GBNodeContainer.setTemplate("Arbeitsplan", ArbeitsplanNode);

//Set Speaking Rate for Audio
GBRender.GetRenderer().setPlaybackSpeedGetter(() => {
    return TextSpeaker.getInstance().getSpeakingRate();
})

//Diese App benutzt eine eigene Seite für Registration und Login. Daher deaktiviere Standard-Login und Registration-Seite
//indem Menu-Actions und DeepLinks nicht hinzugefügt werden
LoginSite.ADD_LOGIN_ACTION = false;
LoginSite.ADD_DEEP_LINK = false;
RegistrationSite.ADD_REGISTRATION_ACTION = false;
RegistrationSite.ADD_DEEP_LINK = false;

//Setze Pfade für den Server. __HOST_ADDRESS__ wird durch WebPack ersetzt
DataManager._basePath = __HOST_ADDRESS__ + "/api/v1/";
FileMedium.PUBLIC_PATH = __HOST_ADDRESS__ + "/uploads/img_";

Object.assign(BaseDatabase.CONNECTION_OPTIONS, {
    logging: ["error",],
    synchronize: false,
    migrationsRun: true,

    //Migrations beinhalten die Datenbankstruktur. Generell müssen diese mit einem Unix-Timestamp enden und
    // werden der Reihenfolge dieses Unix-Timestamps ausgeführt.
    migrations: [
        DeleteUserManagement1000000000000,
        SetupEasySync1000000000500,
        SetupUserManagement1000000001000,
        Setup1000000002000,
        DefinitionImageNullable1000000005000,
        ExerciseProgressToPartialModel1000000006000,
        AddWikiEntry1000000007000,
        AddFileMediumMigration1000000011000,
        DefinitionImageAsFileMedium1000000013000,
        DefinitionSimpleGerman1000000014000,
        DefinitionLongerText1000000015000,
        AddStageToExercises1000000017000,
        AddImageAndDescription1000000018000,
    ]
});

//Falls es zu Datenbank-Fehlern kommt. mache folgendes
EasySyncClientDb.errorListener = async (e) => {
    console.error(e);
    debugger; //debugger, damit bei der lokalen Entwicklung der Fehler auch auffällt

    //löscht die Datenbank komplett. Danach wird die Seite neu geladen (siehe unten)
    // Löschen ist nicht schlimm, da alle Daten noch einmal auf dem Server gespeichert sind. Dauert nur
    // etwas, diese alle wieder herunterzuladen.
    await ClearDatabaseJob.doJob();

    //work with timeout since saving of db only occurs after 150ms in browser
    return new Promise(resolve => {
        setTimeout(() => {
            resolve(window.location.reload(true));
        }, 200);
    });
};

App.addInitialization(async () => {

    //Initialisierung des Translators
    Translator.init({
        translations: {
            "de": translationGerman,
            "de-sl": translationGermanSimple,
        },
        fallbackLanguage: "de",
        markUntranslatedTranslations: __MARK_TRANSLATIONS__, //Durch WebPack wird __MARK_TRANSLATIONS__ ersetzt
    });

    //Sorgt dafür, dass auch die Texte vom Gamebook den Übersetzer hier benutzen
    GBLang.setTranslationCallback((key, asText) => {
        if (asText) {
            return Translator.getInstance().translate(key);
        } else {
            return Translator.getInstance().makePersistentTranslation(key);
        }
    })

    //Fügt die Action zum Ändern der Sprache hinzu
    let submenuLanguage = new Submenu("sprache", MenuAction.SHOW_NEVER, 1001);
    const actionDe = new MenuAction("de", () => {
        //Setze sprache
        Translator.getInstance().setLanguage("de");

        //Zeige im Menü, welches markiert ist
        actionDeSl.setLiClass("");
        actionDe.setLiClass("selected");
    }, MenuAction.SHOW_NEVER);
    const actionDeSl = new MenuAction("de-sl", () => {
        Translator.getInstance().setLanguage("de-sl");
        actionDeSl.setLiClass("selected");
        actionDe.setLiClass("");
    }, MenuAction.SHOW_NEVER);

    //Setze momentan selektierte Sprache im Menü
    if (Translator.getInstance().getCurrentLanguage() === "de") {
        actionDe.setLiClass("selected");
    } else {
        actionDeSl.setLiClass("selected");
    }

    submenuLanguage.addAction(actionDeSl);
    submenuLanguage.addAction(actionDe);

    NavbarFragment.defaultActions.push(submenuLanguage.getParentAction());

    //Füge change font size-Action hinzu
    NavbarFragment.defaultActions.push(new ChangeFontSizeMenu(MenuAction.SHOW_NEVER, 1003).getParentAction());

    //Füge change bgcolor-Action hinzu
    NavbarFragment.defaultActions.push(new ChangeBgColorMenu(MenuAction.SHOW_NEVER, 1003).getParentAction());

    //Füge speakingRateAction hinzu
    const speakingRateAction = TextSpeaker.getInstance().getSpeakingRateSubmenu().getParentAction();

    //Soll nur im Burger-Menü angezeigt werden => verändere showFor
    speakingRateAction._showFor = MenuAction.SHOW_NEVER;
    NavbarFragment.defaultActions.push(speakingRateAction);

    NavbarFragment.defaultActions.push(ErrorAction.getInstance());

    //StartUserSiteMenuAction ist nur für die sichtbar, mit der richtigen Rolle (hier: loggedOut) und startet eine
    // Seite (hier: MbbLoginSite), wenn die Action ausgeführt wird.
    NavbarFragment.defaultActions.push(new StartUserSiteMenuAction("Login", "loggedOut", MbbLoginSite, MenuAction.SHOW_NEVER));

    //Intro anzeigen: soll immer gehen, daher hier einmal für loggedOut und einmal für loggedIn
    NavbarFragment.defaultActions.push(new StartUserSiteMenuAction("Intro", "loggedOut", Intro, MenuAction.SHOW_NEVER));
    NavbarFragment.defaultActions.push(new StartUserSiteMenuAction("Intro", "loggedIn", Intro, MenuAction.SHOW_NEVER));

    //Impressum anzeigen: soll immer gehen, daher hier einmal für loggedOut und einmal für loggedIn
    NavbarFragment.defaultActions.push(new StartUserSiteMenuAction("Impressum", "loggedOut", Impressum, MenuAction.SHOW_NEVER));
    NavbarFragment.defaultActions.push(new StartUserSiteMenuAction("Impressum", "loggedIn", Impressum, MenuAction.SHOW_NEVER));

    //Tutorials anzeigen: soll immer gehen, daher hier einmal für loggedOut und einmal für loggedIn
    NavbarFragment.defaultActions.push(new StartUserSiteMenuAction("Tutorials", "loggedOut", Tutorials, MenuAction.SHOW_NEVER));
    NavbarFragment.defaultActions.push(new StartUserSiteMenuAction("Tutorials", "loggedIn", Tutorials, MenuAction.SHOW_NEVER));

    //Falls mal die Verbindung abbricht, soll ein Fehler angezeigt werden
    DataManager.onlineCallback = isOnline => {
        let error = "No connection to server. May not be online";
        if (!isOnline) {
            // new Toast("not online!").show();
            ErrorAction.addError(error)
        } else {
            ErrorAction.removeError(error);
        }
    };

    //Rufe aktuellen User-Status ab. Checkt auch, ob man momentan eingeloggt ist.
    // Warte auf Ergebnis mit await, da Synchronisierung davon abhängt
    await UserManager.getInstance().getMe().catch(e => console.error(e));

    //Synchronisiere Daten
    let syncJob = new SyncJob();
    await syncJob.syncInBackgroundIfDataExists([Course, Exercise, WikiEntry, FileMedium,
        //Entweder wird ein Model angegeben oder ein Object. Wenn ein Object angegeben wird, kann über
        // where noch spezielle Bedingungen angegeben werden
        {
            model: Definition,
            where: {
                user: null //Alle definitionen, die keinen User haben
            }
        },
        UserManager.syncParamFor(Definition), //Alle Definitionen des aktuellen Users
        UserManager.userSyncParam()]).catch(e => console.error(e));


    UserManager.getInstance().addLoginChangeCallback(async (isLoggedIn) => {
        if (isLoggedIn) {
            //Sobald User sich einloggt, lade auch seine Definitionen, sowie seine Infos
            await new SyncJob().sync([UserManager.syncParamFor(Definition), UserManager.userSyncParam()]).catch(e => console.error(e)).then(r => Lexicon.getInstance()._loadDefinitions(true));
        }
    });

    //Sende Daten zum Server
    new Promise(async resolve => {
        let user = new User();
        user.id = UserManager.getInstance().getUserData().id;

        if (Helper.isNotNull(user.id)) {
            //Lade Progresses
            let queryBuilder = await EasySyncClientDb.getInstance().createQueryBuilder(ExerciseProgress);
            queryBuilder = queryBuilder
                .innerJoinAndSelect("ExerciseProgress.element", "element")
                .andWhere("ExerciseProgress.id IS NULL").andWhere(new Brackets(qb => {
                    qb.where("ExerciseProgress.isDone = 1").orWhere("ExerciseProgress.clientId NOT IN " +
                        qb.subQuery().select("MAX(exerciseProgress2.clientId)").from("ExerciseProgress", "exerciseProgress2").groupBy("elementId").getQuery()
                    );
                }));

            let loadedProgresses = await queryBuilder.getMany();

            let clientIds = [];

            loadedProgresses.forEach(progress => {
                progress.user = user;
                clientIds.push(progress.clientId)
            });
            //durch saveMany werden diese an den Server gesendet
            let savePromise = ExerciseProgress.saveMany(loadedProgresses, false);

            let wrongAnswers = await WrongAnswer.find({exerciseProgress: {clientId: In(clientIds)}});
            wrongAnswers.forEach(wrongAnswer => wrongAnswer.user = user);
            await WrongAnswer.saveMany(wrongAnswers, false);

            //Erst hier awaiting, damit gleichzeitig wrongAnswers und Progresses hochgeladen werden können
            await savePromise;
        }

        resolve();
    });
});

//Starte App
let app = new App();
app.start(Intro);
app.ready(() => {
    console.log("initialisation over", new Date());

    //Zum Testen: Erlaube einfaches querien der Client-DB
    window["queryDb"] = async (sql) => {
        let res = await EasySyncClientDb.getInstance().rawQuery(sql);
        console.log(res);
        return res;
    };
});
