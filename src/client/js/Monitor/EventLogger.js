import {EventMessage} from "./EventMessage";

export class EventLogger {
    static getInstance(){
        if (!EventLogger._instance){
            EventLogger._instance = new EventLogger();
        }
        return EventLogger._instance;
    }

    constructor(){
        this._messages = [];
    }

    log(message, event){
        this._messages.push(new EventMessage(message, event))
    }
}
EventLogger._instance = null;
