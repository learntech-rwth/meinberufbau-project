const strassenbauJPG = require("../../img/courses/strassenbau.png");
const kanalbauJPG = require("../../img/courses/kanalbau.png");
const rohrleitungsbauJPG = require("../../img/courses/rohrleitungsbau.png");
const weitereTiefbau = require("../../img/courses/weitereTiefbau.png");
const mauerwerksbauJPG = require("../../img/courses/Mauerwerksbau.png");
const stahlbetonbauJPG = require("../../img/courses/stahlbetonbau.png");
const weitereHochbau = require("../../img/courses/weitereHochbau.png");
const fliesenPNG = require("../../img/courses/fliesen.png");
const stukkateurPNG = require("../../img/courses/stukkateur.png");
const zimmerPNG = require("../../img/courses/zimmer.png");
const weitereAusbau = require("../../img/courses/weitereAusbau.png");

export class CourseHelper{
    static editSource(source) {
        if (source.endsWith("Straßenbau.jpg")) {
            return strassenbauJPG;
        }
        if (source.endsWith("Kanalbau.JPG")) {
            return kanalbauJPG;
        }
        if (source.endsWith("Rohrleitungsbau.JPG")) {
            return rohrleitungsbauJPG;
        }
        if (source.endsWith("tiefbau")) {
            return weitereTiefbau;
        }

        if (source.endsWith("Mauerwerksbau.JPG")) {
            return mauerwerksbauJPG;
        }
        if (source.endsWith("Stahlbetonbau.jpg")) {
            return stahlbetonbauJPG;
        }
        if (source.endsWith("hochbau")) {
            return weitereHochbau;
        }

        if (source.endsWith("fliesen")) {
            return fliesenPNG;
        }
        if (source.endsWith("stukkateur")) {
            return stukkateurPNG;
        }
        if (source.endsWith("zimmer")) {
            return zimmerPNG;
        }
        if (source.endsWith("ausbau")) {
            return weitereAusbau;
        }
        return source;
    }
}
