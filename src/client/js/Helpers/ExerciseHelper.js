import {ExerciseProgress} from "../../../shared/model/ExerciseProgress.ts";
import {UserManager} from "cordova-sites-user-management/dist/client";
import {User} from "cordova-sites-user-management/dist/shared";
import {WrongAnswer} from "../../../shared/model/WrongAnswer";
import {Helper} from "js-helper/dist/shared/Helper";

export class ExerciseHelper {
    /**
     * @returns {Promise<null|ExerciseProgress>}
     */
    static async getProgressFor(exercise) {
        let userId = UserManager.getInstance().getUserData().id;

        if (!exercise.progress) {
            exercise.progress = await ExerciseProgress.findOne({
                element: {id: exercise.id},
                // user: {id: userId}
            }, {runNumber: "DESC"}, undefined, ExerciseProgress.getRelations());
        }

        let p = exercise.progress;
        if (!p) {
            p = new ExerciseProgress();
            p.element = exercise;
            exercise.progress = p;
            let user = new User();
            user.id = userId;
            p.user = user;
            p.userId = user.id;
        }

        await p._savePromise;

        return p;
    }

    static async createNextProgressFor(exercise) {
        let p = await ExerciseHelper.getProgressFor(exercise);

        let newP = new ExerciseProgress();
        newP.runNumber = p.runNumber+1;
        newP.element = exercise;
        exercise.progress = newP;
        let user = new User();
        user.id = UserManager.getInstance().getUserData().id;
        newP.user = user;

        return newP;
    }

    static addWrongAnswer(exerciseProgress, field, given, expected){
        exerciseProgress._wrongAnswers = Helper.nonNull(exerciseProgress._wrongAnswers, []);

        let wrongAnswer = new WrongAnswer();
        wrongAnswer.exerciseProgress = exerciseProgress;
        wrongAnswer.field = field;
        wrongAnswer.given = given;
        wrongAnswer.expected = expected;

        let user = new User();
        user.id = UserManager.getInstance().getUserData().id;
        wrongAnswer.user = user;

        exerciseProgress._wrongAnswers.push(wrongAnswer);
        return wrongAnswer;
    }

}
