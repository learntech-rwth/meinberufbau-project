import {AbstractFragment} from "cordova-sites/dist/client";
import {Calculator} from "../Dialog/Calculator";
import {UnitCalculator} from "../Dialog/UnitCalculator";

const view = require("../../html/Fragments/footerFragment.html");

export class FooterFragment extends AbstractFragment {

    private forwardListener: () => void = null;
    private skipForwardListener: () => void = null;
    private backListener: () => boolean = null;
    private arbeitsplanListener: () => void = null;

    private forwardButton: HTMLElement;
    private skipForwardButton: HTMLElement;

    private isPlanungshilfenOpen: boolean = false;

    constructor(site: any) {
        super(site, view);
    }

    onViewLoaded(): Promise<any[]> {
        const res = super.onViewLoaded();
        //ToolsMenü
        const toolsMenu = this.findBy("#tools-menu");
        this.findBy("#open-tools-button")?.addEventListener("click", () => {
            toolsMenu.classList.add("open");
            planungshilfenMenu.classList.add("hidden");
        });
        this.findBy("#close-tools-button")?.addEventListener("click", () => {
            toolsMenu.classList.remove("open");
            if(this.isPlanungshilfenOpen){planungshilfenMenu.classList.remove("hidden");}
        });
        this.findBy("#tools-menu-close-listener")?.addEventListener("click", () => {
            toolsMenu.classList.remove("open");
            if(this.isPlanungshilfenOpen){planungshilfenMenu.classList.remove("hidden");}
        });

        this.findBy("#calculator-button")?.addEventListener("click", () => {
            toolsMenu.classList.remove("open");
            if(this.isPlanungshilfenOpen){planungshilfenMenu.classList.remove("hidden");}
            new Calculator().show();
        });
        this.findBy("#unit-calculator-button")?.addEventListener("click", () => {
            toolsMenu.classList.remove("open");
            if(this.isPlanungshilfenOpen){planungshilfenMenu.classList.remove("hidden");}
            new UnitCalculator().show();
        });
        this.findBy("#glossary-button")?.addEventListener("click", () => {
            toolsMenu.classList.remove("open");
            if(this.isPlanungshilfenOpen){planungshilfenMenu.classList.remove("hidden");}
            this.startSite(window["LexiconSite"], null);
        });
        this.findBy("#formula-button")?.addEventListener("click", () => {
            toolsMenu.classList.remove("open");
            if(this.isPlanungshilfenOpen){planungshilfenMenu.classList.remove("hidden");}
            this.startSite(window["FormulaSite"], null);
        });
        this.findBy("#file-button")?.addEventListener("click", () => {
            toolsMenu.classList.remove("open");
            if(this.isPlanungshilfenOpen){planungshilfenMenu.classList.remove("hidden");}
            this.startSite(window["FileSite"], null);
        });

        //Planungshilfen
        const planungshilfenMenu = this.findBy("#planungshilfe-menu");
        this.findBy("#open-planungshilfe-button")?.addEventListener("click", () => {
            planungshilfenMenu.classList.add("open");
        });
        this.findBy("#close-planungshilfe-button")?.addEventListener("click", () => {
            planungshilfenMenu.classList.remove("open");
        });
        this.findBy("#planungshilfe-menu-close-listener")?.addEventListener("click", () => {
            planungshilfenMenu.classList.remove("open");
        });
        this.findBy("#arbeitsplan-button")?.addEventListener("click", () => {
            planungshilfenMenu.classList.remove("open");
            if (this.arbeitsplanListener){
                this.arbeitsplanListener();
            }
        });
        this.findBy("#objects-button")?.addEventListener("click", () => {
            planungshilfenMenu.classList.remove("open");
            this.startSite(window["GbObjectsSite"], null);
        });

        //Navigation
        this.findBy("#back-button").addEventListener("click", () => {
            //FÜhrt den Back-Listener aus. Wenn dieser nicht false zurück gibt, wird auch die Seite zurückgegangen
            if (!this.backListener || this.backListener()) {
                this.getSite().goBack()
            }
        });

        this.forwardButton = this.findBy("#forward-button");
        this.skipForwardButton = this.findBy("#skip-forward-button");

        this.forwardButton.addEventListener("click", () => {
            if (this.forwardListener) {
                this.forwardListener();
            }
        });
        this.skipForwardButton.addEventListener("click", () => {
            if (this.skipForwardListener) {
                this.skipForwardListener();
            }
        });

        this.setForwardListener(this.forwardListener);
        this.setSkipForwardListener(this.skipForwardListener);

        return res;
    }

    showPlanungshilfenMenu(shouldShow: boolean){
        this.isPlanungshilfenOpen = shouldShow;
        const planungshilfenMenu = this.findBy("#planungshilfe-menu");
        if (shouldShow){
            planungshilfenMenu.classList.remove("hidden");
        }
        else {
            planungshilfenMenu.classList.add("hidden");
        }
    }

    setArbeitsplanListener(listener: () => void){
        this.arbeitsplanListener = listener;
    }


    setBackListener(listener: () => boolean){
        this.backListener = listener;
    }

    setForwardListener(listener: () => void) {
        this.forwardListener = listener;

        //Zeige Forward-Button nur, wenn ein Listener gesetzt ist
        if (this.forwardButton) {
            if (this.forwardListener) {
                this.forwardButton.classList.remove("hidden");
            } else {
                this.forwardButton.classList.add("hidden");
            }
        }
    }

    setSkipForwardListener(listener: () => void) {
        this.skipForwardListener = listener;

        //Zeige Skip-Button nur, wenn ein Listener gesetzt ist
        if (this.skipForwardButton) {
            if (this.skipForwardListener) {
                this.skipForwardButton.classList.remove("hidden");
            } else {
                this.skipForwardButton.classList.add("hidden");
            }
        }
    }
}
