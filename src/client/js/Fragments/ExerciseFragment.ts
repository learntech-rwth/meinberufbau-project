import {Exercise} from "../../../shared/model/Exercise";

const view = require("../../html/Fragments/exerciseFragment.html");
import {WallLengthExerciseFragment} from "./ExerciseFragments/WallLengthExerciseFragment";
import {AbstractFragment, Helper} from "cordova-sites/dist/client";
// import {BuildWallExerciseFragment} from "./ExerciseFragments/BuildWallExerciseFragment";
import {WallLengthEightExerciseFragment} from "./ExerciseFragments/WallLengthEightExerciseFragment";
import {BuildWallDimension} from "./ExerciseFragments/BuildWallDimension";
import {Generator} from "../ExerciseGenerator/Generator";
import {EditExerciseSite} from "../Sites/EditExerciseSite";
import {ExerciseHelper} from "../Helpers/ExerciseHelper";
import {StorybookFragment} from "./ExerciseFragments/StorybookFragment";

/**
 * Zeigt eine Exercise an. Dieses Fragment inkludiert dann ein weiteres, Exercise-Spezifisches Fragment, welche unter
 * src/client/js/Fragments/ExerciseFragments gefunden werden können.
 */
export class ExerciseFragment extends AbstractFragment {
    private _mainFragment: BuildWallDimension | StorybookFragment | WallLengthExerciseFragment | WallLengthEightExerciseFragment;
    private _exercise: Exercise;

    constructor(site) {
        super(site, view);
    }

    async setExercise(exercise: Exercise) {
        this._exercise = exercise;

        let progress = await ExerciseHelper.getProgressFor(this._exercise);

        if (progress.data === null) {
            //Generiere, wenn Exercise generiert werden muss.
            if (this._exercise.isGenerating) {
                let generator = new Generator();
                let {validator, values} = EditExerciseSite.getTypeInfos(this._exercise.elementType);

                //Generiere und beachte den Validator. Wiederhole, bis Validator validiert oder bis maximale Zyklenzahl
                // durchlaufen ist
                let data = await generator.generateData(this._exercise.generatingData, validator);

                //Aufbereitung der values, generierung von Bildern
                await Helper.asyncForEach(Object.keys(values), async value => {
                    if (typeof values[value] === "number") {
                        values[value] = {
                            type: values[value]
                        }
                    }
                    if (values[value].type === EditExerciseSite.TYPE.EXERCISE_IMAGE) {
                        data[value] = await generator._generateImageFor(this._exercise.elementType, data);
                    }
                }, true);

                progress.data = data;
            } else {
                progress.data = this._exercise.generatingData;
            }
        }

        //await view for name and view-container!
        await this._viewPromise;

        //Füge spezielles Aufgaben-Fragment hinzu
        await this.addFragmentForElement(this._exercise);

        //Da Aufgabenfragment auch Views hinzufügen, warte auch hier, bis diese geladen sind
        await this._viewPromise;
    }

    // async onViewLoaded() {
    //     //Wenn zurück gedrückt wird, mache dies beim Main-Fragment
    //     this.findBy("#back-button").addEventListener("click", () => {
    //         if (Helper.isNotNull(this._mainFragment) && "goBack" in this._mainFragment) {
    //             this._mainFragment.goBack();
    //         }
    //     });
    //
    //     return await super.onViewLoaded();
    // }

    async addFragmentForElement(element) {
        let newFragment = null;
        switch (element.getElementType()) {
            case "element-wall-length-exercise": {
                newFragment = new WallLengthExerciseFragment(this);
                break;
            }
            case "element-wall-length-eight-exercise": {
                newFragment = new WallLengthEightExerciseFragment(this);
                break;
            }
            case "element-build-wall-dimension": {
                newFragment = new BuildWallDimension(this);
                break;
            }
              case "storybook": {
                newFragment = new StorybookFragment(this);
                break;
            }
            default: {
                console.error("not supported element type!", element);
                return;
            }
        }

        if (Helper.isNull(this._mainFragment)) {
            this._mainFragment = newFragment;
        }

        await newFragment.setElement(element);
        this.addFragment("#view-container", newFragment);
    }
}
