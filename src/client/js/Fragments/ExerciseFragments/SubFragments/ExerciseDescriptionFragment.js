import view from "../../../../html/Fragments/ExerciseFragments/Exercises/SubFragments/exerciseDescriptionFragment.html";
import {Helper, SwipeChildFragment, Toast} from "cordova-sites/dist/client";
import {ExerciseHelper} from "../../../Helpers/ExerciseHelper";

/**
 * Fragment zum Anzeigen der Aufgabenbeschreibung
 */
export class ExerciseDescriptionFragment extends SwipeChildFragment {
    constructor(site) {
        super(site, view);
        this._answerIndexes = [];
        this._rightAnswer = null;
    }

    async setElement(element) {
        this._exercise = element;
        let progress = await ExerciseHelper.getProgressFor(this._exercise);

        await this.setData(progress.getData());
        await this.setState(progress.state);
    }

    async onViewLoaded() {
        let res = super.onViewLoaded();
        this.findBy(".start-exercise-button").addEventListener("click", () => {
            this._checkAnswer();
        });

        return res;
    }

    //Gibt kein Fragment davor, also mache auch nichts
    async onSwipeRight() {
    }

    //Überprüfe die Antwort. In checkAnswer wird dann auf das nächste Segment zurückgegriffen, falls
    // die Antwort richtig ist
    async onSwipeLeft() {
        await this._checkAnswer();
    }

    async _checkAnswer() {
        if (Helper.isNotNull(this._rightAnswer) && this._answerIndexes.length > 0) {

            //Wähle selektierte Antwort aus
            let selectedAnswer = this.findBy("[name='exercise-understood-exercise-correct']:checked");
            let progress = await ExerciseHelper.getProgressFor(this._exercise);

            //Wenn keine Antwort ausgewählt wurde, informiere User und mache nichts
            if (Helper.isNull(selectedAnswer)) {

                //Speichere falsche Antwort
                let data = progress.getData();
                ExerciseHelper.addWrongAnswer(progress, "exercise-understood-exercise-correct", null, data["exercise-understood-exercise-answer-"+this._rightAnswer]);

                new Toast("please select an answer").show();
                return;
            }

            //Falsch false Antwort ausgewählt, informiere User und mache nichts
            if (selectedAnswer.value >= this._answerIndexes.length
                || this._answerIndexes[selectedAnswer.value] !== this._rightAnswer) {

                //Speichere falsche Antwort
                let data = progress.getData();
                ExerciseHelper.addWrongAnswer(progress, "exercise-understood-exercise-correct", data["exercise-understood-exercise-answer-"+(this._answerIndexes[selectedAnswer.value])], data["exercise-understood-exercise-answer-"+this._rightAnswer]);

                new Toast("wrong answer").show();
                return;
            }

            //Richtige Antwort wurde gegeben, speichere Antwort und gehe zum nächsten Fragment
            let state = progress.state;
            state["exercise-understood-exercise-selected"] = this._rightAnswer;
            progress.state = state;
            await this.getSite().showLoadingSymbol();
            await progress.save();
            await this.getSite().removeLoadingSymbol();

            this.nextFragment();
        }
    }

    async setData(data) {

        //Setze die Aufgabenbeschreibung
        this._descriptionElement = document.createTextNode(data["exercise-description"]);
        (await this.findBy(".exercise-description", null, true)).appendChild(this._descriptionElement);

        this._image = data["exercise-image"];
        this._imageDescription = data["exercise-image-description"];

        //Falls Bild gesetzt, setze dieses (Nicht jede Aufgabe hat ein Bild für die Aufgabenbeschribung)
        if (!Helper.imageUrlIsEmpty(this._image)) {
            let img = await this.findBy(".exercise-image", null, true);
            img.src = this._image;
            img.title = this._imageDescription;
            img.alt = this._imageDescription;

            (await this.findBy(".exercise-image-description", null, true)).appendChild(document.createTextNode(this._imageDescription));
        }

        //Setze Aufgabenfrage
        (await this.findBy(".exercise-understood-exercise-question", null, true))
            .appendChild(document.createTextNode(data["exercise-understood-exercise-question"]));

        //Randomize Antworten und setze diese
        this._answerIndexes = Helper.shuffleArray([
            1, 2, 3, 4
        ]);
        this._rightAnswer = parseInt(data["exercise-understood-exercise-correct"]);

        await Helper.asyncForEach(this._answerIndexes, async (index, i) => {
            (await this.findBy(".exercise-understood-exercise-answer-" + (i + 1), null, true))
                .appendChild(document.createTextNode(data["exercise-understood-exercise-answer-" + index]));
        });
    }

    async setState(state) {

        //Setze die Aufgabe als beantwortet, wenn dies bereits aus dem Savestate hervorgeht
        if (state["exercise-understood-exercise-selected"]) {
            let elem = (await this.findBy("[name='exercise-understood-exercise-correct'][value='"
                + this._answerIndexes.indexOf(state["exercise-understood-exercise-selected"])
                + "']", null, true));
            elem.checked = true;
        }
    }
}
