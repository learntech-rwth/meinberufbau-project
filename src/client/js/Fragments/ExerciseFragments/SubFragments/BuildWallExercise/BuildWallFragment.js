import view from "../../../../../html/editArticleView/editBuildWallExercise.html";
import {Helper, Toast, SwipeChildFragment} from "cordova-sites/dist/client";
import {ExerciseHelper} from "../../../../Helpers/ExerciseHelper";
import {Point} from "../../../../Geometry/Point";
import {Rect} from "../../../../Geometry/Rect";
import {JsonHelper} from "js-helper/dist/shared/JsonHelper";

/**
 * Fragment zum Bauen einer Mauer
 */
export class BuildWallFragment extends SwipeChildFragment {

    constructor(site) {
        super(site, view);

        //Initialisierung von Variablen
        this._topViewCanvas = null;
        this._history = [];
        this._figures = {};
        this._figuresX = {};
        this._figuresY = {};
        this._figuresZ = {};

        this._nextFigureId = 0;
        this._currentFigure = null;

        this._isRotating = false;

        this._possibleOrientations = [
            []
        ];

        this._ctxTopView = null;

        this._resolutionMultiplier = 10;
        this._gridResolution = new Point(6, 8, 6).multiply(this._resolutionMultiplier);

        this._wallPosition = new Point();
        this._wallDimensions = new Point();

        this._lastOrientation = null;
    }

    async onViewLoaded() {
        let res = await super.onViewLoaded();

        //Initialisierung von View-Variablen
        this._topViewCanvas = this.findBy("#topView");
        this._sideViewCanvas = this.findBy("#sideView");
        this._frontViewCanvas = this.findBy("#frontView");
        this._ctxTopView = this._topViewCanvas.getContext("2d");
        this._ctxSideView = this._sideViewCanvas.getContext("2d");
        this._ctxFrontView = this._frontViewCanvas.getContext("2d");

        //Füge Listeners für UI hinzu
        this._addDragListeners();
        this._addButtonFunctions();

        this.setData(this._data);
        this._draw();
        return res;
    }

    async setElement(element) {
        this._element = element;
        let progress = await ExerciseHelper.getProgressFor(element);

        await this.setData(progress.getData());
        await this.setState(progress.getState());

        progress.timeNeeded = 0;
    }

    async setData(data) {
        this._data = data;

        if (!this._view || !this._topViewCanvas) {
            return;
        }

        //Extra nur == und nicht ===, um string und number zu beachtn
        // Es wird auf andere Werte gesetzt um ein berechnen leichter zu machen
        if (data["exercise-length-wall"] == "86.5") {
            data["exercise-length-wall"] = 84;
        } else if (data["exercise-length-wall"] == "125") {
            data["exercise-length-wall"] = 120;
        }

        this._wallDimensions.set(
            data["exercise-length-wall"],
            data["exercise-breadth-wall"],
            24
        );

        this._wallDimensions.multiply(this._resolutionMultiplier);
        this._wallDimensions.divide(this._gridResolution);

        this._possibleOrientations = BuildWallFragment.ORIENTATIONS[this._wallDimensions.x];

        this._drawGrid();
    }

    async setState(state) {
        if (state.figures) {
            let highestId = -1;
            let currentPosY = 0;

            //Füge bereits vorhandene Figuren hinzue
            Object.keys(state.figures).forEach(figureId => {
                highestId = Math.max(figureId, highestId);
                let figure = state.figures[figureId];
                currentPosY = Math.max(currentPosY, figure.rect.p1.y);
                figure.rect = new Rect(new Point(figure.rect.p1.x, figure.rect.p1.y, figure.rect.p1.z), new Point(figure.rect.p2.x, figure.rect.p2.y, figure.rect.p2.z))

                this._figures[figure.id] = figure;

                this._figuresX[figure.rect.p1.x] = Helper.nonNull(this._figuresX[figure.rect.p1.x], []);
                this._figuresY[figure.rect.p1.y] = Helper.nonNull(this._figuresY[figure.rect.p1.y], []);
                this._figuresZ[figure.rect.p1.z] = Helper.nonNull(this._figuresZ[figure.rect.p1.z], []);
                this._figuresX[figure.rect.p1.x].push(figure.id);
                this._figuresY[figure.rect.p1.y].push(figure.id);
                this._figuresZ[figure.rect.p1.z].push(figure.id);
            });
            this._wallPosition.y = currentPosY + 1;
            this._nextFigureId = highestId + 1;
        }
    }

    _addDragListeners() {
        //Drag function for all the objects for BROWSER platform
        if (device.platform === 'browser') {
            this.findBy(".draggable", true).forEach(element => {
                element.addEventListener("dragstart", e => {
                    this._currentFigure = {
                        type: e.target.id,
                        rect: new Rect(),
                        cementFaces: [],
                    };
                });
            });

            this._topViewCanvas.addEventListener("drop", e => {
                this._addStone();
            });

            //Allow dropping
            this._topViewCanvas.addEventListener("dragover", e => {
                e.preventDefault();

                let canvasRect = this._topViewCanvas.getBoundingClientRect();

                let previewPosition = new Point(e.clientX - canvasRect.left, 0, e.clientY - canvasRect.top)
                    .divide(new Point(this._topViewCanvas.offsetWidth, 1, this._topViewCanvas.offsetHeight))
                    .multiply(this._wallDimensions)
                    .intval().setY(this._wallPosition.y);

                this._preview(previewPosition);
            });

            this._topViewCanvas.addEventListener("dragleave", e => {
                if (this._currentFigure) {
                    this._currentFigure.draw = false;
                    this._draw();
                }
            });

        } else {
            //Handys, besonders iPhones haben keine dragfunktion, arbeite mit touches
            this.findBy(".draggable", true).forEach(element => {
                element.addEventListener("touchstart", e => {
                    // console.log("start", e);
                    this._currentFigure = {
                        type: e.target.id,
                        rect: new Rect(),
                        cementFaces: [],
                    };
                });

                element.addEventListener("touchend", e => {

                    let touches = e.changedTouches;
                    if (touches.length === 1) {
                        this._addStone();
                        e.preventDefault();
                    }
                });

                element.addEventListener("touchmove", e => {
                    let touches = e.changedTouches;
                    if (touches.length === 1) {
                        let clientX = touches[0].clientX;
                        let clientY = touches[0].clientY;

                        let canvasRect = this._topViewCanvas.getBoundingClientRect();

                        let position = new Point(clientX - canvasRect.left, 0, clientY - canvasRect.top)
                            .divide(new Point(this._topViewCanvas.offsetWidth, 1, this._topViewCanvas.offsetHeight))
                            .multiply(this._wallDimensions)
                            .intval().setY(this._wallPosition.y);
                        this._preview(position);
                        e.preventDefault();
                    }
                });
            });
        }
    }

    _addButtonFunctions() {
        let stonePanel = this.findBy("#stonePanel");

        this.findBy("#cement").addEventListener("click", e => {
            this._addJoints();
        });
        this.findBy("#rotate").addEventListener("click", e => {
            this._isRotating = !this._isRotating;
            if (this._isRotating) {
                stonePanel.classList.add("rotated");
            } else {
                stonePanel.classList.remove("rotated");
            }
        });
        this.findBy("#validate").addEventListener("click", e => {
            if (this._checkStones() === 1) {
                this._nextRow();
            }
        });
        this.findBy("#revert").addEventListener("click", e => {
            this._revertLastAction();
        });
    }

    _checkStones() {

        //Checke, ob irgendwo Zement fehlt
        let hasFiguresWithoutCement = Object.values(this._figures).some(figure => {
            return (
                figure.cementFaces.indexOf(0) === -1
                || figure.cementFaces.indexOf(1) === -1
                || figure.cementFaces.indexOf(2) === -1
                || figure.cementFaces.indexOf(3) === -1
            )
        });
        if (hasFiguresWithoutCement) {
            new Toast("Not every stone has cement!").show();
            return -1;
        }

        //checke, ob noch Orientationen möglich sind
        let figureRects = [];
        Object.values(this._figures).forEach(f => {
            if (f.rect.p1.y === this._wallPosition.y) {
                figureRects.push(new Rect(f.rect.p1.copy().setY(0), f.rect.p2.copy().setY(1)))
            }
        });
        let orientationsPossible = [];
        this._possibleOrientations.forEach((orientation, index) => {
            if (figureRects.every(figureRect => {
                return orientation.some(rect => {
                    return figureRect.equals(rect);
                })
            })) {
                orientationsPossible.push(index);
            }
        });

        if (orientationsPossible.length === 0) {
            //Wenn keine Orientation möglich ist, gebe einen Fehler aus
            new Toast("No orientation is correct!").show();
            return -1;
        } else if (orientationsPossible.length === 1 && orientationsPossible[0] === this._lastOrientation) {
            //Wenn die selbe Orientation nocheinmal gebaut wurde, gebe ebenfalls einen Fehler aus
            new Toast("You cannot build your last orientation again!").show();
            return -1;
        }

        //Entferne die letzte Orientation
        let index = orientationsPossible.indexOf(this._lastOrientation);
        if (index >= 0) {
            orientationsPossible.splice(index, 1);
        }

        //Checke, ob eine Orientation bereits gebaut wurde
        if (orientationsPossible.some(orientationIndex => {
            if (this._possibleOrientations[orientationIndex].every(orientationRect => {
                return figureRects.some(figureRect => {
                    return figureRect.equals(orientationRect);
                });
            })) {
                //Wurde gebaut, setze es
                this._lastOrientation = orientationIndex;
                return true;
            }
            return false;
        })) {
            //1 bedeutet, Orientationw urde zuende gebaut, Aufgabe ist beendet
            return 1;
        } else {
            //Orientationen sind noch möglich, aber noch keine wurde zuende gebaut
            new Toast("orientations are possible").show();
            return 0;
        }
    }

    /**
     * Fügt den Zement hinzu
     */
    _addJoints() {
        let changedFigures = {};

        Object.values(this._figures).forEach(figure => {
            let newFaces = [0, 1, 2, 3];

            //Finde die Seiten heraus, die keinen Schnittpunkt mit anderen Figuren haben
            Object.values(this._figures).forEach(f => {
                if (f === figure) {
                    return;
                }
                let adjacentFace = figure.rect.getAdjacentFace(f.rect);
                if (adjacentFace !== null) {
                    //It is possible that one face is adjacent to two or more stones. Therefore only remove face, if it is not removed already
                    let index = newFaces.indexOf(adjacentFace);
                    if (index !== -1) {
                        newFaces.splice(index, 1);
                    }
                }
            });

            //Filtere die Seiten, die bereits Cement haben
            figure.cementFaces.forEach(c => {
                let index = newFaces.indexOf(c);
                if (index !== -1) {
                    newFaces.splice(index, 1);
                }
            });

            //Füge neue Seiten mit Zement hinzu und speichere vorherige Zementseiten in changedFigures
            if (newFaces.length > 0) {
                //DeepCopy, ansonsten wird der Wert verändert, wenn neue Elemente hinzugefügt werden
                changedFigures[figure.id] = JsonHelper.deepCopy(figure.cementFaces);
                figure.cementFaces.push(...newFaces);
            }
        });

        //Speichere für die Historie, welche Seiten bei welchen Figuren bereits Zement hatten
        if (Object.keys(changedFigures).length > 0) {
            this._history.push({
                "action": "joints",
                "figuresBefore": changedFigures
            });

            this._draw();
        }
    }

    async _nextRow() {
        this._wallPosition.addY(1);
        if (this._wallPosition.y === this._wallDimensions.y) {
            new Toast("done!").show();
        } else {
            new Toast("next row").show();
        }
        this._draw();

        //save current state
        let progress = await ExerciseHelper.getProgressFor(this._element);
        progress.state["figures"] = this._figures;
        await this.getSite()._saveExercise(progress);
    }

    _revertLastAction() {
        if (this._history.length === 0) {
            return;
        }

        let lastHistoryEntry = this._history[this._history.length - 1];
        this._history.splice(this._history.length - 1, 1);

        switch (lastHistoryEntry.action) {
            case "add": {
                delete this._figures[lastHistoryEntry["figureId"]];
                break;
            }
            case "joints": {
                Object.keys(lastHistoryEntry["figuresBefore"]).forEach(id => {
                    this._figures[id].cementFaces = lastHistoryEntry["figuresBefore"][id];
                });
                break;
            }
        }

        this._draw();
    }

    _preview(position) {
        if (!this._currentFigure) {
            return;
        }

        let stoneDimensions = this._getDimensionsFor(this._currentFigure.type);

        position.substract(stoneDimensions.copy().divide(2).intval());
        let stoneRect = new Rect(position, position.copy().add(stoneDimensions));

        this._currentFigure.rect = stoneRect;
        this._currentFigure.draw = true;
        this._currentFigure.rotated = this._isRotating;

        let wallRect = new Rect(new Point, this._wallDimensions);
        if (wallRect.isInside(stoneRect.p1) && wallRect.isInside(stoneRect.p2) && !this._isColliding(stoneRect)) {
            this._currentFigure.color = "rgba(0,255,0,0.5)";
        } else {
            this._currentFigure.color = "rgba(255,0,0,0.51)";
        }

        this._draw();
    }

    _addStone() {
        let figure = this._currentFigure;
        delete figure.color;

        this._currentFigure = null;
        let wallRect = new Rect(new Point(), this._wallDimensions);

        //Wenn Stein innerhalb der Mauer und nicht in Kollision mit einem anderen Stein ist,
        if (wallRect.isInside(figure.rect.p1) && wallRect.isInside(figure.rect.p2) && !this._isColliding(figure.rect)) {

            //Setze bei der Figur die bereits zementierten Seiten
            Object.values(this._figures).forEach(f => {
                let adjacentFace = f.rect.getAdjacentFace(figure.rect);
                if (f.cementFaces.indexOf(adjacentFace) !== -1) {
                    figure.cementFaces.push((adjacentFace + 2) % 4);
                }
            });

            //Setze die Figur bei den aktuellen Figuren
            figure.id = this._nextFigureId;
            this._figures[this._nextFigureId] = figure;
            this._nextFigureId++;

            //Zum Überprüfen von Kollisionen werden Referenzen (ids) auf Figuren für die entsprechenden Koordinaten
            // gespeichert. Speichere hier die Figuten
            this._figuresX[figure.rect.p1.x] = Helper.nonNull(this._figuresX[figure.rect.p1.x], []);
            this._figuresY[figure.rect.p1.y] = Helper.nonNull(this._figuresY[figure.rect.p1.y], []);
            this._figuresZ[figure.rect.p1.z] = Helper.nonNull(this._figuresZ[figure.rect.p1.z], []);
            this._figuresX[figure.rect.p1.x].push(figure.id);
            this._figuresY[figure.rect.p1.y].push(figure.id);
            this._figuresZ[figure.rect.p1.z].push(figure.id);

            //Setze die History
            let newHistoryEntry = {
                "action": "add",
                "figureId": figure.id,
            };
            this._history.push(newHistoryEntry);

            this._draw();

            return true;
        } else {
            new Toast("this stone does not fit!").show();
            this._draw();
            return false;
        }
    }

    _draw() {
        this._ctxTopView.clearRect(0, 0, this._wallDimensions.x * this._gridResolution.x, this._wallDimensions.z * this._gridResolution.z);
        this._ctxFrontView.clearRect(0, 0, this._wallDimensions.x * this._gridResolution.x, this._wallDimensions.y * this._gridResolution.y);
        this._ctxSideView.clearRect(0, 0, this._wallDimensions.z * this._gridResolution.z, this._wallDimensions.y * this._gridResolution.y);
        this._drawGrid();

        this._drawTopView();
        this._drawSideView();
        this._drawFrontView();
    }

    _drawTopView() {
        if (this._figuresY[this._wallPosition.y]) {
            this._figuresY[this._wallPosition.y].forEach(figureId => {
                this._drawFigure("top", this._figures[figureId]);
            });
        }

        if (Helper.isNotNull(this._currentFigure) && this._currentFigure.draw !== false) {
            this._drawFigure("top", this._currentFigure);
        }
    }

    _drawSideView() {
        let sortedX = Object.keys(this._figuresX).map(v => parseInt(v)).sort((a, b) => a - b);
        sortedX.forEach(x => {
            this._figuresX[x].forEach(figureId => {
                this._drawFigure("side", this._figures[figureId]);
            });
        })

        if (Helper.isNotNull(this._currentFigure) && this._currentFigure.draw !== false) {
            this._drawFigure("side", this._currentFigure);
        }
    }

    _drawFrontView() {
        let sortedY = Object.keys(this._figuresY).map(v => parseInt(v)).sort((a, b) => a - b);
        sortedY.forEach(y => {
            this._figuresY[y].forEach(figureId => {
                this._drawFigure("front", this._figures[figureId]);
            });
        });

        if (Helper.isNotNull(this._currentFigure) && this._currentFigure.draw !== false) {
            this._drawFigure("front", this._currentFigure);
        }
    }

    _drawGrid() {

        //set canvas dimensions
        this._topViewCanvas.width = this._wallDimensions.x * this._gridResolution.x;
        this._topViewCanvas.height = this._wallDimensions.z * this._gridResolution.z;

        this._frontViewCanvas.width = this._wallDimensions.x * this._gridResolution.x;
        this._frontViewCanvas.height = this._wallDimensions.y * this._gridResolution.y;

        this._sideViewCanvas.width = this._wallDimensions.z * this._gridResolution.z;
        this._sideViewCanvas.height = this._wallDimensions.y * this._gridResolution.y;

        this._ctxTopView.beginPath();
        this._ctxTopView.strokeStyle = "#373737";
        for (let x = 1; x < this._wallDimensions.x; x++) {
            this._ctxTopView.moveTo(x * this._gridResolution.x, 0);
            this._ctxTopView.lineTo(x * this._gridResolution.x, this._wallDimensions.z * this._gridResolution.z);
        }

        for (let z = 1; z < this._wallDimensions.y; z++) {
            this._ctxTopView.moveTo(0, z * this._gridResolution.z);
            this._ctxTopView.lineTo(this._wallDimensions.x * this._gridResolution.x, z * this._gridResolution.z);
        }
        this._ctxTopView.lineWidth = 1;
        this._ctxTopView.stroke();
    }

    _drawFigure(ctxType, figure) {
        if (Helper.isNull(figure)) {
            return;
        }

        let ctx = null;
        let rect = figure.rect;
        switch (ctxType) {
            case "top": {
                ctx = this._ctxTopView;
                rect = new Rect(new Point(rect.p1.x, rect.p1.z), new Point(rect.p2.x, rect.p2.z))
                    .multiply(new Point(this._gridResolution.x, this._gridResolution.z))
                break;
            }
            case "front": {
                ctx = this._ctxFrontView;
                rect = new Rect(new Point(rect.p1.x, this._wallDimensions.y - rect.p1.y), new Point(rect.p2.x, this._wallDimensions.y - rect.p2.y))
                    .multiply(new Point(this._gridResolution.x, this._gridResolution.y))
                break;
            }
            case "side": {
                ctx = this._ctxSideView;
                rect = new Rect(new Point(rect.p1.z, this._wallDimensions.y - rect.p1.y), new Point(rect.p2.z, this._wallDimensions.y - rect.p2.y))
                    .multiply(new Point(this._gridResolution.z, this._gridResolution.y))
                break;
            }
        }

        switch (figure.type) {
            case "whole-stone": {
                this._drawWholeStone(ctx, rect, figure.cementFaces, figure.rotated, figure.color);
                break;
            }
            case "three-quarter-stone": {
                this._drawThreeQuarterStone(ctx, rect, figure.cementFaces, figure.rotated, figure.color);
                break;
            }
            case "half-stone": {
                this._drawHalfStone(ctx, rect, figure.cementFaces, figure.rotated, figure.color);
                break;
            }
            case "quarter-stone": {
                this._drawQuarterStone(ctx, rect, figure.cementFaces, figure.rotated, figure.color);
                break;
            }
        }
    }

    _drawWholeStone(ctx, rect, cementFaces, rotated, color, cementColor) {
        color = Helper.nonNull(color, "black");
        cementColor = Helper.nonNull(cementColor, "#9b9b9b");

        const strokeWidth = 6;
        const strokeWidthCement = 9;

        //topView
        // if (figure.rect.p1.y === this._wallPosition.y) {
        ctx.fillStyle = "white";
        ctx.fillRect(rect.p1.x, rect.p1.y, (rect.p2.x - rect.p1.x), (rect.p2.y - rect.p1.y))

        ctx.beginPath();
        ctx.lineWidth = strokeWidth;
        ctx.strokeStyle = color;
        ctx.moveTo(rect.p1.x, rect.p1.y);
        ctx.lineTo(rect.p1.x, rect.p2.y);
        if (cementFaces.indexOf(0) !== -1) {
            ctx.strokeStyle = cementColor;
            ctx.lineWidth = strokeWidthCement;
        }
        ctx.stroke();

        ctx.beginPath();
        ctx.lineWidth = strokeWidth;
        ctx.strokeStyle = color;
        ctx.moveTo(rect.p1.x, rect.p2.y);
        ctx.lineTo(rect.p2.x, rect.p2.y);
        if (cementFaces.indexOf(1) !== -1) {
            ctx.strokeStyle = cementColor;
            ctx.lineWidth = strokeWidthCement;
        }
        ctx.stroke();

        ctx.beginPath();
        ctx.lineWidth = strokeWidth;
        ctx.strokeStyle = color;
        ctx.moveTo(rect.p2.x, rect.p2.y);
        ctx.lineTo(rect.p2.x, rect.p1.y);
        if (cementFaces.indexOf(2) !== -1) {
            ctx.strokeStyle = cementColor;
            ctx.lineWidth = strokeWidthCement;
        }
        ctx.stroke();

        ctx.beginPath();
        ctx.lineWidth = strokeWidth;
        ctx.strokeStyle = color;
        ctx.moveTo(rect.p2.x, rect.p1.y);
        ctx.lineTo(rect.p1.x, rect.p1.y);
        if (cementFaces.indexOf(3) !== -1) {
            ctx.strokeStyle = cementColor;
            ctx.lineWidth = strokeWidthCement;
        }
        ctx.stroke();
    }

    _drawThreeQuarterStone(ctx, rect, cementFaces, rotated, color, cementColor) {
        color = Helper.nonNull(color, "black");
        this._drawWholeStone(ctx, rect, cementFaces, rotated, color, cementColor);

        ctx.beginPath();
        ctx.strokeStyle = color;
        if (rotated === true) {
            ctx.moveTo(rect.p1.x, rect.p1.y);
            ctx.lineTo(rect.p2.x, rect.p2.y);
        } else {
            ctx.moveTo(rect.p1.x, rect.p2.y);
            ctx.lineTo(rect.p2.x, rect.p1.y);
        }
        ctx.stroke();
    }

    _drawHalfStone(ctx, rect, cementFaces, rotated, color, cementColor) {
        color = Helper.nonNull(color, "black");

        this._drawThreeQuarterStone(ctx, rect, cementFaces, rotated, color, cementColor);

        ctx.beginPath();
        ctx.strokeStyle = color;
        if (rotated === true) {
            ctx.moveTo(rect.p1.x, rect.p2.y);
            ctx.lineTo(rect.p2.x, rect.p1.y);
        } else {
            ctx.moveTo(rect.p1.x, rect.p1.y);
            ctx.lineTo(rect.p2.x, rect.p2.y);
        }
        ctx.stroke();
    }

    _drawQuarterStone(ctx, rect, cementFaces, rotated, color, cementColor) {
        color = Helper.nonNull(color, "black");

        this._drawWholeStone(ctx, rect, cementFaces, rotated, color, cementColor);

        //topView
        ctx.beginPath();
        ctx.strokeStyle = color;
        if (rotated === true) {
            ctx.moveTo((rect.p1.x + (rect.p2.x - rect.p1.x) * 0.35), (rect.p1.y + (rect.p2.y - rect.p1.y) * 0.5));
            ctx.lineTo((rect.p1.x + (rect.p2.x - rect.p1.x) * 0.65), (rect.p1.y + (rect.p2.y - rect.p1.y) * 0.5));

        } else {
            ctx.moveTo((rect.p1.x + (rect.p2.x - rect.p1.x) / 2), (rect.p1.y + (rect.p2.y - rect.p1.y) * 0.35));
            ctx.lineTo((rect.p1.x + (rect.p2.x - rect.p1.x) / 2), (rect.p1.y + (rect.p2.y - rect.p1.y) * 0.65));
        }
        ctx.stroke();
    }

    _getDimensionsFor(elementId) {

        let dimension = new Point();
        switch (elementId) {
            case "whole-stone": {
                dimension = new Point(4, 1, 2);
                break;
            }
            case "three-quarter-stone": {
                dimension = new Point(3, 1, 2);
                break;
            }
            case "half-stone": {
                dimension = new Point(2, 1, 2);
                break;
            }
            case "quarter-stone": {
                dimension = new Point(1, 1, 2);
                break;
            }
        }

        if (this._isRotating) {
            dimension = new Point(dimension.z, dimension.y, dimension.x)
        }
        return dimension;
    }

    _isColliding(rect) {
        return Object.values(this._figures).some(figure => figure.rect.isOverlapping(rect));
    }
}


BuildWallFragment.ORIENTATIONS = {
    14: [
        [
            new Rect(new Point(0, 0, 0), new Point(2, 1, 4)),
            new Rect(new Point(2, 0, 0), new Point(4, 1, 4)),
            new Rect(new Point(4, 0, 0), new Point(6, 1, 4)),
            new Rect(new Point(6, 0, 0), new Point(8, 1, 4)),
            new Rect(new Point(8, 0, 0), new Point(10, 1, 4)),
            new Rect(new Point(10, 0, 0), new Point(12, 1, 4)),
            new Rect(new Point(12, 0, 0), new Point(14, 1, 4)),
        ],
        [
            new Rect(new Point(0, 0, 0), new Point(3, 1, 2)),
            new Rect(new Point(0, 0, 2), new Point(3, 1, 4)),
            new Rect(new Point(3, 0, 0), new Point(5, 1, 4)),
            new Rect(new Point(5, 0, 0), new Point(7, 1, 4)),
            new Rect(new Point(7, 0, 0), new Point(9, 1, 4)),
            new Rect(new Point(9, 0, 0), new Point(11, 1, 4)),
            new Rect(new Point(11, 0, 0), new Point(14, 1, 2)),
            new Rect(new Point(11, 0, 2), new Point(14, 1, 4)),
        ],
        [
            new Rect(new Point(0, 0, 0), new Point(3, 1, 2)),
            new Rect(new Point(0, 0, 2), new Point(3, 1, 4)),
            new Rect(new Point(3, 0, 0), new Point(7, 1, 2)),
            new Rect(new Point(3, 0, 2), new Point(7, 1, 4)),
            new Rect(new Point(7, 0, 0), new Point(11, 1, 2)),
            new Rect(new Point(7, 0, 2), new Point(11, 1, 4)),
            new Rect(new Point(11, 0, 0), new Point(14, 1, 2)),
            new Rect(new Point(11, 0, 2), new Point(14, 1, 4)),
        ],
    ],
    20: [
        [
            new Rect(new Point(0, 0, 0), new Point(2, 1, 4)),
            new Rect(new Point(2, 0, 0), new Point(4, 1, 4)),
            new Rect(new Point(4, 0, 0), new Point(6, 1, 4)),
            new Rect(new Point(6, 0, 0), new Point(8, 1, 4)),
            new Rect(new Point(8, 0, 0), new Point(10, 1, 4)),
            new Rect(new Point(10, 0, 0), new Point(12, 1, 4)),
            new Rect(new Point(12, 0, 0), new Point(14, 1, 4)),
            new Rect(new Point(14, 0, 0), new Point(16, 1, 4)),
            new Rect(new Point(16, 0, 0), new Point(18, 1, 4)),
            new Rect(new Point(18, 0, 0), new Point(20, 1, 4)),
        ],
        [
            new Rect(new Point(0, 0, 0), new Point(3, 1, 2)),
            new Rect(new Point(0, 0, 2), new Point(3, 1, 4)),
            new Rect(new Point(3, 0, 0), new Point(7, 1, 2)),
            new Rect(new Point(3, 0, 2), new Point(7, 1, 4)),
            new Rect(new Point(7, 0, 0), new Point(11, 1, 2)),
            new Rect(new Point(7, 0, 2), new Point(11, 1, 4)),
            new Rect(new Point(11, 0, 0), new Point(15, 1, 2)),
            new Rect(new Point(11, 0, 2), new Point(15, 1, 4)),
            new Rect(new Point(15, 0, 0), new Point(17, 1, 4)),
            new Rect(new Point(17, 0, 0), new Point(20, 1, 2)),
            new Rect(new Point(17, 0, 2), new Point(20, 1, 4)),
        ],
        [
            new Rect(new Point(0, 0, 0), new Point(3, 1, 2)),
            new Rect(new Point(0, 0, 2), new Point(3, 1, 4)),
            new Rect(new Point(3, 0, 0), new Point(5, 1, 4)),
            new Rect(new Point(5, 0, 0), new Point(9, 1, 2)),
            new Rect(new Point(5, 0, 2), new Point(9, 1, 4)),
            new Rect(new Point(9, 0, 0), new Point(13, 1, 2)),
            new Rect(new Point(9, 0, 2), new Point(13, 1, 4)),
            new Rect(new Point(13, 0, 0), new Point(17, 1, 2)),
            new Rect(new Point(13, 0, 2), new Point(17, 1, 4)),
            new Rect(new Point(17, 0, 0), new Point(20, 1, 2)),
            new Rect(new Point(17, 0, 2), new Point(20, 1, 4)),
        ],
    ]
};
