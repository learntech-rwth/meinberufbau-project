import view from "../../../../../html/Fragments/ExerciseFragments/Exercises/SubFragments/calculationFragment2.html";
import {Form, Helper, SwipeChildFragment, Toast, Translator} from "cordova-sites/dist/client";
import {ExerciseHelper} from "../../../../Helpers/ExerciseHelper";
import {JsonHelper} from "js-helper/dist/shared/JsonHelper";

export class CalculationFragment extends SwipeChildFragment {

    constructor(site, objects) {
        super(site, view);
        this._data = null;
        this._element = null;
        this._objects = Helper.nonNull(objects, {});
    }

    async onSwipeLeft() {
    }

    async setElement(element, objects) {
        this._objects = Helper.nonNull(objects, this._objects);
        this._element = element;

        let progress = await ExerciseHelper.getProgressFor(this._element);
        await this.setData(progress.getData());
        await this.setState(progress.state);
    }

    async setData(data) {
        Helper.objectForEach(this._objects, (length, key) => {
            data["single-" + key + "-length"] = length;
            data["combined-" + key + "-length"] = length * data["num-" + key];
        });
        this._data = data;
    }

    async setState(state) {
        await this._viewPromise;

        //Setze die Werte in den Formularen
        Object.keys(state).forEach(name => {
            let inputElement = this.findBy("[name='" + name + "']");

            if (inputElement) {
                inputElement.value = state[name];
            }
        });

        //Sende Formulare ab, wenn bereits Spielstand vorhanden, um die Listener zu triggern
        let types = Object.keys(this._objects);
        if (Helper.isNotNull(this._numberInputForm) && types.length >= 1) {
            if (state["num-" + types[0]] && state["num-" + types[0]].trim() !== "") {
                this._numberInputForm.doSubmit();
            }
            this._multiplierForm.doSubmit();
            Object.keys(this._addtionRowForms).forEach(formKey => {
                this._addtionRowForms[formKey].doSubmit();
            });
        }
    }

    async onViewLoaded() {
        let res = super.onViewLoaded();

        this._handleNumberSection();
        this._handleMultiplierSection();
        this._handleAdditionSection();

        if (this._element) {
            let progress = await ExerciseHelper.getProgressFor(this._element);
            await this.setState(progress.state);
        }

        //Nummerneingabe mit Unit-Select müssen andere Werte anzeigen, wenn sich die Selectbox ändert
        let selects = this.findBy(".number-unit-input select", true);
        selects.forEach(select => {
            select.addEventListener("change", () => {
                selects.forEach(otherSelect => {
                    if (otherSelect !== select && otherSelect.parentElement.querySelector("input").value.trim() === "") {
                        otherSelect.value = select.value;
                    }
                });
            });
        });

        return res;
    }

    /**
     * Checkt, ob ein Wert richtig ist. Gibt eine Fehlermeldung bei einem falschen Wert in Form eines
     * Strings zurück. Ansonsten wird ein leerstring zurückgegeben
     *
     * @param name
     * @param value
     * @param notTranslated
     * @returns {string|string|any}
     * @private
     */
    _checkValueFor(name, value, notTranslated) {
        notTranslated = Helper.nonNull(notTranslated, false);

        if (Helper.isNotNull(this._data) && Helper.isNotNull(this._data[name])) {
            if (typeof value === "string" && value.trim() === "") {
                ExerciseHelper.getProgressFor(this._element).then(progress => {
                    ExerciseHelper.addWrongAnswer(progress, name, value, this._data[name]);
                });
                return notTranslated ? "value is required" : Translator.translate("value is required");
            } else if (typeof value === "number") {
                value = parseFloat(value.toPrecision(10));
            }

            //Use unsafe comparsion, because data[name] can be number or string and value can also be number or string
            if (this._data[name] != value) {
                ExerciseHelper.getProgressFor(this._element).then(progress => {
                    ExerciseHelper.addWrongAnswer(progress, name, value, this._data[name]);
                });
                return notTranslated ? "this number is wrong" : Translator.translate("this number is wrong");
            }
        }
        return "";
    }

    _handleNumberSection() {
        let multiplierSection = this.findBy(".multiplier-section");

        let numberFormElem = this.findBy(".number-stones-form");

        let numberTemplateElement = this.findBy(".number-element-template");
        numberTemplateElement.remove();
        numberTemplateElement.classList.remove("number-element-template");
        let numberElementContainer = this.findBy(".number-stones-container");

        //Setzt die numberElement-Inputs für das Formular
        let types = Object.keys(this._objects);
        types.forEach(type => {
            let numberElement = numberTemplateElement.cloneNode(true);
            numberElement.querySelector(".stone-name").appendChild(Translator.makePersistentTranslation(type));
            numberElement.querySelector(".stone-number").name = "num-" + type;
            numberElementContainer.appendChild(numberElement);
        });

        //Setzt das Formular, führt Callback aus, sobald das Formular abgesendet wird.
        this._numberInputForm = new Form(numberFormElem, async () => {
            this.getSite().showLoadingSymbol();

            //Setzt und Checkt auf Fortschritt
            let progress = await ExerciseHelper.getProgressFor(this._element);
            let state = JsonHelper.deepCopy(progress.state);
            this.findBy(".number-stones-form input", true).forEach(input => {
                input.setAttribute("disabled", true);
                state[input.name] = input.value;
            });

            //Gab Fortschritt, setze ihn und speichere
            if (!JsonHelper.deepEqual(state, progress.state)) {
                progress.state = state;
                await this.getSite()._saveExercise(progress);
            }

            //Aktiviert die nächste Sektion
            this.getSite().removeLoadingSymbol();
            multiplierSection.classList.remove("hidden");
            multiplierSection.scrollIntoView(true);
        });

        //Fügt dem Number-Input ein Formular hinzu
        let triedTimes = 0;
        let lastValues = {};
        this._numberInputForm.addValidator(async values => {
            for (let k in values) {
                let checkedReason = this._checkValueFor(k, values[k], true);
                if (values[k].trim() === "" || checkedReason !== "") {
                    if (!Helper.deepEqual(values, lastValues)) {
                        triedTimes++;
                    }

                    //Gibt einen genauren Fehler aus, wenn man schon 5 unterschiedliche, falsche Angaben gemacht hat
                    if (triedTimes > 5) {
                        let errors = {};
                        errors[k] = checkedReason;
                        return errors;
                    } else {
                        new Toast("At least one number is wrong").show();
                    }
                    lastValues = values;
                    return false;
                }
            }
            return true;
        });
    }

    _handleMultiplierSection() {
        let multiplierTemplate = this.findBy(".multiplier-element-template");
        multiplierTemplate.remove();
        multiplierTemplate.classList.remove("multiplier-element-template");

        let types = Object.keys(this._objects);

        let multiplierFormElem = this.findBy(".multiplier-form");

        //Füge HTML-Elemente der Multiplier-Sektion hinzu
        types.forEach(type => {
            if (this._data["num-" + type] > 0) {
                let multiplierElement = multiplierTemplate.cloneNode(true);
                multiplierElement.querySelector(".stone-type").appendChild(Translator.makePersistentTranslation(type));
                multiplierElement.dataset["type"] = type;
                multiplierElement.querySelector(".number-stones").name = "multiplier-num-" + type;
                multiplierElement.querySelector(".single-stone-length .number").name = "length-" + type;
                multiplierElement.querySelector(".single-stone-length .unit").name = "unit-" + type;
                multiplierElement.querySelector(".combined-stone-length .number").name = "combined-length-" + type;
                multiplierElement.querySelector(".combined-stone-length .unit").name = "combined-unit-" + type;
                multiplierFormElem.appendChild(multiplierElement);
            }
        });

        let multiplierForm = new Form(multiplierFormElem, async () => {
            this.getSite().showLoadingSymbol();

            //Checke auf Updates und speichere diese
            let progress = await ExerciseHelper.getProgressFor(this._element);
            let state = JsonHelper.deepCopy(progress.state);
            multiplierFormElem.querySelectorAll("input, select").forEach(input => {
                input.setAttribute("disabled", true);
                state[input.name] = input.value;
            });

            //Speichere Updates, wenn welche vorhanden
            if (!JsonHelper.deepEqual(state, progress.state)) {
                progress.state = state;
                await this.getSite()._saveExercise(progress);
            }

            //Aktiviere nächste Sektion (Addition
            this.getSite().removeLoadingSymbol();
            let additionSection = this.findBy(".addition-section");
            additionSection.classList.remove("hidden");
            additionSection.scrollIntoView(true);
        });

        //Checke, ob die Ergebnisse nicht leer sind
        multiplierForm.addValidator(values => {
            for (let k in values) {
                if (values[k].trim() === "") {
                    return false;
                }
            }
            return true;
        });

        //Füge diesen Validator an die Input und Select-Elemente hinzu
        let validationCallback = e => {
            let multiplierElement = e.target.closest(".multiplier-element");
            let type = multiplierElement.dataset["type"];

            let input = e.target;

            let grandparentElement = input.parentElement.parentElement;
            if (grandparentElement.matches(".single-stone-length")) {
                let unitInput = grandparentElement.querySelector(".unit");
                let numberInput = grandparentElement.querySelector(".number");

                //Überprüfe und Setze Fehler
                numberInput.setCustomValidity(this._checkValueFor("single-" + type + "-length", numberInput.value * unitInput.value * (this._objects[type] < 0 ? -1 : 1)));
            } else if (grandparentElement.matches(".combined-stone-length")) {
                let unitInput = grandparentElement.querySelector(".unit");
                let numberInput = grandparentElement.querySelector(".number");

                //Überprüfe und Setze Fehler
                numberInput.setCustomValidity(this._checkValueFor("combined-" + type + "-length", numberInput.value * unitInput.value * (this._objects[type] < 0 ? -1 : 1)));
            } else {

                //Überprüfe und Setze Fehler
                input.setCustomValidity(this._checkValueFor("num-" + type, input.value));
            }
            multiplierForm.doSubmit();
        };

        //Füge den Validator den Elementen hinzu
        multiplierFormElem.querySelectorAll("input").forEach(input => {
            input.addEventListener("keydown", (e) => {
                let elem = e.target;
                clearTimeout(elem._keydownTimeout);
                elem._keydownTimeout = setTimeout(() => {
                    validationCallback(e);
                }, 500);
            });
        });
        multiplierFormElem.querySelectorAll("select, input").forEach(select => {
            select.addEventListener("change", validationCallback);
        });

        this._multiplierForm = multiplierForm;
    }

    _handleAdditionSection() {
        let additionRowTemplate = this.findBy(".addition-row-template");
        additionRowTemplate.remove();
        additionRowTemplate.classList.remove(".addition-row-template");

        let additionContainer = this.findBy(".addition-container");

        let rowForms = {};

        let sum = 0;
        let numRow = 0;
        let types = Object.keys(this._objects);
        types.forEach(type => {
            if (this._data["num-" + type] > 0) {
                numRow++;
                sum += this._data["combined-" + type + "-length"];
                this._data["sum-" + type] = sum;

                //Füge die HTML-Elemente der Seite hinzu
                let additionElement = null;
                if (numRow === 1) {
                    additionElement = additionContainer.querySelector(".first-row");
                    additionElement.querySelector(".name").appendChild(Translator.makePersistentTranslation(type));
                    additionElement.dataset["type"] = type;
                    additionElement.querySelector(".stone-length .number").name = "combined-" + type + "-length";
                    additionElement.querySelector(".stone-length .unit").name = "combined-" + type + "-unit";
                } else {
                    additionElement = additionRowTemplate.cloneNode(true);
                    additionElement.querySelector(".name").appendChild(Translator.makePersistentTranslation(type));
                    additionElement.dataset["type"] = type;
                    additionElement.querySelector(".stone-length .number").name = "combined-" + type + "-length";
                    additionElement.querySelector(".stone-length .unit").name = "combined-" + type + "-unit";
                    additionElement.querySelector(".result .number").name = "sum-" + type;
                    additionElement.querySelector(".result .unit").name = "sum-unit-" + type;

                    if (this._objects[type] < 0) {
                        additionElement.querySelector(".added").innerHTML = "-";
                    }

                    additionContainer.appendChild(additionElement);

                    if (numRow === 2) {
                        additionElement.classList.remove("hidden");
                        additionElement.scrollIntoView(true);
                    }
                }

                //Jede Reihe ist ein eiges Formular
                let currentRow = numRow;
                rowForms[type] = new Form(additionElement, async () => {
                    this.getSite().showLoadingSymbol();

                    let progress = await ExerciseHelper.getProgressFor(this._element);
                    if (currentRow === 2) {
                        //Checke Validität für erste Row, wenn zweite abgesendet wird, da diese alleine
                        // nichts macht
                        if (!await Helper.toArray(rowForms)[0].validate()) {
                            return;
                        } else {

                            //Set Value for first row
                            let state = progress.state;

                            //Disable from first form
                            Helper.toArray(rowForms)[0].getFormElement().querySelectorAll("input, select").forEach(input => {
                                input.setAttribute("disabled", "true");
                                state[input.name] = input.value;
                            });
                            progress.state = state;
                        }
                    }

                    //Set Value for current row
                    let state = JsonHelper.deepCopy(progress.state);
                    additionElement.querySelectorAll("input, select").forEach(input => {
                        input.setAttribute("disabled", "true");
                        state[input.name] = input.value;
                    });
                    if (!JsonHelper.deepEqual(state, progress.state)) {
                        progress.state = state;
                        if (currentRow < Object.values(rowForms).length) {
                            await this.getSite()._saveExercise(progress);
                        }
                    }

                    //Check if done
                    if (currentRow < Helper.toArray(rowForms).length) {
                        //Not done, show next row
                        let nextRow = Helper.toArray(rowForms)[currentRow];
                        nextRow.getFormElement().classList.remove("hidden");
                        nextRow.getFormElement().scrollIntoView(true);

                        if (currentRow === 1) {
                            await nextRow.doSubmit();
                        }
                    } else {
                        //done, show it!
                        let doneSection = this.findBy(".done-section");
                        doneSection.classList.remove("hidden");
                        doneSection.scrollIntoView(true);
                        if (!progress.isDone) {
                            await this.getSite().isDone();
                        }
                    }
                    this.getSite().removeLoadingSymbol();
                });

                //Füge nicht-leer-Validator hinzu
                rowForms[type].addValidator(values => {
                    for (let k in values) {
                        if (values[k].trim() === "") {
                            return false;
                        }
                    }
                    return true;
                });

                if (numRow === 2) {
                    Helper.toArray(rowForms)[0].addValidator(() => {
                        return rowForms[type].validate();
                    })
                }
            }
        });

        //Füge Richtiger-Wert-Validator hinzu
        let validationCallback = (e) => {
            let elem = e.target;

            let additionElement = elem.closest(".addition-element");
            let type = additionElement.dataset["type"];

            let grandparentElement = elem.parentElement.parentElement;
            if (grandparentElement.matches(".stone-length")) {
                let unitInput = grandparentElement.querySelector(".unit");
                let numberInput = grandparentElement.querySelector(".number");
                numberInput.setCustomValidity(this._checkValueFor("combined-" + type + "-length", numberInput.value * unitInput.value * (this._objects[type] < 0 ? -1 : 1)));
            } else if (grandparentElement.matches(".result")) {
                let unitInput = grandparentElement.querySelector(".unit");
                let numberInput = grandparentElement.querySelector(".number");
                numberInput.setCustomValidity(this._checkValueFor("sum-" + type, numberInput.value * unitInput.value));
            }

            rowForms[type].doSubmit();
        };

        additionContainer.querySelectorAll("input").forEach(input => {
            input.addEventListener("keydown", (e) => {
                let elem = e.target;
                clearTimeout(elem._keydownTimeout);
                elem._keydownTimeout = setTimeout(() => {
                    validationCallback(e);
                }, 350);
            });
        });
        additionContainer.querySelectorAll("select, input").forEach(select => {
            select.addEventListener("change", validationCallback);
        });

        this._addtionRowForms = rowForms;
    }
}
