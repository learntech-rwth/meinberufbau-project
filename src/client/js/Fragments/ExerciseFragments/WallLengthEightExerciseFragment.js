import {ExerciseDescriptionFragment} from "./SubFragments/ExerciseDescriptionFragment";
import {CalculationFragment} from "./SubFragments/CalculationFragment/CalculationFragment";
import {SwipeFragment} from "cordova-sites/dist/client";
import {ExerciseHelper} from "../../Helpers/ExerciseHelper";

/**
 * Diese Exercise wird für die Wandköpfe-Aufgabe benutzt.
 */
export class WallLengthEightExerciseFragment extends SwipeFragment {
    constructor(site) {
        super(site);

        this._exerciseDescriptionFragment = new ExerciseDescriptionFragment(this.getSite());

        //CalculationFragment bekommt objekten und Längen für diese Objekte übergeben. In den Daten stehen dann wie viele
        // Objekte von welchem Typen existieren. Dann wird für jedes Element die Länge ausgerechnet und die Gesamtlänge
        // der Mauer.
        this._calculationFragment = new CalculationFragment(this.getSite(), {heads: 12.5, stossfuge: 1});

        this._element = null;

        this.addFragment(this._exerciseDescriptionFragment);
        this.addFragment(this._calculationFragment);
    }

    async setElement(element){
        this._element = element;

        let progress = await ExerciseHelper.getProgressFor(element);
        let data = progress.getData();

        //Setze Daten für Calculation-Fragment. Diese muss den Objektnamen entsprechen.
        // daher entferne exercise- am Anfang
        Object.keys(data).forEach(key => {
            if (key.startsWith("exercise-num")){
                data[key.substr(9)] = data[key];
            }
        });

        data["exercise-understood-exercise-question"] = "Was für ein Maß l ist gesucht?";
        data["exercise-understood-exercise-answer-1"] = "Außenmaß";
        data["exercise-understood-exercise-answer-2"] = "Öffnungsmaß";
        data["exercise-understood-exercise-answer-3"] = "Anbaumaß";
        data["exercise-understood-exercise-answer-4"] = "Innenmaß";

        let objects = {heads: 12.5};


        //Da hier unterschiedliches gewählt werden kann, setze die entsprechenden Daten
        switch(data["mass-to-calculate"]){
            case "aussenmass":{
                objects["stossfuge"] = -1;
                data["num-stossfuge"] = 1;
                data["exercise-understood-exercise-correct"] = "1";
                break;
            }
            case "oeffnungsmass":{
                objects["stossfuge"] = 1;
                data["num-stossfuge"] = 1;
                data["exercise-understood-exercise-correct"] = "2";
                break;
            }
            default: {
                data["exercise-understood-exercise-correct"] = "3";
                break;
            }
        }

        progress.setData(data);

        await this._exerciseDescriptionFragment.setElement(element);
        await this._calculationFragment.setElement(element, objects);
    }

    goBack(){
        return this.previousFragment();
    }

    previousFragment() {
        if (this._activeIndex > 0) {
            super.previousFragment();
        } else {
            this.getSite().goBack();
        }
    }
}
