import {AbstractFragment} from "cordova-sites/dist/client/js/Context/AbstractFragment";
import {Toast} from "cordova-sites/dist/client/js/Toast/Toast";
import {GBPlayer} from "ba-mbb-digitalstorybook/dist/index";
import {Translator} from "cordova-sites/dist/client";
import {ExerciseHelper} from "../../Helpers/ExerciseHelper";
import {FileMedium} from "cordova-sites-easy-sync/dist/shared";
import {ExerciseProgress} from "../../../../shared/model/ExerciseProgress";
import {Exercise} from "../../../../shared/model/Exercise";
import {MyMenuSite} from "../../Sites/MyMenuSite";



const view = require("../../../html/Fragments/ExerciseFragments/Exercises/storybookFragment.html");

/**
 * Fragment zum Anzeigen und Durchspielen eines Gamebooks
 */
export class StorybookFragment extends AbstractFragment {
    private saveLoaded: boolean;
    private storybookPlayer: GBPlayer;
    private progress: ExerciseProgress;
    private exercise: Exercise;
    private arbeitsplanSite: number = null;
    private imgUrls = [];

    /**
     * Echt nicht schöne Methode, aber momentan die schnellste. Sorgt dafür, dass Nodes während des
     * durchspielens den Arbeitsplan oder Materialliste hinzufügen
     * @private
     */
    private static instance: StorybookFragment

    static getInstance() {
        return this.instance;
    }

    constructor(site) {
        super(site, view);
        this.saveLoaded = false;

        //Setze Footer-Listener
        const menuSite = <MyMenuSite>this.getSite();
        menuSite.getFooterFragment().setBackListener(() => this.back());
        menuSite.getFooterFragment().setForwardListener(() => this.forward());
        menuSite.getFooterFragment().setSkipForwardListener(() => this.skipForward());
    }

    onConstruct(constructParameters: any): Promise<any[]> {
        const res = super.onConstruct(constructParameters);

        //Sorgt dafür, dass immer nur eine Instance auf einmal verfügbar ist
        if (StorybookFragment.instance && StorybookFragment.instance !== this) {
            StorybookFragment.instance.getSite().finish();
        }
        StorybookFragment.instance = this;

        (this.getSite() as MyMenuSite).getFooterFragment().setArbeitsplanListener(() => {
            if (this.arbeitsplanSite) {
                this.storybookPlayer.GoToPage(this.arbeitsplanSite);
            } else {
                new Toast("kein arbeitsplan vorhanden").show();
            }
        });



        (this.getSite() as MyMenuSite).getFooterFragment().showPlanungshilfenMenu(true);

        return res;
    }

    setArbeitsplanSite() {
        this.arbeitsplanSite = this.storybookPlayer.getCurrentPageNumber();
    }

    async onViewLoaded() {
        let res = super.onViewLoaded();

        this.storybookPlayer = new GBPlayer(this.findBy("#storybook-container"), r => {
            //TODO durch Popup oder Seite austauschen
            //Aktuell wird finish in einem Toast angezeigt, wenn ein Storybook beendet wird
            new Toast("finish!").show();
        }, "de");

        //Add Callback for simple speech and textToSpeech (through translator)
        this.storybookPlayer.setNodeRenderedCallback(async elem => {

            //Update die Translation in der Node
            Translator.updateTranslations(elem);

            //Stand nur speichern, wenn Initialisierung vorbei ist. Andernfalls wird
            // der Spielstand mit einem leeren Spielstand überschrieben
            if (this.saveLoaded) {
                let state = JSON.parse(this.storybookPlayer.SaveState());
                if (this.arbeitsplanSite) {
                    state.arbeitsplanSite = this.arbeitsplanSite;
                }
                this.progress.setState(JSON.stringify(state));
                await this.progress.save();
            }
        });

        await this.loadGamebook();

        return res;
    }

    async loadGamebook() {
        //checke, dass sowohl exercise und storybook durchlaufen sind
        // mit anderen Worten, dass this.setExercise() und this.viewLoaded() aufgerufen wurden
        if (this.exercise && this.storybookPlayer) {
            this.progress = await ExerciseHelper.getProgressFor(this.exercise);

            //Tausche Bilder durch FileMediums aus, um die richtigen Bilder herunterzuladen oder bereits
            // heruntergeladene Bilder anzuzeigen
            let data: any = this.exercise.generatingData;
            const imgData = data.i.map(i => JSON.parse(i));

            const fileMediumIds = [];
            imgData.forEach(img => {
                if (img.values.fileMediumId) {
                    fileMediumIds.push(img.values.fileMediumId);
                }
            });
            const fileMediums = await FileMedium.findByIds(Object.values(fileMediumIds));
            let indexedFileMediums = {};
            fileMediums.forEach(f => indexedFileMediums[f.id] = f);

            this.imgUrls = [];

            imgData.forEach(img => {
                if (img.values.fileMediumId && indexedFileMediums[img.values.fileMediumId]) {
                    img.values.url = indexedFileMediums[img.values.fileMediumId].getUrl();
                    this.imgUrls.push(img.values);
                }
            });

            data.i = imgData.map(i => JSON.stringify(i));



            //Danach lade das Gamebook
            this.storybookPlayer.LoadGamebook(data);

            //Danach lade den Spielstand des Gamebooks
            let state = this.progress.getState();
            if (typeof state === "string") {
                this.storybookPlayer.LoadState(state);
                const jsonData = JSON.parse(state);
                if (jsonData.arbeitsplanSite) {
                    this.arbeitsplanSite = jsonData.arbeitsplanSite;
                }
            }
            this.saveLoaded = true;
        }
    }

    async setElement(exercise) {
        this.exercise = exercise;
        await this.loadGamebook();
    }


    private forward() {
        const currentNumber = this.storybookPlayer.getCurrentPageNumber();
        this.storybookPlayer.GoToPage(currentNumber + 1);
    }

    private skipForward() {
        this.storybookPlayer.GoToCurrentPage()
    }


    public getImgUrls(){
      return this.imgUrls;
    }


    //Falls nicht mehr zurück gegangen werden kann, soll Seite eins zurück gehen
    goBack() {
        if (!this.back()) {
            return this.getSite().goBack();
        }
    }

    //Return false, falls nicht mehr im Storybook zurückgegangen werden kann => es wird goBack() von Seite aufgerufen
    private back() {
        const currentNumber = this.storybookPlayer.getCurrentPageNumber();
        if (currentNumber > 1) {
            this.storybookPlayer.GoToPage(currentNumber - 1);
            return false;
        } else {
            return true;
        }
    }
}
