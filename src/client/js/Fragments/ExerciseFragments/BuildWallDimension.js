import {ExerciseDescriptionFragment} from "./SubFragments/ExerciseDescriptionFragment";
import {SwipeFragment} from "cordova-sites/dist/client";
import {ExerciseHelper} from "../../Helpers/ExerciseHelper";
import {BuildWallFragment} from "./SubFragments/BuildWallExercise/BuildWallFragment";

/**
 * Fragment zum Bauen einer Wand. SwipeFragment (Basis-Klasse) beinhaltet mehrere Kinder-Fragmente,
 * die durch swipen gewechselt werden können
 */
export class BuildWallDimension extends SwipeFragment {
    constructor(site) {
        super(site);
        this._element = null;

        //Fragment, um die Aufgabenbeschreibung anzuzeigen
        this._exerciseDescriptionFragment = new ExerciseDescriptionFragment(this.getSite());

        //Fragment, um die eigentliche Mauer zu bauen
        this._buildwallfragment = new BuildWallFragment(this.getSite());
        this.addFragment(this._exerciseDescriptionFragment);
        this.addFragment(this._buildwallfragment);
    }

    /**
     * Wird von ExerciseFragment aufgerufen
     * @param element
     * @returns {Promise<void>}
     */
    async setElement(element) {
        this._element = element;

        let progress = await ExerciseHelper.getProgressFor(element);

        let data = progress.getData();

        data["length"]=parseFloat(data["exercise-length-wall"]);
        data ["breadth"]=parseFloat(data["exercise-breadth-wall"]);

        //Setze Daten, die von Unterelementen erwartet werden
        data["exercise-description"] = "This exercise requires you to virtually build a wall of length "+data["length"]+" and breadth "+data["breadth"];
        data["exercise-understood-exercise-question"] = "What is the purpose of the following exercise?";
        data["exercise-understood-exercise-answer-1"] = "Virtually build a wall of the above stated dimensions";
        data["exercise-understood-exercise-answer-2"] = "State the length and breadth of the wall";
        data["exercise-understood-exercise-answer-3"] = "Calculate the length and breadth of the wall";
        data["exercise-understood-exercise-answer-4"] = "Calculate the surface area and volume of the wall with given dimensions";
        data["exercise-understood-exercise-correct"] = "1";
        progress.setData(data);

        //Setze Element bei unterelementen
        await this._exerciseDescriptionFragment.setElement(element);
        await this._buildwallfragment.setElement(element);
    }

    goBack() {
        return this.previousFragment();
    }

    previousFragment() {
        if (this._activeIndex > 0) {
            super.previousFragment();
        } else {
            this.getSite().goBack();
        }
    }
}

