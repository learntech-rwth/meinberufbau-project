import {AbstractFragment} from "cordova-sites/dist/client";
import {ViewHelper} from "js-helper/dist/client/ViewHelper";

const view = require("../../html/Fragments/linkSetFramgent.html");

/**
 * Klasse, um eine "Linksammung" als Fragment anzuzeigen.
 * Jeder Link hat ein Bild und einen Titel, sowie eine Seite, die gestartet werden soll,
 * wenn der Link angeklickt wird.
 *
 * Dieses Fragment wird von AusbauSite, HochbauSite, StartSite und TiefbauSite genutzt,
 * um die Kurse anzuzeigen.
 */
export class LinkSetFragment extends AbstractFragment {

    private links: { name: string, image: string, site: any, constructParameters: any }[] = [];
    private linkTemplate: HTMLElement;
    private linkContainer: HTMLElement;

    constructor(site: any) {
        super(site, view);
    }

    onViewLoaded(): Promise<any[]> {
        const res = super.onViewLoaded();

        this.linkContainer = this.findBy("#link-container");
        this.linkTemplate = this.findBy("#link-template");
        this.linkTemplate.remove();
        this.linkTemplate.removeAttribute("id");

        return res;
    }

    async setLinks(links: { name: string, image: string, site: any, constructParameters: any }[]) {
        this.links = links;
        await this._viewLoadedPromise;

        ViewHelper.removeAllChildren(this.linkContainer);
        this.links.forEach(link => {
            const linkElem = <HTMLElement>this.linkTemplate.cloneNode(true);
            (<HTMLImageElement>linkElem.querySelector(".icon")).src = link.image;
            (<HTMLElement>linkElem.querySelector(".name")).innerText = link.name;
            linkElem.addEventListener("click", () => this.startSite(link.site, link.constructParameters));
            this.linkContainer.appendChild(linkElem);
        });
    }
}
