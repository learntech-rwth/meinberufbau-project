import {AbstractFragment} from "cordova-sites/dist/client/js/Context/AbstractFragment";

import view from "../../../html/Fragments/statistic/wrongAnswerPerExerciseFragment.html";
import {Helper} from "js-helper/dist/shared";
import {ExerciseStatisticSite} from "../../Sites/ExerciseStatisticSite";

export class WrongAnswerPerExerciseFragment extends AbstractFragment{

    constructor(site) {
        super(site, view);
        this._exerciseChart = null;
    }

    async setStatistic(statistic) {
        await this._viewLoadedPromise;

        let canvas = this.findBy("#wrong-answer-exercise-diagram");

        let exerciseCounts = {};
        let exerciseNames = {};

        statistic["data"].forEach(dataset => {
            if (!Helper.isSet(exerciseNames[dataset["exerciseId"]])) {
                exerciseNames[dataset["exerciseId"]] = dataset["exerciseName"] + (dataset["isGenerating"] ? "*" : "");
                exerciseCounts[dataset["exerciseId"]] = 0;
            }
            exerciseCounts[dataset["exerciseId"]]++;
        });

        let options = {
            type: 'bar',
            data: {
                labels: Object.values(exerciseNames),
                datasets: [{
                    label: '# of Wrong Answers',
                    data: Object.values(exerciseCounts),
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                },
                onClick: (chart, dataPoints) => {
                    if (dataPoints.length > 0){
                        let firstPoint = dataPoints[0];
                        if (firstPoint._datasetIndex === 0 && firstPoint._index < Object.values(exerciseCounts).length){
                            let id = Object.keys(exerciseCounts)[firstPoint._index];
                            this.startSite(ExerciseStatisticSite, {exercise: id, filter: JSON.stringify(this.getSite().getFilter())});
                        }
                    }
                }
            }
        };
        if (Helper.isNotNull(this._exerciseChart)) {
            this._exerciseChart.destroy();
        }

        this._exerciseChart = new Chart(canvas.getContext("2d"), options);
    }
}