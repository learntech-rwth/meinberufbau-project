import {AbstractFragment} from "cordova-sites/dist/client/js/Context/AbstractFragment";

import view from "../../../html/Fragments/statistic/countWrongAnswerFragment.html";
import {Helper} from "js-helper/dist/shared";
import {WrongAnswerListDialog} from "../../Dialog/WrongAnswerListDialog";

export class CountWrongAnswerFragment extends AbstractFragment{

    constructor(site) {
        super(site, view);
        this._countWrongAnswerDiagram = null;
    }

    async setStatistic(statistic) {
        await this._viewLoadedPromise;

        let canvas = this.findBy("#count-wrong-answer-diagram");

        let counts = {};
        let indexedDataset = {};

        statistic["data"].forEach(dataset => {
            if (!Helper.isSet(counts, dataset["field"])) {
                counts[dataset["field"]] = 0;
                indexedDataset[dataset["field"]] = [];
            }
            counts[dataset["field"]]++;
            indexedDataset[dataset["field"]].push(dataset);
        });

        let labels = Object.keys(counts).sort();

        let values = [];
        labels.forEach(label => {
            values.push(counts[label]);
        });

        let options = {
            type: 'bar',
            data: {
                labels: labels,
                datasets: [{
                    data: values,
                    label: "# of Wrong Answers per Field"
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                },
                onClick: (chart, dataPoints) => {
                    if (dataPoints.length > 0){
                        let firstPoint = dataPoints[0];
                        if (firstPoint._datasetIndex === 0 && firstPoint._index < values.length){
                            let label = labels[firstPoint._index];
                            new WrongAnswerListDialog(label, indexedDataset[label]).show();
                        }
                    }
                }
            }
        };
        if (Helper.isNotNull(this._timeDiagram)) {
            this._timeDiagram.destroy();
        }

        this._timeDiagram = new Chart(canvas.getContext("2d"), options);
    }
}