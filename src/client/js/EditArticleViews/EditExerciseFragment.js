import {AbstractFragment, Form, Helper} from "cordova-sites/dist/client";

export class EditExerciseFragment extends AbstractFragment {
    constructor(site, view, type) {
        super(site, view);
        this._form = null;
        this._type = type;

        this._changeListener = null;
    }

    setChangeListener(changeListener) {
        this._changeListener = changeListener;
    }

    _triggerChangeListener() {
        if (Helper.isNotNull(this._changeListener)) {
            this._changeListener();
        }
    }

    async onViewLoaded() {
        let res = super.onViewLoaded();
        this._form = new Form(this.findBy("form"));
        this._form.setElementChangeListener(() => {
            this._triggerChangeListener();
        });
        return res;
    }

    async getElementView() {
        return await this._viewPromise;
    }

    async isValid() {
        await this._viewPromise; //zuvor ist this._numberInputForm nicht gesetzt
        return this._form.validate();
    }

    async updateExercise(exercise) {
        exercise.setElementType(this._type);

        let data = await this._form.getValues();

        let formElem = this._form.getFormElement();

        let filePromises = [];
        let filesToSave = [];
        let neededSize = 0;
        Object.keys(data).forEach(key => {
            if (data[key] instanceof Blob) {
                if (data[key].size > 0) {

                    //mark to save as file
                    if (Helper.isNotNull(formElem.elements[key].dataset["file"])) {
                        filesToSave.push(key);
                        neededSize += data[key].size;
                    }
                    //or convert to base64
                    else {
                        filePromises.push(new Promise((resolve, reject) => {
                            const reader = new FileReader();
                            reader.onload = () => resolve(reader.result);
                            reader.onerror = error => reject(error);
                            reader.readAsDataURL(data[key]);
                        }).then(base64 => data[key] = base64));
                    }
                } else {
                    data[key] = null;
                }
            }
        });
        await Promise.all(filePromises);

        if (filesToSave.length >= 1) {
            await new Promise((resolve, reject) => {
                window.requestFileSystem(LocalFileSystem.PERSISTENT, neededSize, fs => {
                    let filePromises = [];
                    filesToSave.forEach(key => {
                        filePromises.push(new Promise((resolveInner, rejectInner) => {
                            fs.root.getFile(data[key].name, {create: true, exclusive: false}, entry => {
                                entry.createWriter(writer => {

                                    writer.onwriteend = () => {
                                        data[key] = entry.toURL();
                                        // if (data[key].startsWith("file:///")){
                                        //     data[key] = "filesystem:http://localhos:8000/"+data[key].substring(8);
                                        // }
                                        resolveInner();
                                    };
                                    writer.onerror = rejectInner;
                                    writer.write(data[key]);
                                }, rejectInner);
                            }, rejectInner);
                        }));
                    });
                    resolve(Promise.all(filePromises));
                }, reject);
            });
        }

        exercise.setData(data);
        return exercise;
    }
}