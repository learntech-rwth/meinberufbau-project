import {Dialog} from "cordova-sites/dist/client";
import {ViewInflater} from "cordova-sites/dist/client/js/ViewInflater";
import {Translator} from "cordova-sites/dist/shared/Translator";
import {ViewHelper} from "js-helper/dist/client/ViewHelper";
import {Helper} from "js-helper/dist/shared";

const view = require("../../html/Dialog/unitCalculator.html");
const units =<{
    [type: string]: {
        [name: string]: number
    }
}> require("../../units.json");

export class UnitCalculator extends Dialog {
    private static units: {
        [type: string]: {
            [name: string]: number
        }
    } = units;

    private typeSelect: HTMLSelectElement;

    private unit1Input: HTMLInputElement;
    private unit1Select: HTMLSelectElement;

    private unit2Input: HTMLInputElement;
    private unit2Select: HTMLSelectElement;

    private formulaContainer: HTMLElement;

    private selectedType: string;
    private selectedUnit1: string;
    private selectedUnit2: string;

    constructor() {
        super(ViewInflater.getInstance().load(view).then((v) => this.prepareView(v)), "Unit calculator");
    }

    private async prepareView(view: HTMLElement) {

        this.typeSelect = view.querySelector("#type-select");
        this.unit1Input = view.querySelector("#unit-1-input");
        this.unit1Select = view.querySelector("#unit-1-select");
        this.unit2Input = view.querySelector("#unit-2-input");
        this.unit2Select = view.querySelector("#unit-2-select");
        this.formulaContainer = view.querySelector("#formula-container");

        const unitOptions = Object.keys(UnitCalculator.units);
        unitOptions.forEach(option => {
            const optionElement = document.createElement("option");
            optionElement.value = option;
            optionElement.innerText = Translator.getInstance().translate(option);
            this.typeSelect.appendChild(optionElement);
        });

        this.typeSelect.addEventListener("change", () => this.selectType(this.typeSelect.value));
        this.unit1Input.addEventListener("input", () => this.calculate());
        this.unit1Select.addEventListener("change", () => this.calculate(false, false));
        this.unit2Input.addEventListener("input", () => this.calculate(true));
        this.unit2Select.addEventListener("change", () => this.calculate(false, true));

        this.selectType(unitOptions[0]);

        return view;
    }

    private selectType(type: string) {
        if (!UnitCalculator.units[type] || type === this.selectedType) {
            return;
        }

        this.selectedType = type;
        this.unit1Input.value = "1";

        ViewHelper.removeAllChildren(this.unit1Select);
        ViewHelper.removeAllChildren(this.unit2Select);

        const options = Object.keys(UnitCalculator.units[type]);
        options.forEach((option, index) => {
            const optionElement = document.createElement("option");
            optionElement.value = UnitCalculator.units[type][option] + "";
            optionElement.innerText = Translator.getInstance().translate(option);

            const optionElement2 = <HTMLOptionElement>optionElement.cloneNode(true);

            if (index === 0) {
                optionElement.selected = true;
            } else if (index === 1) {
                optionElement2.selected = true;
            }

            this.unit1Select.appendChild(optionElement);
            this.unit2Select.appendChild(optionElement2);
        });
        this.calculate();
    }

    private calculate(fromSecond?: boolean, changedSecondIndex?: boolean) {

        let base: number;
        let output: HTMLInputElement;
        let result: number;

        if (Helper.isNotNull(changedSecondIndex)) {
            if (changedSecondIndex) {
                if (this.unit1Select.value === this.unit2Select.value) {
                    this.unit1Select.value = this.selectedUnit2;
                }
            } else {
                if (this.unit2Select.value === this.unit1Select.value) {
                    this.unit2Select.value = this.selectedUnit1;
                }
            }
        }
        let multiplier = parseFloat(this.unit1Select.value) / parseFloat(this.unit2Select.value);

        if (!Helper.nonNull(fromSecond, false)) {
            base = parseFloat(this.unit1Input.value);
            result = base * multiplier;
            output = this.unit2Input;
        } else {
            base = parseFloat(this.unit2Input.value);
            result = base / multiplier;
            output = this.unit1Input;
        }
        let newResult = Math.round(result*100000)/100000;
        if (newResult !== 0){
            result = newResult;
        }
        output.value = result + "";

        this.selectedUnit1 = this.unit1Select.value
        this.selectedUnit2 = this.unit2Select.value

        const type = Translator.getInstance().translate(this.selectedType);
        let translateKey = "formula-unit-calculator-multiply";
        if (multiplier < 1) {
            multiplier = 1 / multiplier;
            translateKey = "formula-unit-calculator-divide";
        }
        const formula = Translator.getInstance().translate(translateKey, [type, UnitCalculator.formatMultiplier(multiplier)]);
        this.formulaContainer.innerText = formula;
    }

    private static formatMultiplier(result: number) {
        const res = result+"";
        const parts = res.split(".");
        if (parts.length >= 1){
            parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ".")
        }
        if (parts.length >= 2 && parts[1].length > 5){
            parts[1] = parts[1].substring(0, 5);
        }
        return parts.join(",");
    }
}
