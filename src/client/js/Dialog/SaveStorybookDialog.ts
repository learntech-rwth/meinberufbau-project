import {Dialog} from "cordova-sites/dist/client/js/Dialog/Dialog";

const view = require("../../html/Dialog/saveStorybookDialog.html");

import {ViewInflater} from "cordova-sites/dist/client/js/ViewInflater";
import {Form} from "cordova-sites/dist/client/js/Form";
import {Course} from "../../../shared/model/Course";
import {Helper} from "cordova-sites/dist/client";
import {FileMedium} from "cordova-sites-easy-sync/dist/shared";

export class SaveStorybookDialog extends Dialog {

    constructor(name, selectedCourseId, stage, description, img: FileMedium) {
        super(ViewInflater.getInstance().load(view).then(async view => {
            let courses = await Course.find(undefined, {"id": "ASC"});
            courses = Helper.arrayToObject(courses, course => course.id);

            let selectCourseInput = view.querySelector("#article-course");
            Object.keys(courses).forEach(courseId => {
                let option = document.createElement("option");
                option.value = courseId;
                option.innerText = courses[courseId].name;

                if (parseInt(courseId) === selectedCourseId) {
                    option.selected = true;
                }
                selectCourseInput.appendChild(option);
            });

            let form = new Form(view.querySelector("#save-storybook-form"), values => {
                this._result = values;
                this.close();
            });

            let values = {};
            values["name"] = name;
            values["course"] = selectedCourseId;
            values["stage"] = stage;
            values["description"] = description;

            const imageInput = view.querySelector("[name=image]");
            imageInput.addEventListener("input", () => {
                if (imageInput.files && imageInput.files[0]) {
                    let reader = new FileReader();
                    reader.onload = e => {
                        // @ts-ignore
                        view.querySelector(".image-preview").src = e.target.result;
                    };
                    reader.readAsDataURL(imageInput.files[0]);
                } else {
                    view.querySelector(".image-preview").src = "";
                }
                view.querySelector("[name=image-changed]").value = "1";
            });

            if (img && !Helper.imageUrlIsEmpty(img.getUrl())) {
                view.querySelector(".image-preview").src = img.getUrl();
            }

            form.setValues(values);

            return view;
        }), "save storybook");
    }
}
