import {Dialog, ViewInflater} from "cordova-sites/dist/client";

import view from "../../html/Dialog/templateInfo.html";

export class TemplateInfo extends Dialog {

    constructor(){
     super(view,"Info");
    }
}
