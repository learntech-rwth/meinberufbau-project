import {Dialog} from "cordova-sites/dist/client/js/Dialog/Dialog";

import view from "../../html/Dialog/wrongAnswerListDialog.html";
import {Translator} from "cordova-sites/dist/client/js/Translator";
import {ViewInflater} from "cordova-sites/dist/client/js/ViewInflater";

export class WrongAnswerListDialog extends Dialog{

    constructor(fieldName, wrongAnswerList) {
        let title = Translator.translate("wrong-answer-list-dialog-title", [fieldName]);
        super(ViewInflater.getInstance().load(view).then(view => {

            this._container = view.querySelector(".wrong-answer-container");
            this._template = view.querySelector("#wrong-answer-template");
            this._template.removeAttribute("id");
            this._template.remove();

            wrongAnswerList.forEach(wrongAnswer => {
                this._addWrongAnswer(wrongAnswer);
            });

            return view;
        }), title);
        this._translatable = false;
    }

    _addWrongAnswer(wrongAnswer){
        let element = this._template.cloneNode(true);
        element.querySelector(".given").innerText = wrongAnswer.given;
        element.querySelector(".expected").innerText = wrongAnswer.expected;
        this._container.appendChild(element);
    }
}