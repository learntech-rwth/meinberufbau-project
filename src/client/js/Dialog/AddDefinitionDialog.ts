import {Definition} from "../../../shared/model/Definition";
import {Lexicon} from "../Lexicon";
import {Dialog, Form, Helper, Toast} from "cordova-sites/dist/client";
import {UserManager} from "cordova-sites-user-management/dist/client";
import {FileMedium} from "cordova-sites-easy-sync/dist/shared";

const view = require("../../html/Dialog/addDefinitionDialog.html");

export class AddDefinitionDialog extends Dialog {
    private definition: Definition;

    constructor(definition?: Definition) {
        super(view, "add-definition-dialog");
        this.definition = Helper.nonNull(definition, new Definition());
    }

    async setContent(content) {
        let res: any = super.setContent(content);
        res = await res;

        let imageChanged = false;
        const form = new Form(this._content.querySelector(".definition-input-form"), async (values) => {
            let keywords = values["keywords"].split(",");
            keywords.forEach((keyword, index) => {
                keywords[index] = keyword.trim();
            });
            this.definition.setKeywords(keywords);
            this.definition.setKey(values["key"].trim());
            this.definition.setDescription(values["description"].trim());

            if (values["description-simple-german"].trim() !== ""){
                this.definition.setDescriptionSimpleGerman(values["description-simple-german"].trim());
            }
            else {
                this.definition.setDescriptionSimpleGerman(null);
            }

            if (values["image"] && values["image"].trim() !== "" && values["image"].trim() !== "data:") {
                let image = this.definition.getImage();
                if (image === null) {
                    image = new FileMedium();
                    this.definition.setImage(image);
                }
                image.setSrc(values["image"]);
                await image.save();
                this.definition.setImageDescription(values["image-description"]);
            } else if (imageChanged){
                this.definition.setImage(null);
                this.definition.setImageDescription(null);
            }

            let saveForAll = await UserManager.getInstance().hasAccess("admin") && Helper.isNotNull(values["save-for-all"]);

            try {
                await Lexicon.getInstance().addOrUpdateDefinition(this.definition, !saveForAll);
                this._result = this.definition;
                this.close()
            } catch (e) {
                new Toast("something went wrong...").show();
                console.error(e);
            }

        });

        form.addValidator(values => {
            if (values["image"] && !Helper.imageUrlIsEmpty(values["image"]) && values["image-description"].trim() === "") {
                return {
                    "image-description": "image-description must be set, if image is set",
                }
            }
            return true;
        });

        form.addValidator(async values => {
            const other = await Definition.findOne({"key": values["key"].trim()});
            if (other && other.id !== this.definition.id) {
                return {
                    "key": "key already exists!",
                }
            }
            return true;
        });

        const values = {
            "key": this.definition.getKey(),
            "keywords": this.definition.getKeywords().join(", "),
            "description": this.definition.getDescription(),
            "description-simple-german": Helper.nonNull(this.definition.getDescriptionSimpleGerman(), ""),
            "image-description": this.definition.getImageDescription(),
            "save-for-all": this.definition.user === null
        }

        const imagePreview = this._content.querySelector(".image-preview");
        if (this.definition.getImage() !== null) {
            imagePreview.src = this.definition.getImage().getUrl();
        }
        const imageInput = this._content.querySelector("input[name='image']");
        imageInput.addEventListener("input", () => {
            imageChanged = true;
            if (imageInput.files && imageInput.files[0]) {
                let reader = new FileReader();
                reader.onload = e => {
                    imagePreview.src = e.target.result;
                };
                reader.readAsDataURL(imageInput.files[0]);
            }
            else {
                imagePreview.src = "https://placehold.it/180";
            }
        });
        form.setValues(values);

        if (this.definition.id !== null){
            this._content.querySelector("input[name='save-for-all']").disabled = true;
        }

        return res;
    }
}
