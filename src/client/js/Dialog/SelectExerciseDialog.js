import {ChooseDialog} from "cordova-sites/dist/client";
import {EditExerciseSite} from "../Sites/EditExerciseSite";

export class SelectExerciseDialog extends ChooseDialog{
    constructor() {
        let exercises = {};
        exercises[EditExerciseSite.EXERCISE_TYPE.WALL_LENGTH] = "wall-length";
        exercises[EditExerciseSite.EXERCISE_TYPE.WALL_LENGTH_EIGHT] = "wall-length-eight";
        exercises[EditExerciseSite.EXERCISE_TYPE.BUILD_WALL] = "build-wall";
        exercises["storybook"] = "storybook";
        super(exercises, "select-exercise" );
    }
}