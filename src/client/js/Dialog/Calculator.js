import {Dialog, ViewInflater} from "cordova-sites/dist/client";

import view from "../../html/Dialog/calculator.html";
import * as mathjs from 'mathjs';

export class Calculator extends Dialog {

    constructor() {
        let viewPromise = ViewInflater.getInstance().load(view).then(view => {
            this._inputElement = view.querySelector(".equation");
            this._outputElement = view.querySelector(".answer");
            view.querySelector("#calp2").style.display = "none";
            view.querySelector("#calp3").style.display = "none";

            this._inputElement.addEventListener("keyup", e => {
                if (e.code === "Enter" || e.code === "NumpadEnter") {
                    this.evaluateEquation(this._inputElement.innerText);
                } else {
                    console.log(e);
                }
            });

           view.querySelectorAll(".result-button").forEach(element => {
               element.addEventListener("click", () => {
                 this.evaluateEquation(this._inputElement.innerText);
               })
             });

            view.querySelectorAll(".more-button").forEach(element => {
                element.addEventListener("click", () => {
                  view.querySelector("#calp1").style.display = "none";
                  view.querySelector("#calp2").style.display = "inline";
                  view.querySelector("#calp3").style.display = "none";
                })
              });

             view.querySelectorAll(".numbers-button").forEach(element => {
                 element.addEventListener("click", () => {
                   view.querySelector("#calp1").style.display = "inline";
                   view.querySelector("#calp2").style.display = "none";
                   view.querySelector("#calp3").style.display = "none";
                 })
               });

              view.querySelector(".drg-button").addEventListener("click", () => {
                view.querySelector("#calp1").style.display = "none";
                view.querySelector("#calp2").style.display = "none";
                view.querySelector("#calp3").style.display = "inline";
               });

               view.querySelector(".deg").addEventListener("click", () => {
                 this.evaluateEquation(this._inputElement.innerText);
                 this._outputElement.innerText += " °";
                });

               view.querySelector(".rad").addEventListener("click", () => {
                 this.evaluateEquation(this._inputElement.innerText+"*PI/180");
                 this._outputElement.innerText += " rad";
                });

                view.querySelector(".gon").addEventListener("click", () => {
                  this.evaluateEquation(this._inputElement.innerText+"*200/180");
                  this._outputElement.innerText += " gon";
                 });

                 view.querySelector(".kehrwert").addEventListener("click", () => {
                   this.evaluateEquation("1/"+this._inputElement.innerText);
                  });

                 view.querySelector(".plusmin").addEventListener("click", () => {
                   this._outputElement.innerText = "";
                   let num = this._inputElement.innerText;
                   if(num.includes("+")){num = num.substr(num.lastIndexOf("+")+1,num.length-1);}
                   if(num.includes("-")){num = num.substr(num.lastIndexOf("-")+1,num.length-1);}
                   if(num.includes("*")){num = num.substr(num.lastIndexOf("*")+1,num.length-1);}
                   if(num.includes("/")){num = num.substr(num.lastIndexOf("/")+1,num.length-1);}
                   if(num.includes("(")){num = num.substr(num.lastIndexOf("(")+1,num.length-1);}
                   if(num.includes(")")){num = num.substr(num.lastIndexOf(")")+1,num.length-1);}
                   this._inputElement.innerText = this._inputElement.innerText.substr(0, this._inputElement.innerText.length-num.length);
                   this._inputElement.innerText += "-("+num;
                  });






            view.querySelectorAll("[data-value]").forEach(element => {
                element.addEventListener("click", e => {
                    this.insertValue(e.target.dataset["value"]);
                    this._outputElement.innerText = "";
                })
            });

            view.querySelectorAll(".delete-input").forEach(element => {
                element.addEventListener("mousedown", () => {
                  this._inputElement.innerText = this._inputElement.innerText.substr(0, this._inputElement.innerText.length - 1);
                  this._outputElement.innerText = "";
                  this._mouseDownTimer = setTimeout(() => {
                    this._outputElement.innerText = "";
                    this._inputElement.innerText = "";
                  }, Calculator.TIME_LONG_PRESS);
                })
              });

              view.querySelectorAll(".delete-input").forEach(element => {
                  element.addEventListener("mouseup", () => {
                    clearTimeout(this._mouseDownTimer);
                  })
                });

            view.querySelectorAll(".delete-all").forEach(element => {
                element.addEventListener("mousedown", () => {
                  this._inputElement.innerText = "";
                  this._outputElement.innerText = "";
                })
              });

            this._inputElement.focus();
            return view;
        });
        super(viewPromise, "calculator");
    }

    insertValue(value) {
      if(value == undefined){} else{
        if (document.selection) {
            // IE
            this._inputElement.focus();
            let sel = document.selection.createRange();
            sel.text = value;
        } else if (this._inputElement.selectionStart || this._inputElement.selectionStart === 0) {
            // Others
            let startPos = this._inputElement.selectionStart;
            let endPos = this._inputElement.selectionEnd;
            this._inputElement.innerText = this._inputElement.innerText.substring(0, startPos) +
                value +
                this._inputElement.innerText.substring(endPos, this._inputElement.innerText.length);
            this._inputElement.selectionStart = startPos + value.length;
            this._inputElement.selectionEnd = startPos + value.length;
        } else {

          let last = "";
          if(this._inputElement.innerText.length > 0){
            last = this._inputElement.innerText.substr((this._inputElement.innerText.length - 1),1);
          }

          if((value == "+" || value == "-" || value == "×" || value == "÷" || value == ",") && (last == "+" || last == "-" || last == "×" || last == "÷" || last == "" || last == "(" || last == ")")){
            if(value == ","){
              this._inputElement.innerText += "0" + value;
            }else{
              if(last == ""){
              }else{
                this._inputElement.innerText = this._inputElement.innerText.substr(0, this._inputElement.innerText.length - 1) + value;
              }
            }
          }else{
            if((value == "%") && (last == "+" || last == "-" || last == "×" || last == "÷" || last == "" || last == "(" || last == ")")){}
            else{
              if((value == "^" || value == "²") && (last == "+" || last == "-" || last == "×" || last == "÷" || last == "" || last == "(")){}
              else{
                if(value == ")"){
                  if(last == "("){}else{
                    let bracketopen = 0;
                    for (let position = 0; position < this._inputElement.innerText.length; position++){
                      if (this._inputElement.innerText.charAt(position) == "("){bracketopen += 1;}
                    }
                    let bracketclose = 0;
                    for (let position = 0; position < this._inputElement.innerText.length; position++){
                      if (this._inputElement.innerText.charAt(position) == ")"){bracketclose += 1;}
                    }
                    if(bracketclose<bracketopen){this._inputElement.innerText += value;}
                  }

                }
                else{
                  this._inputElement.innerText += value;
                }
              }
            }
          }
        }
        this._inputElement.focus();
    }
  }

    evaluateEquation(equation) {
        if (this._outputElement) {
            try {
                equation = equation.replace(/×/g, "*");
                equation = equation.replace(/÷/g, "/");
                equation = equation.replace(/\./g, "");
                equation = equation.replace(/,/g, ".");
                equation = equation.replace(/%/g, "/100");
                equation = equation.replace(/log/g, "log10");
                equation = equation.replace(/√/g, "sqrt");
                equation = equation.replace(/π/g, "PI");

                while (equation.includes("²")) {
                  console.log("not pow");
                    let num = equation.substr(0,equation.indexOf("²"));
                    if(num.includes("+")){num = num.substr(num.lastIndexOf("+")+1,num.length-1);}
                    if(num.includes("-")){num = num.substr(num.lastIndexOf("-")+1,num.length-1);}
                    if(num.includes("*")){num = num.substr(num.lastIndexOf("*")+1,num.length-1);}
                    if(num.includes("/")){num = num.substr(num.lastIndexOf("/")+1,num.length-1);}
                    if(num.includes("(")){num = num.substr(num.lastIndexOf("(")+1,num.length-1);}
                    if(num.includes(")")){num = num.substr(num.lastIndexOf(")")+1,num.length-1);}
                    equation = equation.replace(num+"²", "pow("+num+",2)");
                }

                let bracketopen = 0;
                for (let position = 0; position < equation.length; position++){
                  if (equation.charAt(position) == "("){
                    bracketopen += 1;
                  }
                }

                let bracketclose = 0;
                for (let position = 0; position < equation.length; position++){
                  if (equation.charAt(position) == ")"){
                    bracketclose += 1;
                  }
                }

                if(bracketopen > bracketclose){
                  for (let i = 0; bracketopen-bracketclose; i++){
                    equation += ")";
                    bracketopen = 0;
                    for (let position = 0; position < equation.length; position++){
                      if (equation.charAt(position) == "("){
                        bracketopen += 1;
                      }
                    }
                    bracketclose = 0;
                    for (let position = 0; position < equation.length; position++){
                      if (equation.charAt(position) == ")"){
                        bracketclose += 1;
                      }
                    }
                  }
                }




                console.log(equation);
            } catch (e) {
                console.error(e);
            }



            let result = "";
            try {
                result = mathjs.evaluate(equation);
            } catch (e) {
                //result = e.message;
                result = "Ungültige Eingabe";
            }

            if (result === undefined) {
                result = "";
            }

            result = "" + result;
            result = result.replace(/\./, ",");
            this._outputElement.innerText = result;
        }
    }

}

Calculator.TIME_LONG_PRESS = 1000;
