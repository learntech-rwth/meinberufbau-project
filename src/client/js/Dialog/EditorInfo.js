import {Dialog, ViewInflater} from "cordova-sites/dist/client";

import view from "../../html/Dialog/editorInfo.html";

export class EditorInfo extends Dialog {

    constructor(){
     super(view,"Info");
    }
}
