import {Definition} from "../../../shared/model/Definition";
import {Lexicon} from "../Lexicon";
import {Dialog, Form, Helper, Toast} from "cordova-sites/dist/client";
import {UserManager} from "cordova-sites-user-management/dist/client";
import {FileMedium} from "cordova-sites-easy-sync/dist/shared";

const view = require("../../html/Dialog/addFileDialog.html");

export class AddFileDialog extends Dialog {
    private definition: Definition;

    constructor(definition?: Definition) {
        super(view, "add-file-dialog");
        this.definition = Helper.nonNull(definition, new Definition());
    }

    async setContent(content) {
        let res: any = super.setContent(content);
        res = await res;

        let imageChanged = false;
        const form = new Form(this._content.querySelector(".definition-input-form"), async (values) => {
            let keywords= ["-filelist"];

            this.definition.setKeywords(keywords);
            this.definition.setKey("-file"+values["key"].trim());
            this.definition.setDescription(values["description"].trim());

            if (values["description-simple-german"].trim() !== ""){
                this.definition.setDescriptionSimpleGerman(values["description-simple-german"].trim());
            }
            else {
                this.definition.setDescriptionSimpleGerman(null);
            }

            if (values["image"] && values["image"].trim() !== "" && values["image"].trim() !== "data:") {
                let image = this.definition.getImage();
                if (image === null) {
                    image = new FileMedium();
                    this.definition.setImage(image);
                }
                image.setSrc(values["image"]);
                await image.save();
                this.definition.setImageDescription("file");

            } else if (imageChanged){
                this.definition.setImage(null);
                this.definition.setImageDescription(null);
            }

            let saveForAll = await UserManager.getInstance().hasAccess("admin") && Helper.isNotNull(values["save-for-all"]);

            try {
                await Lexicon.getInstance().addOrUpdateDefinition(this.definition, !saveForAll);
                this._result = this.definition;
                this.close()
            } catch (e) {
                new Toast("something went wrong...").show();
                console.error(e);
            }

        });

        form.addValidator(values => {
            if (values["description"] === "" && Helper.imageUrlIsEmpty(values["image"])) {
                this._content.querySelector(".savenotpossible").classList.remove("hidden");
                return false;
            }
            return true;
        });

        form.addValidator(async values => {
            const other = await Definition.findOne({"key": "-file"+values["key"].trim()});
            if (other && other.id !== this.definition.id) {
                return {
                    "key": "key already exists!",
                }
            }
            return true;
        });

        const values = {
            "key": this.definition.getKey().substring(5,this.definition.getKey().length),
            "keywords": this.definition.getKeywords().join(", "),
            "description": this.definition.getDescription(),
            "description-simple-german": Helper.nonNull(this.definition.getDescriptionSimpleGerman(), ""),
            "image-description": this.definition.getImageDescription(),
            "save-for-all": this.definition.user === null
        }

        const imagePreview = this._content.querySelector(".image-preview");
        if (this.definition.getImage() !== null) {
            imagePreview.src = this.definition.getImage().getUrl();
        }
        const imageInput = this._content.querySelector("input[name='image']");
        imageInput.addEventListener("input", () => {
            imageChanged = true;
            if (imageInput.files && imageInput.files[0]) {
                let reader = new FileReader();
                reader.onload = e => {
                    imagePreview.src = e.target.result;
                };
                reader.readAsDataURL(imageInput.files[0]);
            }
            else {
                imagePreview.src = "https://placehold.it/180";
            }
        });

        const imageURL = this._content.querySelector("input[name='image-url']");
        if (this.definition.getImage() !== null) {
            imageURL.value = this.definition.getImage().getUrl();
        }

        form.setValues(values);

        if (this.definition.id !== null){
            this._content.querySelector("input[name='save-for-all']").disabled = true;
        }

        return res;
    }
}
