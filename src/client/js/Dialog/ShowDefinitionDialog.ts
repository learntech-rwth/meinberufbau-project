import {Definition} from "../../../shared/model/Definition";

const view = require("../../html/Dialog/showDefinition.html");
import {Lexicon} from "../Lexicon";
import {Dialog, Helper, Translator} from "cordova-sites/dist/client";

export class ShowDefinitionDialog extends Dialog {
    private definition: Definition;

    constructor(definition: Definition) {
        super(view, "");
        this.setTranslatable(false);
        this.setAdditionalClasses("no-title");

        this.definition = definition;
    }

    async setContent(content) {
        let res: any = super.setContent(content);
        res = await res;
        await Lexicon.getInstance()._loadDefinitions(false);

        content = this._content;
        content.querySelector(".key").appendChild(document.createTextNode(this.definition.getKey()));
        content.querySelector(".keywords").appendChild(document.createTextNode(this.definition.getKeywords().join(", ")));
        // content.querySelector(".description").appendChild(await Lexicon.getInstance().prepareText(this.definition.getDescription(), this.definition.getKey()));
        const descriptionKey = Translator.getInstance().createDynamicKey();
        const translations = {
            "de":{},
            "de-sl":{}
        }
        translations["de"][descriptionKey] = this.definition.getDescription();
        translations["de-sl"][descriptionKey] = this.definition.getDescriptionSimpleGerman();
        Translator.getInstance().addDynamicTranslations(translations)

        content.querySelector(".description").appendChild(Translator.makePersistentTranslation(descriptionKey));

        let img = this.definition.getImage();
        if (Helper.isNotNull(img)) {
            content.querySelector(".image-container").classList.remove("hidden");
            content.querySelector(".image").setAttribute("src", img.getUrl());
            content.querySelector(".image-description").appendChild(Lexicon.getInstance().prepareText(this.definition.getImageDescription(), this.definition.getKey()));
        }

        return res;
    }
}
