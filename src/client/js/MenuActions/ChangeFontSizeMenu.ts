import {MenuAction, Submenu} from "cordova-sites/dist/client";

export class ChangeFontSizeMenu extends Submenu{

    private factors = [1.0, 1.1, 1.2, 1.3, 1.4, 1.5, 1.6, 1.7, 1.8, 1.9, 2.0];
    private currentIndex = 0;
    private defaultFontSize = null;

    constructor(showFor?: any, order?: any, icon?: any, renderers?: any) {
        super("fontSize", showFor, order, icon, renderers);
        this.addAction(new MenuAction("sizeUp", () => {
            if (this.currentIndex >= this.factors.length-1){
                return;
            }
            this.currentIndex++;
            document.documentElement.style.fontSize = (this.getDefaultFontsize()*this.factors[this.currentIndex])+"px";
        }, showFor))
        this.addAction(new MenuAction("sizeDown", () => {
            if (this.currentIndex <= 0){
                return;
            }
            this.currentIndex--;
            document.documentElement.style.fontSize = (this.getDefaultFontsize()*this.factors[this.currentIndex])+"px";
        }))
    }

    private getDefaultFontsize(){
        if (!this.defaultFontSize){
            const fontSize = window.getComputedStyle(document.body).getPropertyValue("font-size");
            this.defaultFontSize = parseFloat(fontSize.substring(0, fontSize.length-2));
        }
        return this.defaultFontSize;
    }
}
