import {MenuAction, Submenu} from "cordova-sites/dist/client";

export class ChangeBgColorMenu extends Submenu{

    constructor(showFor?: any, order?: any, icon?: any, renderers?: any) {
        super("background", showFor, order, icon, renderers);
        this.addAction(new MenuAction("weiß", () => {
            document.body.style.background = "#FFFFFF";
        }, showFor))
        this.addAction(new MenuAction("blau", () => {
            document.body.style.background = "#C7DDF2";
        }))
        this.addAction(new MenuAction("türkis", () => {
            document.body.style.background = "#CAE7E7";
        }))
        this.addAction(new MenuAction("grün", () => {
            document.body.style.background = "#F0F3D0";
        }))
        this.addAction(new MenuAction("orange", () => {
            document.body.style.background = "#FEEAC9";
        }))
    }


}
