const imgSrc = require("../../img/steins/1SteinSideways.png");

export class ImageGenerator {
    async generateSideStoneRow(numSideStones) {
        let sideStoneImg = new Image();
        // @ts-ignore
        sideStoneImg.src = imgSrc;

        let canvas = document.createElement("canvas");

        return new Promise(resolve => {
            sideStoneImg.onload = () => {
                canvas.width = sideStoneImg.width*numSideStones;
                canvas.height = sideStoneImg.height;
                let context = canvas.getContext("2d");

                context.clearRect(0, 0, canvas.width, canvas.height);
                let offsetX = 0;
                for (let i = 0; i < numSideStones; i++) {
                    context.drawImage(sideStoneImg, offsetX, 0);
                    offsetX += sideStoneImg.width;
                }
                resolve(canvas.toDataURL());
            };
        });
    }
}