import {MyMenuSite} from "./MyMenuSite";
import {LinkSetFragment} from "../Fragments/LinkSetFragment";
import {Course} from "../../../shared/model/Course";
import {ListExerciseSite} from "./ListExerciseSite";
import {CourseHelper} from "../Helpers/CourseHelper";

const view = require("../../html/sites/ausbauSite.html");

/**
 * Seite zum Anzeigen der Ausbau-Kurse
 */
export class AusbauSite extends MyMenuSite {

    private linkFragment: LinkSetFragment;

    constructor(siteManager: any) {
        super(siteManager, view);
        this.linkFragment = new LinkSetFragment(this);
        this.addFragment("#link-fragment", this.linkFragment);
    }

    async onConstruct(constructParameters: any): Promise<any[]> {
        const res = super.onConstruct(constructParameters);
        const courses = await Course.findByIds([10, 11, 12, 13]);

        courses[1].name="Stuckateurarbeiten";

        this.linkFragment.setLinks(courses.map(c => {
            return {
                name: c.name,
                image: CourseHelper.editSource(c.icon),
                site: ListExerciseSite,
                constructParameters: {courseId: c.id}
            }
        }));

        return res;
    }
}
