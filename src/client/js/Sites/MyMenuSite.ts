import {MenuSite} from "cordova-sites/dist/client";
import {FooterFragment} from "../Fragments/FooterFragment";
import {StartSite} from "./StartSite";
import {Helper} from "js-helper/dist/shared";

const navbarLogo  = require("../../img/navbar_logo.svg");
const defaultTemplate = require("../../html/sites/myMenuSite.html");

/**
 * Base-Klasse bis auf wenige Ausnahmen, wie MbbLoginSite und MbbRegistrationSite.
 * Fügt das Footer-Fragment hinzu
 */
export class MyMenuSite extends MenuSite{

    private footerFragment: FooterFragment;

    constructor(siteManager: any, view: any, menuTemplate?: any) {
        super(siteManager, view, Helper.nonNull(menuTemplate, defaultTemplate));
        this.getNavbarFragment().setLogo(navbarLogo);

        this.footerFragment = new FooterFragment(this);
        this.addFragment("#footer-fragment", this.footerFragment)
    }

    async onViewLoaded() {
        const res = super.onViewLoaded();

        this.findBy(".logo-img").addEventListener("click", () => {
          console.log("click");
        });

        return res;
    }

    getFooterFragment(){
        return this.footerFragment;
    }
}
