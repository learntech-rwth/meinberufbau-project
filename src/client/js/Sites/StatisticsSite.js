import view from "../../html/sites/statisticsSite.html";
import {UserSite} from "cordova-sites-user-management/dist/client/js/Context/UserSite";
import {EasySyncClientDb} from "cordova-sites-easy-sync/dist/client/EasySyncClientDb";
import {App} from "cordova-sites/dist/client/js/App";
import {StatisticQueryBuilder} from "../../../shared/statisticQuery/StatisticQueryBuilder";
import {DataManager} from "cordova-sites/dist/client/js/DataManager";
import {MenuAction} from "cordova-sites/dist/client/js/Context/Menu/MenuAction/MenuAction";
import {StatisticFilterDialog} from "../Dialog/StatisticFilterDialog";
import {Helper} from "js-helper/dist/shared/Helper";
import {DateHelper} from "js-helper/dist/shared/DateHelper";
import {ExerciseStatisticsOverviewFragment} from "../Fragments/statistic/ExerciseStatisticsOverviewFragment";
import {WrongAnswerPerExerciseFragment} from "../Fragments/statistic/WrongAnswerPerExerciseFragment";
import {WrongAnswerPerDayFragment} from "../Fragments/statistic/WrongAnswerPerDayFragment";
import {MyMenuSite} from "./MyMenuSite";

/**
 * Bisher eine Seite, die noch nicht verwendet wird
 */
export class StatisticsSite extends MyMenuSite {

    constructor(siteManager) {
        super(siteManager, view);
        // this._userStatisticFragment = new UserStatisticsFragment(this);
        this._exerciseStatisticsOverviewFragment = new ExerciseStatisticsOverviewFragment(this);
        this._wrongAnswerPerExerciseFragment = new WrongAnswerPerExerciseFragment(this);
        this._wrongAnswerPerDayFragment = new WrongAnswerPerDayFragment(this);

        // this.addFragment("#user-statistic", this._userStatisticFragment);
        this.addFragment("#exercise-statistic", this._exerciseStatisticsOverviewFragment);
        this.addFragment("#wrong-answer-per-exercise-statistic", this._wrongAnswerPerExerciseFragment);
        this.addFragment("#wrong-answer-per-day-statistic", this._wrongAnswerPerDayFragment);

        this.addDelegate(new UserSite(this, "admin", true));

        this._start = DateHelper.strftime("%Y-%m-%d", new Date().setTime((new Date).getTime() - 1000 * 60 * 60 * 24 * 140));
        this._end = null;
        this._userId = null;

        this._online = true;
        this._filter = {
            start: this._start,
            end: this._end,
            userId: this._userId,
            online: this._online,
        };
    }

    onCreateMenu(navbar) {
        super.onCreateMenu(navbar);
        navbar.addAction(new MenuAction("filter", async () => {
            let res = await new StatisticFilterDialog(this._filter).show();

            if (Helper.isNotNull(res)) {
                this._filter = res;
                if (Helper.isNotNull(res["start"]) && res["start"].trim() !== "") {
                    this._start = res["start"];
                } else {
                    this._start = null;
                }
                if (Helper.isNotNull(res["end"]) && res["end"].trim() !== "") {
                    this._end = res["end"];
                } else {
                    this._end = null;
                }
                if (Helper.isNotNull(res["userId"]) && res["userId"].trim() !== "") {
                    this._userId = res["userId"];
                } else {
                    this._userId = null;
                }
                this._online = !!(Helper.isNotNull(res["online"]) && res["online"].trim() !== "");
                await this.filter();
            }
        }));
        return navbar;
    }

    async getOfflineStatistic() {
        let queryBuilder = await EasySyncClientDb.getInstance().createQueryBuilder();
        return await StatisticQueryBuilder.getSummedData(queryBuilder, this._start, this._end, this._userId);
    }

    async filter() {

        let queryRes = null;
        if (this._online) {
            queryRes = await this.getOnlineStatistics();
        } else {
            queryRes = await this.getOfflineStatistic();
        }

        queryRes["number-exercises-in-progress"] = queryRes["number-exercises"] - queryRes["number-exercises-executed"];
        queryRes["number-exercises-aborted"] = queryRes["number-exercises-executed"] - queryRes["number-exercises-done"];

        queryRes["average-time-exercises-done"] = queryRes["number-exercises-done"] > 0 ? (queryRes["time-exercises-done"] / queryRes["number-exercises-done"]) : 0;
        queryRes["average-time-exercises-aborted"] = queryRes["number-exercises-aborted"] > 0 ? (queryRes["time-exercises-aborted"] / queryRes["number-exercises-aborted"]) : 0;
        queryRes["average-time-exercises"] = (parseInt(queryRes["number-exercises-aborted"]) + parseInt(queryRes["number-exercises-done"])) > 0 ?
            ((parseInt(queryRes["time-exercises-done"]) + parseInt(queryRes["time-exercises-aborted"])) /
                (parseInt(queryRes["number-exercises-aborted"]) + parseInt(queryRes["number-exercises-done"]))) : 0;

        // this._userStatisticFragment.setStatistic(queryRes);
        this._exerciseStatisticsOverviewFragment.setStatistic(queryRes);
        this._wrongAnswerPerExerciseFragment.setStatistic(queryRes);
        this._wrongAnswerPerDayFragment.setStatistic(queryRes);
    }

    async onViewLoaded() {
        let res = super.onViewLoaded();

        await new Promise(resolve => {
            setTimeout(resolve, 25);
        });
        await this.filter();

        return res;
    }

    async getOnlineStatistics() {
        let data = await DataManager.load("/statistics" + DataManager.buildQueryWithoutNullValues({
            userId: this._userId,
            start: this._start,
            end: this._end,
        }));
        if (data.success) {
            return data.data;
        } else {
            return {}
        }
    }

    getFilter(){
        return this._filter;
    }
}

App.addInitialization((app) => {
    app.addDeepLink("statistic", StatisticsSite)
});
