import {MyMenuSite} from "./MyMenuSite";
import {LinkSetFragment} from "../Fragments/LinkSetFragment";
import {Course} from "../../../shared/model/Course";
import {ListExerciseSite} from "./ListExerciseSite";
import {CourseHelper} from "../Helpers/CourseHelper";

const view = require("../../html/sites/hochbauSite.html");

/**
 * Seite zum Anzeigen der Hochbau-Kurse
 */
export class HochbauSite extends MyMenuSite {

    private linkFragment: LinkSetFragment;

    constructor(siteManager: any) {
        super(siteManager, view);
        this.linkFragment = new LinkSetFragment(this);
        this.addFragment("#link-fragment", this.linkFragment);
    }

    async onConstruct(constructParameters: any): Promise<any[]> {
        const res = super.onConstruct(constructParameters);
        const courses = await Course.findByIds([1, 5, 9]);

        this.linkFragment.setLinks(courses.map(c => {
            return {
                name: c.name,
                image: CourseHelper.editSource(c.icon),
                site: ListExerciseSite,
                constructParameters: {courseId: c.id}
            }
        }));

        return res;
    }
}
