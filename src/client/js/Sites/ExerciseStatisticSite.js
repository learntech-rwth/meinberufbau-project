import view from "../../html/sites/exerciseStatisticsSite.html";

import {ExerciseStatisticsOverviewFragment} from "../Fragments/statistic/ExerciseStatisticsOverviewFragment";
import {WrongAnswerPerDayFragment} from "../Fragments/statistic/WrongAnswerPerDayFragment";
import {UserSite} from "cordova-sites-user-management/dist/client/js/Context/UserSite";
import {DateHelper} from "js-helper/dist/shared/DateHelper";
import {Helper} from "js-helper/dist/shared/Helper";
import {EasySyncClientDb} from "cordova-sites-easy-sync/dist/client/EasySyncClientDb";
import {StatisticQueryBuilder} from "../../../shared/statisticQuery/StatisticQueryBuilder";
import {DataManager} from "cordova-sites/dist/client/js/DataManager";
import {App, MenuAction, Toast} from "cordova-sites";
import {CountWrongAnswerFragment} from "../Fragments/statistic/CountWrongAnswerFragment";
import {StatisticFilterDialog} from "../Dialog/StatisticFilterDialog";
import {MyMenuSite} from "./MyMenuSite";

/**
 * Alte Seite, welche momentan nicht benutzt wird
 */
export class ExerciseStatisticSite extends MyMenuSite{

    constructor(siteManager) {
        super(siteManager, view);
        this._exerciseStatisticsOverviewFragment = new ExerciseStatisticsOverviewFragment(this);
        this._wrongAnswerPerDayFragment = new WrongAnswerPerDayFragment(this);
        this._countWrongAnswerFragment = new CountWrongAnswerFragment(this);

        this.addFragment("#exercise-statistic", this._exerciseStatisticsOverviewFragment);
        this.addFragment("#wrong-answer-per-day-statistic", this._wrongAnswerPerDayFragment);
        this.addFragment("#count-wrong-answer-statistic", this._countWrongAnswerFragment);

        this.addDelegate(new UserSite(this, "admin", true));

        this._online = true;
        this._filter = {
            start: DateHelper.strftime("%Y-%m-%d", new Date().setTime((new Date).getTime() - 1000 * 60 * 60 * 24 * 140)),
            end: null,
            userId: null,
            online: this._online,
        };
    }

    async onConstruct(constructParameters) {
        let res = super.onConstruct(constructParameters);

        if (Helper.isSet(constructParameters, "exercise")){
            this._exerciseId = constructParameters["exercise"];
        }
        else {
            new Toast("no exercise given!").show();
            this.finish();
        }

        if (Helper.isSet(constructParameters, "filter")){
            this._filter = JSON.parse(constructParameters["filter"]);
        }

        return res;
    }

    onCreateMenu(navbar) {
        super.onCreateMenu(navbar);
        navbar.addAction(new MenuAction("filter", async () => {
            let res = await new StatisticFilterDialog(this._filter).show();
            if (Helper.isNotNull(res)){
                this._filter = res;
                this._online = this._filter["online"];
                this.filter();
            }
        }));
        return navbar;
    }

    async getOfflineStatistic() {
        let queryBuilder = await EasySyncClientDb.getInstance().createQueryBuilder();
        return await StatisticQueryBuilder.getExerciseStatistic(queryBuilder, this._filter["start"], this._filter["end"], this._filter["user"], this._exerciseId);
    }

    async filter() {

        let queryRes = null;
        if (this._online) {
            queryRes = await this.getOnlineStatistics();
        } else {
            queryRes = await this.getOfflineStatistic();
        }

        queryRes["number-exercises-in-progress"] = queryRes["number-exercises"] - queryRes["number-exercises-executed"];
        queryRes["number-exercises-aborted"] = queryRes["number-exercises-executed"] - queryRes["number-exercises-done"];

        queryRes["average-time-exercises-done"] = queryRes["number-exercises-done"] > 0 ? (queryRes["time-exercises-done"] / queryRes["number-exercises-done"]) : 0;
        queryRes["average-time-exercises-aborted"] = queryRes["number-exercises-aborted"] > 0 ? (queryRes["time-exercises-aborted"] / queryRes["number-exercises-aborted"]) : 0;
        queryRes["average-time-exercises"] = (parseInt(queryRes["number-exercises-aborted"]) + parseInt(queryRes["number-exercises-done"])) > 0 ?
            ((parseInt(queryRes["time-exercises-done"]) + parseInt(queryRes["time-exercises-aborted"])) /
                (parseInt(queryRes["number-exercises-aborted"]) + parseInt(queryRes["number-exercises-done"]))) : 0;

        this._exerciseStatisticsOverviewFragment.setStatistic(queryRes);
        this._wrongAnswerPerDayFragment.setStatistic(queryRes);
        this._countWrongAnswerFragment.setStatistic(queryRes);
    }

    async onViewLoaded() {
        let res = super.onViewLoaded();

        await new Promise(resolve => {
            setTimeout(resolve, 250);
        });
        await this.filter();

        return res;
    }

    async getOnlineStatistics() {
        let data = await DataManager.load("/statistics" + DataManager.buildQueryWithoutNullValues({
            userId: this._filter["userid"],
            start: this._filter["start"],
            end: this._filter["end"],
            exerciseId: this._exerciseId
        }));
        if (data.success) {
            return data.data;
        } else {
            return {}
        }
    }
}
App.addInitialization(app => {
    app.addDeepLink("exerciseStatistic", ExerciseStatisticSite);
});
