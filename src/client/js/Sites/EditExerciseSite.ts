const view = require("../../html/sites/editEcerciseSiteNew.html");

import {App, Form, Translator} from "cordova-sites/dist/client";
import {UserSite} from "cordova-sites-user-management/dist/client";
import {Course} from "../../../shared/model/Course";
import {Exercise} from "../../../shared/model/Exercise";
import {ShowExerciseSite} from "./ShowExerciseSite";
import {EditStorybookSite} from "./EditStorybookSite";
import {MyMenuSite} from "./MyMenuSite";
import {FileMedium} from "cordova-sites-easy-sync/dist/shared";
import {Helper} from "js-helper/dist/shared";
import {ViewHelper} from "js-helper/dist/client/ViewHelper";

/**
 * Diese Seite ist zum bearbeiten von Exercises. Dabei geht es nicht im Storybooks, sondern
 * um andere. Hierbei wird ein möglichst generelles System angewendet.
 *
 * Es gibt Zahlen, Text, Auswahl, versteckte Attribute und ein Bild, welche man hinzufügen kann.
 * Jeder Aufgabentyp hat andere Attribute.
 */
export class EditExerciseSite extends MyMenuSite {
    static readonly EXERCISE_TYPE = {
        WALL_LENGTH: "element-wall-length-exercise",
        WALL_LENGTH_EIGHT: "element-wall-length-eight-exercise",
        BUILD_WALL: "element-build-wall-dimension"
    };

    static readonly TYPE = {
        NUMBER: 1,
        TEXT: 2,
        SELECTION: 3,
        HIDDEN: 4,
        EXERCISE_IMAGE: 5,
    };

    /**
     * Definierung der einzelnen Attribute für die unterschiedlichen Aufgabentypen.
     * @private
     */
    private static readonly EXERCISE_VALUES = {
        WALL_LENGTH: {
            "num-whole-stone": EditExerciseSite.TYPE.NUMBER,
            "num-three-quarter-stone": EditExerciseSite.TYPE.NUMBER,
            "num-half-stone": EditExerciseSite.TYPE.NUMBER,
            "num-quarter-stone": EditExerciseSite.TYPE.NUMBER,
            "num-stones-sideways": EditExerciseSite.TYPE.NUMBER,
            "exercise-description": EditExerciseSite.TYPE.TEXT,
            "exercise-image-description": EditExerciseSite.TYPE.TEXT,
            "exercise-image": {
                type: EditExerciseSite.TYPE.EXERCISE_IMAGE,
            },
        },
        WALL_LENGTH_EIGHT: {
            "num-heads": EditExerciseSite.TYPE.NUMBER,
            "mass-to-calculate": {
                type: EditExerciseSite.TYPE.SELECTION,
                values: [
                    "aussenmass",
                    "oeffnungsmass",
                    "anbaumass"
                ]
            },
            "exercise-description": EditExerciseSite.TYPE.TEXT,
            "exercise-image-description": EditExerciseSite.TYPE.TEXT,
            "exercise-image": EditExerciseSite.TYPE.EXERCISE_IMAGE,
        },
        BUILD_WALL: {
            "exercise-description": EditExerciseSite.TYPE.TEXT,
            "exercise-length-wall": {
                type: EditExerciseSite.TYPE.SELECTION,
                values: [
                    "86.5",
                    "124",
                ]
            },
            "exercise-breadth-wall": {
                type: EditExerciseSite.TYPE.NUMBER,
                readonly: true,
                value: 125,
            },
        }
    };

    /**
     * Validatoren für die unterschiedlichen Typen. Da das nicht einfach abzudecken ist,
     * wird hierfür eine Callback angegeben.
     * @private
     */
    private static readonly VALIDATORS = {
        WALL_LENGTH: (values, generating) => {
            let errors = {};
            let hasErrors = false;
            if (!generating) {
                //Testen, ob überhaupt steine gesetzt wurden
                if (parseInt(values["num-whole-stone"])
                    + parseInt(values["num-three-quarter-stone"])
                    + parseInt(values["num-half-stone"])
                    + parseInt(values["num-quarter-stone"])
                    + parseInt(values["num-stones-sideways"]) <= 0) {
                    errors["num-stone"] = "number stones must be greater 0";
                    hasErrors = true;
                }

                //Man braucht ein Bild, falls nicht generiert wird
                if (Helper.imageUrlIsEmpty(values["exercise-image"])) {
                    errors["exercise-image"] = "Image is required";
                    hasErrors = true;
                }
            }

            //Falls Bild gesetzt, aber keine Bildbeschreibung
            if (values["exercise-image"].trim() !== "" && values["exercise-image-description"].trim() === "") {
                errors["exercise-image-description"] = "image-description must be set";
                hasErrors = true;
            }
            if (hasErrors) {
                return errors;
            }
            return true;
        },
        WALL_LENGTH_EIGHT: (values, generating) => {
            let errors = {};
            let hasErrors = false;

            if (!generating) {
                if (parseInt(values["num-heads"]) <= 0) {
                    errors["num-heads"] = "number heads must be greater 0";
                    hasErrors = true;
                }
                if (Helper.imageUrlIsEmpty(values["exercise-image"])) {
                    errors["exercise-image"] = "Image is required";
                    hasErrors = true;
                }
            }
            if (values["exercise-image"].trim() !== "" && values["exercise-image-description"].trim() === "") {
                errors["exercise-image-description"] = "image-description must be set";
                hasErrors = true;
            }
            if (hasErrors) {
                return errors;
            }
            return true;
        },
        BUILD_WALL: (values, generating) => {
            let errors = {};
            let hasErrors = false;

            if (!generating) {

                if (parseFloat(values["exercise-length-wall"]) <= 0) {
                    errors["exercise-length-wall"] = "value must be higher than 0";
                }
                if (parseFloat(values["exercise-breadth-wall"]) <= 0) {
                    errors["exercise-breadth-wall"] = "value must be higher than 0";
                }
            }

            if (hasErrors) {
                return errors;
            }
            return true;
        }
    };

    private _exercise: Exercise;
    private _courseId: number;
    private _exerciseType: string;
    private _courses: { [key: number]: Course };
    private _valueContainer: HTMLElement;
    private _exValues: any;
    private _form: Form;
    private _hiddenTemplate: HTMLElement;
    private _numberTemplate: HTMLElement;
    private _textTemplate: HTMLElement;
    private selectionTemplate: HTMLElement;
    private _imageTemplate: HTMLElement;

    constructor(siteManager) {
        super(siteManager, view);
        this.addDelegate(new UserSite(this, "admin"));
    }

    async onConstruct(constructParameters) {
        let res = super.onConstruct(constructParameters);

        this._exercise = null;
        this._courseId = null;
        if (Helper.isNotNull(constructParameters["course"])) {
            this._courseId = parseInt(constructParameters["course"]);
        }
        this._exerciseType = constructParameters["type"];

        if (Helper.isNotNull(constructParameters["exercise"])) {
            this._exercise = await Exercise.findById(constructParameters["exercise"], Exercise.getRelations());
        }

        //Checke, ob neue oder bereits existierende Aufgabe
        if (Helper.isNull(this._exercise)) {
            this._exercise = new Exercise();
        } else {
            //Wenn ein Storybook bearbeitet wird, leite an die entsprechende Seite weiter
            if (this._exercise.elementType === "storybook") {
                //Nicht das beenden awaiten, weil das in onConstruct zu einem Deadlock führen kann!
                this.finishAndStartSite(EditStorybookSite, {course: this._courseId, exercise: this._exercise.id})
                return res;
            }
            this._exerciseType = this._exercise.elementType;
            this._courseId = this._exercise.course.id;
        }

        let courses = await Course.find(undefined, {"id": "ASC"});
        this._courses = Helper.arrayToObject(courses, course => course.id);

        return res
    }

    async onViewLoaded() {
        let res = super.onViewLoaded();

        this._valueContainer = this.findBy("#value-container");

        //Builde die Auswahl der Kurse
        let selectCourseInput = this.findBy("#article-course");
        Object.keys(this._courses).forEach(courseId => {
            let option = document.createElement("option");
            option.value = courseId;
            option.innerText = this._courses[courseId].name;

            if (parseInt(courseId) === this._courseId) {
                option.selected = true;
            }
            selectCourseInput.appendChild(option);
        });

        this._setTemplates();

        let {values, validator} = EditExerciseSite.getTypeInfos(this._exerciseType);
        this._exValues = values;

        this.buildFormInputs(values);

        this._handleForm(validator);
        this.setFormValues(values);

        return res;
    }

    _handleForm(validator) {
        let generatedCheckbox = this.findBy("#generate-exercise-checkbox");
        let formElem = this.findBy("#edit-exercise-form");

        //Füge Preview für das Bild hinzu
        const imageInput = formElem.querySelector("[name=exercise-image]");
        imageInput.addEventListener("input", () => {
            if (imageInput.files && imageInput.files[0]) {
                let reader = new FileReader();
                reader.onload = e => {
                    // @ts-ignore
                    formElem.querySelector(".exercise-image-preview").src = e.target.result;
                };
                reader.readAsDataURL(imageInput.files[0]);
            } else {
                formElem.querySelector(".exercise-image-preview").src = "";
            }
            formElem.querySelector("[name=image-changed]").value = "1";
        });

        this._form = new Form(formElem, async values => {
            //Listener wird ausgeführt, wenn das Formular "abgesendet" wird.
            this.showLoadingSymbol();

            let data = this.getDataFromFormValues(values, generatedCheckbox.checked);

            //Setze allgemeine Daten
            let exercise = this._exercise;
            exercise.setName(data["exercise-name"]);
            exercise.setGeneratingData(data);
            exercise.setStage(parseInt(values["stage"]));
            exercise.setElementType(this._exerciseType);
            exercise.isGenerating = generatedCheckbox.checked;
            exercise.course = this._courses[values["course"]];
            exercise.description = values["short-description"];

            //Update Bild, falls dieses gesetzt wurde
            let exerciseImg = exercise.image;
            if (values["image-changed"] === "1") {
                if (!Helper.imageUrlIsEmpty(values["exercise-image"])) {
                    if (!exerciseImg) {
                        exerciseImg = new FileMedium();
                        exercise.image = exerciseImg;
                    }
                    exerciseImg.setSrc(values["exercise-image"])
                    await exerciseImg.save();
                } else if (exerciseImg) {
                    exerciseImg.setSrc("");
                    await exerciseImg.save();
                }
            }

            if (!exercise.course.activated) {
                exercise.course.activated = true;
                await exercise.course.save();
            }

            //Speichere und starte Exercise
            await exercise.save();
            this.removeLoadingSymbol();
            this.finishAndStartSite(ShowExerciseSite, {"exerciseId": exercise.getId()})
        });

        this._form.addValidator(values => {
            //Checke Data auf validität

            let data = this.getDataFromFormValues(values);

            //Checke
            let validatorErrors = validator(data, generatedCheckbox.checked);
            let hasErrors = false;

            //Setze Fehler bei entsprechenden Elementen, damit diese angezeigt werden
            let errors = {};
            Object.keys(validatorErrors).forEach(valName => {
                hasErrors = true;
                if (typeof this._exValues[valName] !== "object") {
                    this._exValues[valName] = {type: this._exValues[valName]}
                }

                switch (this._exValues[valName].type) {
                    case EditExerciseSite.TYPE.NUMBER: {
                        errors["number-" + valName + "-lb"] = validatorErrors[valName];
                        break;
                    }
                    case EditExerciseSite.TYPE.TEXT: {
                        errors["text-" + valName] = validatorErrors[valName];
                        break;
                    }
                    case EditExerciseSite.TYPE.SELECTION: {
                        if (generatedCheckbox.checked) {
                            errors["checkbox-" + valName] = validatorErrors[valName];
                        } else {
                            errors["selection-" + valName] = validatorErrors[valName];
                        }
                        break;
                    }
                    case EditExerciseSite.TYPE.EXERCISE_IMAGE: {
                        errors["image-" + valName] = validatorErrors[valName];
                        break;
                    }
                    default: {
                        errors[valName] = validatorErrors[valName];
                        break;
                    }
                }
            });


            if (generatedCheckbox.checked) {
                Object.keys(values).forEach(valName => {
                    //generierung von Zahlen: lower bound muss niedriger sein als upper bound
                    if (valName.startsWith("number-")) {
                        let realName = valName.substring(7, valName.length - 3);
                        let lb = "number-" + realName + "-lb";
                        let up = "number-" + realName + "-ub";
                        if (parseFloat(values[lb]) > parseFloat(values[up])) {
                            errors[up] = "value must be higher than the min value!";
                            hasErrors = true;
                        }
                    }
                });
                Object.keys(this._exValues).forEach(valName => {
                    if (typeof this._exValues[valName] !== "object") {
                        this._exValues[valName] = {type: this._exValues[valName]}
                    }

                    //Generierung von Selektion: mindestens eine Checkbox muss ausgewählt werden
                    if (this._exValues[valName].type === EditExerciseSite.TYPE.SELECTION && (Helper.isNull(data[valName]) || data[valName].length === 0)) {
                        errors[valName] = "Select at least one value of " + valName
                    }
                })
            }

            if (hasErrors) {
                return errors;
            }
            return true;
        });

        //Wechsel der Form-Inputs zwischen generated und normalen
        generatedCheckbox.addEventListener("change", () => {
            if (generatedCheckbox.checked) {
                formElem.classList.add("generated");
            } else {
                formElem.classList.remove("generated");
            }
        });

        return formElem;
    }

    /**
     * Lädt Templates für die unterschiedlichen Typen
     */
    _setTemplates() {
        this._numberTemplate = this.findBy("#number-template");
        this._numberTemplate.remove();
        this._numberTemplate.removeAttribute("id");

        this._textTemplate = this.findBy("#text-template");
        this._textTemplate.remove();
        this._textTemplate.removeAttribute("id");

        this.selectionTemplate = this.findBy("#selection-template");
        this.selectionTemplate.remove();
        this.selectionTemplate.removeAttribute("id");

        this._hiddenTemplate = this.findBy("#hidden-template");
        this._hiddenTemplate.remove();
        this._hiddenTemplate.removeAttribute("id");

        this._imageTemplate = this.findBy("#image-template");
        this._imageTemplate.remove();
        this._imageTemplate.removeAttribute("id");
    }

    static _getTypeIndex(type) {
        let index = null;

        Object.keys(EditExerciseSite.EXERCISE_TYPE).some(exType => {
            if (EditExerciseSite.EXERCISE_TYPE[exType] === type) {
                index = exType;
                return true;
            }
        });

        return index;
    }

    static getTypeInfos(type) {

        let index = this._getTypeIndex(type);
        if (index !== null) {
            return {
                values: EditExerciseSite.EXERCISE_VALUES[index],
                validator: EditExerciseSite.VALIDATORS[index]
            };
        }
        return {
            values: null,
            validator: null
        }
    }

    /**
     * Diese Funktion extrahiert die Daten aus dem Formular
     * @param values
     * @param isGenerating
     * @private
     */
    private getDataFromFormValues(values, isGenerating?) {
        let data = {};
        Object.keys(values).forEach(valName => {
            if (valName.startsWith("number-")) {
                let realName = valName.substring(7, valName.length - 3);
                if (!data[realName]) {
                    if (isGenerating) {
                        data[realName] = {
                            "min": parseFloat(values["number-" + realName + "-lb"]),
                            "max": parseFloat(values["number-" + realName + "-ub"])
                        }
                    } else {
                        data[realName] = parseFloat(values["number-" + realName + "-lb"]);
                    }
                }
            } else if (valName.startsWith("text-")) {
                let realName = valName.substring(5);
                data[realName] = values[valName];
            } else if (valName.startsWith("checkbox-") && isGenerating) {
                let parts = valName.split("-");
                let index = parseInt(parts[parts.length - 1]);
                let name = parts.slice(1, -1).join("-");

                if (!data[name]) {
                    data[name] = [];
                }

                data[name].push(this._exValues[name].values[index]);
            } else if (valName.startsWith("selection-") && !isGenerating) {
                let realName = valName.substring(10);
                data[realName] = values[valName];
            } else if (valName.startsWith("hidden-")) {
                let realName = valName.substring(7);
                data[realName] = values[valName];
            } else if (valName.startsWith("image-before-")) {
                let realName = valName.substring(13);
                if (Helper.imageUrlIsEmpty(data[realName])) {
                    data[realName] = values[valName];
                }
            } else if (valName.startsWith("image-")) {
                let realName = valName.substring(6);
                if (Helper.imageUrlIsEmpty(data[realName]) || !Helper.imageUrlIsEmpty(values[valName])) {
                    data[realName] = values[valName];
                }
            } else if (valName === "exercise-number" || valName === "exercise-name") {
                data[valName] = values[valName];
            }
        });

        return data;
    }

    private buildFormInputs(values) {
        if (Array.isArray(values)) {
            let newValues = {};
            values.forEach(val => {
                newValues[val] = {type: EditExerciseSite.TYPE.NUMBER}
            });
            values = newValues;
        }

        Object.keys(values).forEach(valName => {
            if (typeof values[valName] !== "object") {
                values[valName] = {type: values[valName]}
            }
        });

        let varNames = Object.keys(values).filter(name => {
            return ([EditExerciseSite.TYPE.NUMBER, EditExerciseSite.TYPE.SELECTION].indexOf(values[name].type) !== -1);
        });
        this.findBy("#variable-explanation").appendChild(Translator.makePersistentTranslation("variable-explanation", ["%%" + varNames.join("%%, %%") + "%%"]));

        ViewHelper.removeAllChildren(this._valueContainer);

        Object.keys(values).forEach(valName => {
            if (typeof values[valName] !== "object") {
                values[valName] = {type: values[valName]}
            }

            switch (values[valName].type) {
                case EditExerciseSite.TYPE.NUMBER: {
                    this._addNumberValue(valName, values[valName]);
                    break;
                }
                case EditExerciseSite.TYPE.TEXT: {
                    this._addTextValue(valName, values[valName], values);
                    break;
                }
                case EditExerciseSite.TYPE.SELECTION: {
                    this._addSelectionValue(valName, values[valName]);
                    break;
                }
                case EditExerciseSite.TYPE.HIDDEN: {
                    this._addHiddenValue(valName, values[valName]);
                    break;
                }
                case EditExerciseSite.TYPE.EXERCISE_IMAGE: {
                    this._addImageValue(valName, values[valName]);
                    break;
                }
            }
        })
    }

    _addNumberValue(valName, value) {
        let numberElem = <HTMLElement>this._numberTemplate.cloneNode(true);
        numberElem.querySelector(".min-heading").appendChild(document.createTextNode(valName));
        numberElem.querySelector(".max-heading").appendChild(document.createTextNode(valName));

        numberElem.querySelector(".min").setAttribute("name", "number-" + valName + "-lb");
        numberElem.querySelector(".max").setAttribute("name", "number-" + valName + "-ub");

        if (value["readonly"] === true) {
            numberElem.querySelector(".min").setAttribute("readonly", "readonly");
            numberElem.querySelector(".max").setAttribute("readonly", "readonly");
        }

        if (value["value"]) {
            numberElem.querySelector(".min").setAttribute("value", value["value"]);
            numberElem.querySelector(".max").setAttribute("value", value["value"]);
        }

        this._valueContainer.appendChild(numberElem);
    }

    _addTextValue(valName, value, values) {
        let textElem = <HTMLElement>this._textTemplate.cloneNode(true);
        textElem.querySelector(".text-heading").appendChild(document.createTextNode(valName));
        textElem.querySelector(".text").setAttribute("name", "text-" + valName);

        this._valueContainer.appendChild(textElem);
    }

    _addSelectionValue(valName, value) {
        let checkboxElem = <HTMLElement>this.selectionTemplate.cloneNode(true);
        let checkboxValueElem = checkboxElem.querySelector(".value-of-checkbox");
        checkboxValueElem.remove();

        checkboxElem.querySelector(".selection-heading").appendChild(document.createTextNode(valName));
        let selectElement = <HTMLSelectElement>checkboxElem.querySelector(".selection-element");
        selectElement.name = "selection-" + valName;

        value.values.forEach((checkboxValue, i) => {
            let optionsElement = document.createElement("option");
            optionsElement.value = checkboxValue;
            optionsElement.appendChild(document.createTextNode(checkboxValue));
            selectElement.appendChild(optionsElement);

            let currentElem = <HTMLElement>checkboxValueElem.cloneNode(true);
            (currentElem.querySelector(".checkbox") as HTMLInputElement).name = ("checkbox-" + valName + "-" + i);
            currentElem.querySelector(".checkbox-heading").appendChild(document.createTextNode(checkboxValue));
            checkboxElem.appendChild(currentElem);
        });

        this._valueContainer.appendChild(checkboxElem);
    }

    _addHiddenValue(valName, value) {
        let hiddenElem = <HTMLElement>this._hiddenTemplate.cloneNode(true);
        let hiddenInputElem = <HTMLInputElement>hiddenElem.querySelector(".hidden-value");

        hiddenInputElem.name = "hidden-" + valName;
        hiddenInputElem.value = value.value;

        this._valueContainer.append(hiddenInputElem);
    }

    _addImageValue(valName, value) {
        let imageElem = <HTMLElement>this._imageTemplate.cloneNode(true);
        let imageInputElem = <HTMLInputElement>imageElem.querySelector(".image-element");
        let imagePreview = <HTMLImageElement>imageElem.querySelector(".preview-image");
        let imageBeforeInput = <HTMLInputElement>imageElem.querySelector(".image-before");
        imagePreview.classList.add("image-preview-" + valName);
        imageBeforeInput.name = "image-before-" + valName;

        imageInputElem.name = "image-" + valName;
        imageInputElem.addEventListener("change", () => {
            if (imageInputElem.files && imageInputElem.files[0]) {
                let reader = new FileReader();
                reader.onload = e => {
                    // @ts-ignore
                    imagePreview.src = e.target.result;
                };
                reader.readAsDataURL(imageInputElem.files[0]);
            }
        });

        this._valueContainer.append(imageElem);
    }

    private setFormValues(fields) {
        if (this._exercise.id == null) {
            return;
        }

        let formValues = {};

        //Setze die allgemeinen Infos
        formValues["exercise-name"] = this._exercise.getName();
        formValues["course"] = this._exercise.course.id;
        formValues["stage"] = this._exercise.getStage();
        formValues["short-description"] = this._exercise.description;

        //Datei-Inputs können nicht gesetzt werden. Daher setze hier das Preview-Element
        if (this._exercise.image) {
            this.findBy(".exercise-image-preview").src = this._exercise.image.getUrl();
        }

        let generatingData = this._exercise.generatingData;
        let isGenerating = this._exercise.isGenerating;

        if (isGenerating) {
            formValues["generate-exercise"] = "1";
            this._form.getFormElement().classList.add("generated");
        }

        //Setze die anderen Form-Values
        Object.keys(fields).forEach(field => {
            if (typeof fields[field] !== "object") {
                fields[field] = {type: fields[field]};
            }
            switch (fields[field].type) {
                case EditExerciseSite.TYPE.NUMBER: {
                    if (isGenerating) {
                        formValues["number-" + field + "-ub"] = generatingData[field]["max"];
                        formValues["number-" + field + "-lb"] = generatingData[field]["min"];
                    } else {
                        formValues["number-" + field + "-lb"] = generatingData[field];
                    }
                    break;
                }
                case EditExerciseSite.TYPE.TEXT: {
                    formValues["text-" + field] = generatingData[field];
                    break;
                }
                case EditExerciseSite.TYPE.SELECTION: {
                    if (isGenerating) {
                        generatingData[field].forEach(val => {
                            let index = fields[field].values.indexOf(val);
                            formValues["checkbox-" + field + "-" + index] = "1";
                        });
                    } else {
                        formValues["selection-" + field] = generatingData[field];
                    }
                    break;
                }
                case EditExerciseSite.TYPE.HIDDEN: {
                    formValues["hidden-" + field] = generatingData[field];
                    break;
                }
                case EditExerciseSite.TYPE.EXERCISE_IMAGE: {
                    formValues["image-before-" + field] = generatingData[field];

                    this.findBy(".image-preview-" + field).src = generatingData[field];
                    this.findBy("input[type='file'][name='image-" + field + "']").removeAttribute("required");

                    break;
                }
            }
        });
        this._form.setValues(formValues);
    }
}

App.addInitialization((app) => {
    app.addDeepLink("editExercise", EditExerciseSite);
});
