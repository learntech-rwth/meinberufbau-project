import {MyMenuSite} from "./MyMenuSite";
import {App} from "cordova-sites/dist/client";

const view = require("../../html/sites/impressum.html");

/**
 * Seite zum Anzeigen der Definitionen
 */
export class Impressum extends MyMenuSite {

  constructor(siteManager: any) {
      super(siteManager, view);
  }
}

//Wenn im Footer-Fragment inkludiert wird, sorgt das für Fehler. Daher hier als globales Objekt setzen, auf
// welches das Footer-Fragment zugreifen kann
window["Impressum"] = Impressum;

App.addInitialization((app) => {
    app.addDeepLink("impressum", Impressum);

});
