import {Helper} from "js-helper/dist/shared";

const view = require("../../html/sites/listExercisesSite.html");
import {ShowExerciseSite} from "./ShowExerciseSite";
import {SelectExerciseDialog} from "../Dialog/SelectExerciseDialog";
import {App} from "cordova-sites/dist/client";
import {UserMenuAction} from "cordova-sites-user-management/dist/client";
import {Course} from "../../../shared/model/Course"
import {Exercise} from "../../../shared/model/Exercise";
import {EditExerciseSite} from "./EditExerciseSite";
import {EditStorybookSite} from "./EditStorybookSite";
import {MyMenuSite} from "./MyMenuSite";
import {CourseHelper} from "../Helpers/CourseHelper";
import {ViewHelper} from "js-helper/dist/client/ViewHelper";

/**
 * Diese Seite zeigt die Exercises eines Kurses an
 */
export class ListExerciseSite extends MyMenuSite {
    private _courseId: number;
    private course: Course;

    private _exerciseTemplate: HTMLElement;
    private _exerciseContainerStage1: HTMLElement;
    private _exerciseContainerStage2: HTMLElement;
    private _exerciseContainerStage3: HTMLElement;
    private _exerciseContainerStage4: HTMLElement;

    constructor(siteManager) {
        super(siteManager, view);
    }

    async onConstruct(constructParameters) {
        let res = super.onConstruct(constructParameters);
        this._courseId = constructParameters["courseId"];

        this.course = await Course.findById(this._courseId);

        return res;
    }

    async onViewLoaded() {
        const res = super.onViewLoaded();

        this._exerciseTemplate = this.findBy("#article-template");
        this._exerciseTemplate.remove();
        this._exerciseTemplate.removeAttribute("id");

        this._exerciseContainerStage1 = this.findBy("#exercise-container-stage-1");
        this._exerciseContainerStage2 = this.findBy("#exercise-container-stage-2");
        this._exerciseContainerStage3 = this.findBy("#exercise-container-stage-3");
        this._exerciseContainerStage4 = this.findBy("#exercise-container-stage-4");
        
        if(this.course.name=="Stukkateurarbeiten"){
          this.findBy("#course-name").innerText = "Stuckateurarbeiten";
        }else{
          this.findBy("#course-name").innerText = this.course.name;
        }




         //Fügt die Listener zum Blättern zur nächsten Aufgabe hinzu. Durch CSS
         // wird sicher gestellt, dass diese auch einrasten
        this.findBy(".stage", true).forEach(stage => {
            const exerciseContainer = stage.querySelector(".exercise-container");
            stage.querySelector(".next-exercise").addEventListener("click", () => {
                exerciseContainer.scrollLeft += exerciseContainer.clientWidth;
            });
            stage.querySelector(".previous-exercise").addEventListener("click", () => {
                exerciseContainer.scrollLeft -= exerciseContainer.clientWidth;
            })
        })

        return res;
    }

    async _updateExercises() {
        let exercises: Exercise[];

        //Lädt entweder die Exercises eines Kurses oder alle Exercises. Wird immer nach ID sortiert, um eine
        // einheitliche Reihenfolge zu haben.
        if (Helper.isNotNull(this._courseId)) {
            exercises = await Exercise.find({course: {id: this._courseId}}, {"id": "ASC"}, undefined, undefined, Exercise.getRelations());
        } else {
            exercises = await Exercise.find(undefined, {"id": "ASC"}, undefined, undefined, Exercise.getRelations());
        }

        //Entfernt alle bisherigen Aufgaben von der view
        ViewHelper.removeAllChildren(this._exerciseContainerStage1);
        ViewHelper.removeAllChildren(this._exerciseContainerStage2);
        ViewHelper.removeAllChildren(this._exerciseContainerStage3);
        ViewHelper.removeAllChildren(this._exerciseContainerStage4);

        //Versteckt alle Stages
        this.findBy(".stage", true).forEach(stage => stage.classList.add("hidden"));

        //fügt die Exercises hinzu
        exercises.forEach(exercise => this._addExercise(exercise));
    }

    onCreateMenu(navbar) {
        navbar.addAction(new UserMenuAction("edit-article-site", "admin", async () => {
            //fragt welche Exercise erstellt werden soll und startet dann die entsprechende Seite
            let type = await new SelectExerciseDialog().show();
            if (typeof type === "string") {
                if (type === "storybook") {
                    this.startSite(EditStorybookSite, {courseId: this._courseId});
                } else {
                    this.startSite(EditExerciseSite, {type: type, course: this._courseId});
                }
            }
        }));
    }

    async onStart(pauseArguments) {
        await super.onStart(pauseArguments);
        await this._updateExercises();
    }

    /**
     * Fügt eine Exercise der View hinzu
     *
     * @param {Exercise} exercise
     * @private
     */
    _addExercise(exercise: Exercise) {
        let exerciseElement = <HTMLElement>this._exerciseTemplate.cloneNode(true);

        //setze den Namen
        let name = exerciseElement.querySelector(".name");
        name.appendChild(document.createTextNode(exercise.getName() + (exercise.isGenerating ? " (generated)" : "")));

        //Starte entsprechende Seite, wenn auf die Exercise geklickt wird
        exerciseElement.addEventListener("click", () => {
            this.startSite(ShowExerciseSite, {exerciseId: exercise.getId()})
        });

        let imgSrc;

        //Wenn Bild existiert und URL nicht leer ist
        if (exercise.image && !Helper.imageUrlIsEmpty(exercise.image?.getUrl())) {
            imgSrc = exercise.image.getUrl();
        } else {
            //Sonst nehme das Bild des Kurses
            imgSrc = CourseHelper.editSource(this.course.icon);
        }

        //Setze Bild und Beschreibung
        (exerciseElement.querySelector(".exercise-image") as HTMLImageElement).src = imgSrc;
        if (exercise.description) {
            (exerciseElement.querySelector(".description") as HTMLElement).innerText = exercise.description;
        }

        //Finde passenden Kontainer und zeige ihn an (durch removing von hidden klasse)
        let container: HTMLElement;
        switch (exercise.getStage()) {
            case Exercise.STAGES.ONE: {
                container = this._exerciseContainerStage1;
                this.findBy("#stage-1").classList.remove("hidden")
                break;
            }
            case Exercise.STAGES.TWO: {
                container = this._exerciseContainerStage2;
                this.findBy("#stage-2").classList.remove("hidden")
                break;
            }
            case Exercise.STAGES.THREE: {
                container = this._exerciseContainerStage3;
                this.findBy("#stage-3").classList.remove("hidden")
                break;
            }
            default: {
                container = this._exerciseContainerStage4;
                this.findBy("#stage-4").classList.remove("hidden")
                break;
            }
        }

        container.appendChild(exerciseElement);
    }
}

App.addInitialization((app) => {
    app.addDeepLink("course", ListExerciseSite);
});
