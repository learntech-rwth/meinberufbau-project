import {Helper} from "js-helper/dist/shared";

const view = require("../../html/sites/lexiconSite.html");
import {Lexicon} from "../Lexicon";
import {AddDefinitionDialog} from "../Dialog/AddDefinitionDialog";
import {ShowDefinitionDialog} from "../Dialog/ShowDefinitionDialog";
import {App, ConfirmDialog, Translator} from "cordova-sites/dist/client";
import {UserMenuAction} from "cordova-sites-user-management/dist/client/js/MenuAction/UserMenuAction";
import {UserManager} from "cordova-sites-user-management/dist/client";
import {MyMenuSite} from "./MyMenuSite";
import {ViewHelper} from "js-helper/dist/client/ViewHelper";
import {Definition} from "../../../shared/model/Definition";

/**
 * Seite zum Anzeigen der Definitionen
 */
export class LexiconSite extends MyMenuSite {
    private _definitionTemplate: HTMLElement;
    private _definitionContainer: HTMLElement;

    constructor(siteManager) {
        super(siteManager, view);
    }

    onCreateMenu(navbar) {
        //Füge neue MenuAction hinzu, um Definitionen zu definieren, klappt nur, wenn User eingeloggt ist
        navbar.addAction(new UserMenuAction("add-definition", "loggedIn", async () => {
            await new AddDefinitionDialog().show();
            await this.addDefinitions();
        }));

        super.onCreateMenu(navbar);
    }

    async onViewLoaded() {
        this._definitionTemplate = this.findBy(".definition");
        this._definitionTemplate.remove();

        this._definitionContainer = this.findBy("#definition-container");

        await this.addDefinitions();
        return super.onViewLoaded();
    }

    async addDefinitions() {
        ViewHelper.removeAllChildren(this._definitionContainer);
        const definitions = await Lexicon.getInstance().getDefinitions(true, true);

        await Helper.asyncForEach(definitions, async definition => {
            await this.addDefinitionElement(definition);
        });

        //Falls keine Definitionen gefunden, zeige Hinweis an, dass keine definiert wurden
        if (definitions.length === 0) {
            this._definitionContainer.appendChild(Translator.getInstance().makePersistentTranslation("no definitions"))
        }

        //Übersetze die Definitionen, welche gefunden wurden
        await Translator.getInstance().updateTranslations(this._definitionContainer);
    }

    private async addDefinitionElement(definition: Definition) {
      let keyword = definition.getKeywords().join(", ");
        console.log(keyword);

      if(keyword!=="-filelist" && keyword!=="-formulalist"){
        let definitionElement = <HTMLElement>this._definitionTemplate.cloneNode(true);

        this._definitionContainer.appendChild(definitionElement);
        definitionElement.querySelector(".key").appendChild(document.createTextNode(definition.getKey()));

        //Füge die Translations der Definition dem Translator hinzu
        const descriptionKey = Translator.getInstance().createDynamicKey();
        const translations = {
            "de": {},
            "de-sl": {}
        }
        translations["de"][descriptionKey] = definition.getDescription();
        translations["de-sl"][descriptionKey] = definition.getDescriptionSimpleGerman();
        Translator.getInstance().addDynamicTranslations(translations)


        const openButton = definitionElement.querySelector(".open-button");
        openButton.addEventListener("click", async () => {

            new ShowDefinitionDialog(definition).show();

        });


        //Füge bearbeiten-Button hinzu, falls User das bearbeiten darf
        const userData = UserManager.getInstance().getUserData();
        if (userData && userData.id && ((definition.user && definition.user.id === userData.id) || await UserManager.getInstance().hasAccess("admin"))) {
            const editButton = definitionElement.querySelector(".edit-button");
            editButton.classList.remove("hidden");
            editButton.addEventListener("click", async () => {
                await new AddDefinitionDialog(definition).show();
                await this.addDefinitions();
            });

            const deleteButton = definitionElement.querySelector(".delete-button");
            deleteButton.classList.remove("hidden");
            deleteButton.addEventListener("click", async () => {
                if (await new ConfirmDialog("delete definition", "delete definition title").show()) {
                    this.showLoadingSymbol();
                    await definition.delete();
                    await this.addDefinitions();
                    this.removeLoadingSymbol();
                }
            });
        } else{
          const editButton = definitionElement.querySelector(".edit-button");
          editButton.classList.add("hidden");
          const deleteButton = definitionElement.querySelector(".delete-button");
          deleteButton.classList.add("hidden");
        }
      }
    }
}

//Wenn im Footer-Fragment inkludiert wird, sorgt das für Fehler. Daher hier als globales Objekt setzen, auf
// welches das Footer-Fragment zugreifen kann
window["LexiconSite"] = LexiconSite;

App.addInitialization((app) => {
    app.addDeepLink("lexicon", LexiconSite);

});
