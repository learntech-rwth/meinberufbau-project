const view = require("../../html/sites/editStorybookSite.html");

import {Exercise} from "../../../shared/model/Exercise";
import {SaveStorybookDialog} from "../Dialog/SaveStorybookDialog";
import {EditorInfo} from "../Dialog/EditorInfo";
import {TemplateInfo} from "../Dialog/TemplateInfo";
import {App, Helper} from "cordova-sites/dist/client";
import {Course} from "../../../shared/model/Course";
import {EditExerciseSite} from "./EditExerciseSite";
import {GBEditor} from "ba-mbb-digitalstorybook/dist/editor/GBEditor";
import {FileMedium} from "cordova-sites-easy-sync/dist/shared";
import {ShowExerciseSite} from "./ShowExerciseSite";
import {MyMenuSite} from "./MyMenuSite";

export class EditStorybookSite extends MyMenuSite {
    private _exercise: Exercise;
    private _courseId: number;

    constructor(siteManager) {
        super(siteManager, view);
    }

    async onConstruct(constructParameters) {
        let res = super.onConstruct(constructParameters);

        this._exercise = null;
        this._courseId = null;
        if (Helper.isNotNull(constructParameters["course"])) {
            this._courseId = parseInt(constructParameters["course"]);
        }
        if (Helper.isNotNull(constructParameters["exercise"])) {
            this._exercise = await Exercise.findById(constructParameters["exercise"], Exercise.getRelations());
        }
        if (Helper.isNull(this._exercise)) {
            this._exercise = new Exercise();
        } else {
            //Starte die richtige Seite, wenn kein Storybook bearbeitet wird
            if (this._exercise.elementType !== "storybook") {
                //Beendigung nicht awaiten. Führt zu Deadlock in onConstruct
                this.finishAndStartSite(EditExerciseSite, {course: this._courseId, exercise: this._exercise.id})
                return res;
            }
            this._courseId = this._exercise.course.id;
        }

        return res
    }

    async onViewLoaded() {
        let res = super.onViewLoaded();

        let content = this.findBy("#editor");
        let editor = new GBEditor(content, "de");

        let imgFileMediums = {};

        this.findBy("#editorInfo").addEventListener("click", () => {
          new EditorInfo().show();
        });

        this.findBy("#templateInfo").addEventListener("click", () => {
          new TemplateInfo().show();
        });


        //Füge neuen Menüeintrag Save hinzu
        editor.menubar.menu.AddMenuItem("Save", async () => {
            //Bekomme Werte, welche nicht über den Storybook-Editor eingefügtw erden können
            let res = await new SaveStorybookDialog(
                this._exercise.getName(),
                this._courseId,
                this._exercise.getStage(),
                this._exercise.description,
                this._exercise.image
            ).show();

            //Wenn Speicher-Dialog nicht beendet wurde, fange das speichern an
            if (res) {
                this.showLoadingSymbol();
                const editorData = editor.Save(true);

                //Wandle hochgeladene Medien in FileMedium um
                const imageData = editorData.i.map(i => JSON.parse(i));
                const fileSavePromises = [];
                imageData.forEach(img => {
                    if (!img.values.url.startsWith("data:")) {
                        return;
                    }

                    let fileMedium = null;
                    if (img.values.fileMediumId) {
                        fileMedium = imgFileMediums[img.values];
                    }
                    if (!fileMedium) {
                        fileMedium = new FileMedium();
                    }
                    fileMedium.src = img.values.url;

                    //Speichere Medium auf Server
                    fileSavePromises.push(fileMedium.save().then(() => {
                        //speichere neue URL zum FileMedium und FileMedium-ID für zukünftige Bearbeitungen
                        img.values.url = fileMedium.getUrl();
                        img.attributeIds.push("fileMediumId");
                        img.attributes["fileMediumId"] = {
                            type: "number",
                            hidden: "true"
                        }
                        img.values["fileMediumId"] = fileMedium.getId();
                    }));
                })
                await Promise.all(fileSavePromises);
                editorData.i = imageData.map(i => JSON.stringify(i));

                //Setze Kurs
                if (!this._exercise.course || this._exercise.course.id !== parseInt(res["course"])) {
                    const course = await Course.findById(res["course"]);
                    this._exercise.setCourse(course);
                    if (!course.activated) {
                        course.activated = true;
                        await course.save();
                    }
                }

                //Setze allgemeine Infos
                this._exercise.setName(res["name"]);
                this._exercise.elementType = "storybook";
                this._exercise.isGenerating = false;
                this._exercise.generatingData = editorData;
                this._exercise.description = res["description"];
                this._exercise.stage = parseInt(res["stage"]);

                //Setze oder update Bild, falls dieses verändert
                let exerciseImg = this._exercise.image;
                if (res["image-changed"] === "1") {
                    if (!Helper.imageUrlIsEmpty(res["image"])) {
                        if (!exerciseImg) {
                            exerciseImg = new FileMedium();
                            this._exercise.image = exerciseImg;
                        }
                        exerciseImg.setSrc(res["image"])
                        await exerciseImg.save();
                    } else if (exerciseImg) {
                        exerciseImg.setSrc("");
                        await exerciseImg.save();
                    }
                }

                await this._exercise.save();
                this.removeLoadingSymbol();
                await this.finishAndStartSite(ShowExerciseSite, {"exerciseId": this._exercise.getId()})
            }
        });

        //not a newly created exercise
        if (this._exercise.id) {

            //Da Storybook schon existiert, müssen FileMediums wieder in Bilder umgewandelt werden
            const saveData: any = this._exercise.generatingData;
            const imgData = saveData.i.map(i => JSON.parse(i));

            const ids = imgData.map(img => img.values.fileMediumId).filter(id => Helper.isNotNull(id));
            const fileMediums = await FileMedium.findByIds(ids);
            fileMediums.forEach(f => imgFileMediums[f.id] = f);

            editor.Load(saveData);
        }

        editor.menubar.menu.AddMenuItem("Reset", editor.Reset.bind(editor));

        //Entferne Nodes, welche nicht verfügbar sein sollen
        // @ts-ignore
        let submenu = editor.menubar.menu.GetSubMenus().filter(menu => menu.id === GBEditor.SUBMENU_NODES);
        if (submenu.length >= 1) {
            const removedNodes = ["Mathe", "Test", "Relais"];

            const firstSubmenu = submenu[0];
            firstSubmenu.GetMenuItems().forEach(item => {
                if (removedNodes.indexOf(item.text) >= 0) {
                    firstSubmenu.RemoveMenuItem(item);
                }
            });
        }

        return res;
    }
}

App.addInitialization(app => {
    app.addDeepLink("editStorybook", EditStorybookSite);
})
