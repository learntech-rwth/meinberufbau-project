import {Helper} from "js-helper/dist/shared";

const view = require("../../html/sites/gbObjectsSite.html");
import {Lexicon} from "../Lexicon";
import {AddFileDialog} from "../Dialog/AddFileDialog";
import {ShowDefinitionDialog} from "../Dialog/ShowDefinitionDialog";
import {App, ConfirmDialog, Translator} from "cordova-sites/dist/client";
import {UserMenuAction} from "cordova-sites-user-management/dist/client/js/MenuAction/UserMenuAction";
import {UserManager} from "cordova-sites-user-management/dist/client";
import {MyMenuSite} from "./MyMenuSite";
import {ViewHelper} from "js-helper/dist/client/ViewHelper";
import {Definition} from "../../../shared/model/Definition";
import {FileMedium} from "cordova-sites-easy-sync/dist/shared";
import {StorybookFragment} from "../Fragments/ExerciseFragments/StorybookFragment";

/**
 * Seite zum Anzeigen der Definitionen
 */
export class GbObjectsSite extends MyMenuSite {
    private _definitionTemplate: HTMLElement;
    private _definitionContainer: HTMLElement;

    constructor(siteManager) {
        super(siteManager, view);
    }


    async onViewLoaded() {
        this._definitionTemplate = this.findBy(".definition");
        this._definitionTemplate.remove();

        this._definitionContainer = this.findBy("#definition-container");

        await this.addDefinitions();
        return super.onViewLoaded();
    }




    async addDefinitions() {
        ViewHelper.removeAllChildren(this._definitionContainer);
        const definitions = await Lexicon.getInstance().getDefinitions(true, true);

        const gbobjects = await StorybookFragment.getInstance().getImgUrls();

        await Helper.asyncForEach(gbobjects, async gbobject => {
          await this.addDefinitionElement(gbobject);
        });

    }

    private async addDefinitionElement(definition) {


        let definitionElement = <HTMLElement>this._definitionTemplate.cloneNode(true);

        this._definitionContainer.appendChild(definitionElement);
        definitionElement.querySelector(".key").appendChild(document.createTextNode(definition.tag));



        const openButton = definitionElement.querySelector(".open-button");
        openButton.addEventListener("click", async () => {

          let url = definition.url;


          console.log(url);
          window.open(url, '_blank');

        });



    }
}

//Wenn im Footer-Fragment inkludiert wird, sorgt das für Fehler. Daher hier als globales Objekt setzen, auf
// welches das Footer-Fragment zugreifen kann
window["GbObjectsSite"] = GbObjectsSite;

App.addInitialization((app) => {
    app.addDeepLink("gbobjects", GbObjectsSite);

});
