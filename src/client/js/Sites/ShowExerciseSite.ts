const view = require("../../html/sites/showExerciseSite.html");

import {ExerciseFragment} from "../Fragments/ExerciseFragment";
import {App, ConfirmDialog, MenuAction, Toast} from "cordova-sites/dist/client";
import {Helper} from "js-helper/dist/shared";
import {Exercise} from "../../../shared/model/Exercise";
import {UserSite} from "cordova-sites-user-management/dist/client";
import {ExerciseHelper} from "../Helpers/ExerciseHelper";
import {WrongAnswer} from "../../../shared/model/WrongAnswer";
import {UserManager, UserMenuAction} from "cordova-sites-user-management/dist/client";
import {EditExerciseSite} from "./EditExerciseSite";
import {User} from "cordova-sites-user-management/dist/shared/v1/model/User";
import {MyMenuSite} from "./MyMenuSite";

/**
 * Diese Seite zeigt die Exercises an. Hierbei wird pro Exercise-Typ ein Fragment eingefügt, welches dann die
 * eigentliche Anzeige übernimmt
 * Aufgrund von Altlast, gibt es ein Fragment (ExerciseFragment), welches dann die eigentlichen Fragmente für den
 * jeweiligen Exercise-Typ lädt
 */
export class ShowExerciseSite extends MyMenuSite {

    private _exerciseFragment: ExerciseFragment;
    private _exercise: Exercise;
    private _currentStartTime: Date;

    constructor(siteManager) {
        super(siteManager, view);

        this._exerciseFragment = new ExerciseFragment(this);
        this.addFragment("#show-exercise-site", this._exerciseFragment);
        this.addDelegate(new UserSite(this, "loggedIn", true));

        this._exercise = null;
    }

    async onConstruct(constructParameters) {
        let res = super.onConstruct(constructParameters);

        if (Helper.isNotNull(constructParameters) && constructParameters["exerciseId"]) {
            this._exercise = await Exercise.findById(constructParameters["exerciseId"]);
        }

        if (Helper.isNull(this._exercise)) {
            console.warn("no exercise found! ", constructParameters);
            res.then(() => {
                this.finish();
            });
            return res;
        }

        //Setze Exercise am Fragment
        await this._exerciseFragment.setExercise(this._exercise);

        return res;
    }

    onCreateMenu(navbar) {
        super.onCreateMenu(navbar);
        navbar.addAction(new MenuAction("reset", async () => {
            try {
                this.showLoadingSymbol();

                //Speichere aktuellen Fortschritt und lege einen neuen an. Bei der Anzeige
                // wird immer der neuste Fortschritt geladen - oder ein neuer angelegt, falls der letzte
                // beendet wurde
                await this._saveExercise();

                let newP = await ExerciseHelper.createNextProgressFor(this._exercise);
                await newP.save();

                let exerciseId = this._exercise.getId();
                this._exercise = null;

                await this.finishAndStartSite(ShowExerciseSite, {"exerciseId": exerciseId})
            } catch (e) {
                console.error(e);
            }
        }));
        navbar.addAction(new UserMenuAction("edit", "admin", async () => {
            this.finishAndStartSite(EditExerciseSite, {"exercise": this._exercise.id});
        }));
        navbar.addAction(new UserMenuAction("remove", "admin", async () => {
            if (await new ConfirmDialog("do you want to delete exercise", "delete exercise").show()) {
                this._exercise.delete();
                new Toast("exercise successfully deleted").show();
                await this.finish();
            }
        }));
    }

    async _saveExercise(progress?) {
        if (Helper.isNull(progress) && Helper.isNotNull(this._exercise)) {
            //Lade oder erstelle einen neuen Fortschritt, falls keiner übergeben
            progress = await ExerciseHelper.getProgressFor(this._exercise);
        }

        if (Helper.isNotNull(progress)) {
            if (!progress.isDone) {
                //Update die gebrauchte Zeit
                if (Helper.isNotNull(this._currentStartTime)) {
                    let currentTimeNeeded = new Date().getTime() - this._currentStartTime.getTime();
                    progress.timeNeeded += currentTimeNeeded;
                    this._currentStartTime = new Date();
                }
                await progress.save();
            }

            //wrongAnswers werden erst in dieser Eigenschaft zwischengespeichert und hier dann in die Datenbank geschrieben
            if (progress._wrongAnswers) {
                await WrongAnswer.saveMany(progress._wrongAnswers);
            }

            return progress;
        }

        return null;
    }

    async isDone(state) {

        //Update Fortschritt und beende den Durchlauf
        let progress = await this._saveExercise();
        if (Helper.isNotNull(progress)) {
            if (Helper.isNotNull(state)) {
                progress.state = state;
            }
            progress.isDone = true;

            //Der Fortschritt ist abhängig vom User
            let user = new User();
            user.id = UserManager.getInstance().getUserData().id;
            progress.user = user;

            let saveProgressPromise = progress.save(false);
            let wrongAnswers = await WrongAnswer.find({exerciseProgress: {clientId: progress.clientId}});

            //Der Progress existiert erst beim Server, wenn dieser auch tatsächlich gespeichert ist.
            // Die Wrong Answers können erst jetzt der Server-ID zugeordnet werden, also hier noch einmal erneut speichern
            await saveProgressPromise;
            wrongAnswers.forEach(wrongAnswer => {
                wrongAnswer.exerciseProgress = progress;
                wrongAnswer.user = user;
            });

            // @ts-ignore
            await WrongAnswer.saveMany(wrongAnswers, false);
        }
    }

    async onStart(pauseArguments) {
        await super.onStart(pauseArguments);
        if (this._exercise) {
            this._currentStartTime = new Date();
        }
    }

    async onPause() {
        let res = super.onPause();

        //Speichere Exercise und update damit auch die genutzte Zeit
        await this._saveExercise();
        return res;
    }

    onBackPressed() {
        //Frage ob Aufgabe wirklich beendet werden soll
        new ConfirmDialog("abort exercise", "abort-exercise-title", "yes", "no").show().then(shouldClose => {
            if (shouldClose) {
                this.finish();
            }
        });

        //Durch Rückgabe von False wird sichergestellt, dass sich um das Back-Event gekümmert wurde => keine Default-Action durchführen
        return false;
    }
}
App.addInitialization((app) => {
    app.addDeepLink("exercise", ShowExerciseSite);
});
