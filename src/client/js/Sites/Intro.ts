import {MyMenuSite} from "./MyMenuSite";
import {StartSite} from "./StartSite";

const view = require("../../html/sites/intro.html");

/**
 * Seite zum Anzeigen des Intros
 */
export class Intro extends MyMenuSite {


    constructor(siteManager: any) {
        super(siteManager, view);

        document.body.style.background = "#00549F";

    }


    introPage = 1;
    private startX = 0;

    async onViewLoaded() {
        let res = super.onViewLoaded();

        this.introPage = 1;
        console.log("hello:" + this.introPage);

        this.findBy("#closeIntro").addEventListener("click", () => {
          this.startSite(StartSite, null);
          document.body.style.background = "#FFFFFF";
        });

        this.findBy("#skipIntro").addEventListener("click", () => {
          this.startSite(StartSite, null);
          document.body.style.background = "#FFFFFF";
        });

        this.findBy("#div0").addEventListener("click", () => {
            console.log(this.introPage);
            if(this.introPage == 1){
              this.findBy("#div1").style.display = "none";
              this.findBy("#div2").style.display = "inline";
              this.findBy("#div3").style.display = "none";
              this.findBy("#div4").style.display = "none";
              this.findBy("#div5").style.display = "none";
              this.introPage++;
            } else {
              if(this.introPage == 2){
                this.findBy("#div1").style.display = "none";
                this.findBy("#div2").style.display = "none";
                this.findBy("#div3").style.display = "inline";
                this.findBy("#div4").style.display = "none";
                this.findBy("#div5").style.display = "none";
                this.introPage++;
              } else {
                if(this.introPage == 3){
                  this.findBy("#div1").style.display = "none";
                  this.findBy("#div2").style.display = "none";
                  this.findBy("#div3").style.display = "none";
                  this.findBy("#div4").style.display = "inline";
                  this.findBy("#div5").style.display = "none";
                  this.introPage++;
                } else {
                  if(this.introPage == 4){
                    this.findBy("#div1").style.display = "none";
                    this.findBy("#div2").style.display = "none";
                    this.findBy("#div3").style.display = "none";
                    this.findBy("#div4").style.display = "none";
                    this.findBy("#div5").style.display = "inline";
                    this.findBy('#changeIntroPage').style.display = "none";
                    this.introPage++;
                  }
                }
              }
            }
        });


        this.findBy("#div1").style.display = "inline";
        this.findBy("#div2").style.display = "none";
        this.findBy("#div3").style.display = "none";
        this.findBy("#div4").style.display = "none";
        this.findBy("#div5").style.display = "none";

        this.findBy("#navbar-fragment").style.display = "none";
        this.findBy("#footer-fragment").style.display = "none";

        return res;
    }


}
