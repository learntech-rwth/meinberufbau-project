import {MyMenuSite} from "./MyMenuSite";
import {LinkSetFragment} from "../Fragments/LinkSetFragment";
import {Course} from "../../../shared/model/Course";
import {ListExerciseSite} from "./ListExerciseSite";
import {CourseHelper} from "../Helpers/CourseHelper";

const view = require("../../html/sites/tiefbauSite.html");

/**
 * Seite zum Anzeigen der Tiefbau-Kurse
 */
export class TiefbauSite extends MyMenuSite {

    private linkFragment: LinkSetFragment;

    constructor(siteManager: any) {
        super(siteManager, view);
        this.linkFragment = new LinkSetFragment(this);
        this.addFragment("#link-fragment", this.linkFragment);
    }

    async onConstruct(constructParameters: any): Promise<any[]> {
        const res = super.onConstruct(constructParameters);
        const courses = await Course.findByIds([6, 4, 3, 8]);

        //Die IDs werden der ID nach geladen. Daher einmal hier richtig einsortieren
        courses.sort((a, b) => {
            if (a.id === 8){
                return 1;
            }
            else if (b.id === 8){
                return -1;
            }
            else {
                return (b.id - a.id)
            }
        })

        this.linkFragment.setLinks(courses.map(c => {
            return {
                name: c.name,
                image: CourseHelper.editSource(c.icon),
                site: ListExerciseSite,
                constructParameters: {courseId: c.id}
            }
        }));

        return res;
    }
}
