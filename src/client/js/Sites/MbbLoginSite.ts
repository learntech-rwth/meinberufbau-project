import {LoginSite} from "cordova-sites-user-management/dist/client/js/Site/LoginSite";
import {MbbRegistrationSite} from "./MbbRegistrationSite";
import {App} from "cordova-sites/dist/client";

const view = require("../../html/sites/mbbLoginSite.html");
const navbarLogo  = require("../../img/navbar_logo.svg");

export class MbbLoginSite extends LoginSite {

    constructor(siteManager) {
        super(siteManager, view);
        this.getNavbarFragment().setLogo(navbarLogo);
    }

    async onViewLoaded() {
        const res = super.onViewLoaded();
        this.findBy("#register-button").addEventListener("click", () => this.finishAndStartSite(MbbRegistrationSite));

        return res;
    }

    onStart(args?){
        const res = super.onStart(args);
        console.log("on start - login");
        return res;
    }
}

App.addInitialization(app => {
    app.addDeepLink("login", MbbLoginSite);
});
