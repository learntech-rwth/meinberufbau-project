import {MyMenuSite} from "./MyMenuSite";
import {LinkSetFragment} from "../Fragments/LinkSetFragment";
import {HochbauSite} from "./HochbauSite";
import {AusbauSite} from "./AusbauSite";
import {TiefbauSite} from "./TiefbauSite";

const view = require("../../html/sites/startSite.html");
const imgHochbau = <string>require("../../img/courses/hochbau.png")
const imgAusbau = <string>require("../../img/courses/ausbau.png")
const imgTiefbau = <string>require("../../img/courses/tiefbau.png")

/**
 * Seite zum Anzeigen der Ausbau, Hochbau und Tiefbau-Seiten
 */
export class StartSite extends MyMenuSite {

    constructor(siteManager: any) {
        super(siteManager, view);
        const linkFragment = new LinkSetFragment(this);
        this.addFragment("#link-fragment", linkFragment);
        linkFragment.setLinks([
            {
                name: "Hochbau",
                image: imgHochbau,
                site: HochbauSite,
                constructParameters: {}
            },
            {
                name: "Ausbau",
                image: imgAusbau,
                site: AusbauSite,
                constructParameters: {}
            },
            {
                name: "Tiefbau",
                image: imgTiefbau,
                site: TiefbauSite,
                constructParameters: {}
            }
        ]);
    }
}
