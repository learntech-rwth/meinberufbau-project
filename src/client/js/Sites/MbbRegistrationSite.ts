import { RegistrationSite } from "cordova-sites-user-management/dist/client/js/Site/RegistrationSite";
import {App} from "cordova-sites";
const view = require("../../html/sites/mbbRegistraionSite.html");
const navbarLogo  = require("../../img/navbar_logo.svg");

export class MbbRegistrationSite extends RegistrationSite{
    constructor(siteManager) {
        super(siteManager, view);
        this.getNavbarFragment().setLogo(navbarLogo);
    }
}
App.addInitialization(app => {
    app.addDeepLink("registration", MbbRegistrationSite);
});
