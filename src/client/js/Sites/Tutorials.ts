import {MyMenuSite} from "./MyMenuSite";
import {App} from "cordova-sites/dist/client";

const view = require("../../html/sites/tutorials.html");

/**
 * Seite zum Anzeigen der Definitionen
 */
export class Tutorials extends MyMenuSite {

  constructor(siteManager: any) {
      super(siteManager, view);
  }
}

//Wenn im Footer-Fragment inkludiert wird, sorgt das für Fehler. Daher hier als globales Objekt setzen, auf
// welches das Footer-Fragment zugreifen kann
window["Tutorials"] = Tutorials;

App.addInitialization((app) => {
    app.addDeepLink("tutorials", Tutorials);

});
