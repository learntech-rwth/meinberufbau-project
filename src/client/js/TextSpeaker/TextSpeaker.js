import {App, Helper, MenuAction, NativeStoragePromise, Submenu, Translator} from "cordova-sites/dist/client";
import ttsImg from "../../img/speaker.svg";
import {ViewHelper} from "js-helper/dist/client/ViewHelper";

export class TextSpeaker {

    constructor() {
        this.isBrowser = (device.platform.toLowerCase() === "browser");
        this._speakingElement = null;
        this._speakingRate = 1;
        NativeStoragePromise.getItem(TextSpeaker.NATIVE_STORAGE_SPEAKING_RATE_KEY, 1).then(rate => {
            if (this._speakingRate === 1) {
                this._speakingRate = rate;
            }
        });
        this._submenu = null;
        this.createChangeRateMenuAction();
    }

    static getLocale() {
        let locale = Translator.getInstance().getCurrentLanguage();
        locale += (locale === "de") ? "-DE" : "-US";
        return locale;
    }

    static prepareText(message) {
        const replacers = [
            {
                search: /≈/g,
                replace: " ungefähr ",
            },
            {
                search: /&asymp;/g,
                replace: " ungefähr ",
            },
            {
                search: /÷/g,
                replace: " geteilt durch ",
            },
            {
                search: /&div;/g,
                replace: " geteilt durch ",
            },
        ]
        try {
            replacers.forEach(r => message = message.replace(r.search, r.replace));
        } catch (e) {
            console.error(e);
        }
        return message;
    }

    async speak(message, options) {
        options = Helper.nonNull(options, {});
        options["rate"] = Helper.nonNull(options["rate"], this._speakingRate);
        options["locale"] = Helper.nonNull(options["locale"], "de-DE");
        options["volume"] = Helper.nonNull(options["volume"], 1);
        options["pitch"] = Helper.nonNull(options["pitch"], 1);

        message = TextSpeaker.prepareText(message);

        if (this.isBrowser) {
            if ("speechSynthesis" in window) {
                let msg = new SpeechSynthesisUtterance(message);
                msg.rate = options["rate"]; // 0.1 to 10
                msg.lang = options["locale"];
                msg.volume = options["volume"];
                msg.pitch = options["pitch"];
                msg.onstart = options["onstart"];

                return new Promise((resolve, reject) => {
                    msg.onend = resolve;
                    msg.onerror = reject;
                    window.speechSynthesis.cancel();
                    window.speechSynthesis.speak(msg);
                });
            } else {
                return Promise.reject("no speechSynthesis api");
            }
        } else {
            options["text"] = message;
            return new Promise((resolve, reject) => {
                TTS.speak(options, resolve, reject);
                if (typeof options["onstart"] === "function") {
                    options["onstart"]();
                }
            });
        }
    }

    static getInstance() {
        if (Helper.isNull(TextSpeaker._instance)) {
            TextSpeaker._instance = new TextSpeaker();
        }
        return TextSpeaker._instance;
    }

    addSpeakers(element) {
        element.querySelectorAll(".textToSpeech").forEach(element => {
            if (!element.querySelector(".ttsButton")) {
                let newChild = document.createElement("span");
                newChild.classList.add("tts-text");
                ViewHelper.moveChildren(element, newChild);
                element.appendChild(newChild);

                let ttsButton = document.createElement("img");
                ttsButton.src = ttsImg;
                ttsButton.classList.add("ttsButton");
                ttsButton.addEventListener("click", async (e) => {
                    try {
                        let locale = "de-DE";
                        if (element.classList.contains("translation")) {
                            locale = TextSpeaker.getLocale();
                        }
                        e.preventDefault();
                        e.stopPropagation();

                        if (this._speakingElement) {
                            this._speakingElement.classList.remove("speaking");
                        }

                        this._speakingElement = element;
                        element.classList.add("speaking");
                        await this.speak(newChild.innerText, {locale: locale}).catch(e => console.error(e));
                        element.classList.remove("speaking");

                    } catch (e) {
                        console.error(e);
                    }
                });
                // ttsButton.innerText = "sound";
                element.insertBefore(ttsButton, newChild);
                element.classList.add("hasTextToSpeechButton");
            }
        });
    }

    setSpeakingRate(speakingRate) {
        this._speakingRate = speakingRate;
        NativeStoragePromise.setItem(TextSpeaker.NATIVE_STORAGE_SPEAKING_RATE_KEY, speakingRate);

        if (this._submenu) {
            this._submenu.getActions().forEach(action => {
                const value = parseFloat(action.getName().replace(",", "."));
                if (value === speakingRate) {
                    action.setLiClass("selected");
                } else {
                    action.setLiClass("");
                }
                action.update();
            });

        }
    }

    createChangeRateMenuAction() {
        const submenu = new Submenu("speaker", MenuAction.SHOW_NEVER, 1002);
        const speeds = ["0,5", "0,75", "1,0", "1,5", "2,0"]

        speeds.forEach(speed => {
            const value = parseFloat(speed.replace(",", "."));
            const menuAction = new MenuAction(speed, () => {
                TextSpeaker.getInstance().setSpeakingRate(value);
            }, MenuAction.SHOW_NEVER);

            if (value === this._speakingRate) {
                menuAction.setLiClass("selected");
            }
            menuAction.setShouldTranslate(false);
            submenu.addAction(menuAction);
        });

        this._submenu = submenu;
        return this._submenu;
    }

    getSpeakingRateSubmenu() {
        if (!this._submenu) {
            this.createChangeRateMenuAction();
        }
        return this._submenu;
    }

    getSpeakingRate(){
        return this._speakingRate;
    }
}

TextSpeaker.NATIVE_STORAGE_SPEAKING_RATE_KEY = "text-speaker-rate";

App.addInitialization((app) => {
    app.ready(() => {
        Translator.addTranslationCallback((baseElement) => {
            if (typeof baseElement !== "undefined") {
                TextSpeaker.getInstance().addSpeakers(baseElement);
            }
        })
    })
});

TextSpeaker._instance = null;
