import {Definition} from "../../shared/model/Definition";
import {ShowDefinitionDialog} from "./Dialog/ShowDefinitionDialog";
import {App, Translator} from "cordova-sites/dist/client";
import {Helper} from "js-helper/dist/shared";
import {UserManager} from "cordova-sites-user-management/dist/client";
import {User} from "cordova-sites-user-management/dist/shared";
import {ViewHelper} from "js-helper/dist/client/ViewHelper";

export class Lexicon {
    private _definitions: { [id: number]: Definition };
    private _definitionKeys: { [key: string]: number };
    private keywords: {[key: string]: number};
    private skeywords: {[key: string]: number};
    private sortedKeywords: string[];

    private static _instance: Lexicon;

    constructor() {
        this._definitions = null;
        this._definitionKeys = {};
        this.keywords = {};
        this.skeywords = {};
        this.sortedKeywords = [];
    }

    updateDefinitions(element) {
        if (element.children.length === 0) {
            let preparedElement = this.prepareText(element.innerHTML);
            ViewHelper.removeAllChildren(element);
            ViewHelper.moveChildren(preparedElement, element);
        } else {
            Array.from(element.children).forEach(child => this.updateDefinitions(child));
        }
        return element;
    }

    async _loadDefinitions(reload?) {
        if (Helper.isNull(this._definitions) || reload) {
            try {
                Definition.getRelations();

                let definitions = await Definition.find(undefined, {"key": "asc"}, undefined, undefined, Definition.getRelations());
                // let definitions = await Definition.find(undefined, {"key": "asc"}, undefined, undefined);

                this._definitions = {};
                this._definitionKeys = {};
                this.keywords = {};
                this.skeywords = {};
                definitions.forEach(definition => {
                    this._definitions[definition.getId()] = definition;
                    this._definitionKeys[definition.getKey()] = definition.getId();
                    this.keywords[definition.getKey()] = definition.getId();
                    definition.getKeywords().forEach(word => {
                        this.keywords[word] = definition.getId();
                    });
                    if(definition.getKeywords().join(", ") !== "-filelist" && definition.getKeywords().join(", ") !== "-formulalist"){
                      this.skeywords[definition.getKey()] = definition.getId();
                      definition.getKeywords().forEach(word => {
                          this.skeywords[word] = definition.getId();
                      });
                    }

                });
                this.sortedKeywords = Object.keys(this.skeywords).sort((a, b) => b.length-a.length);

            } catch (e) {
                debugger;
                console.error(e);
            }
        }
    }

    async getDefinitions(sorted, reload) {
        await this._loadDefinitions(reload);
        let result = Helper.toArray(this._definitions);
        if (Helper.nonNull(sorted, true)) {
            result.sort((a, b) => {
                return (a.getKey().toLowerCase() < b.getKey().toLowerCase()) ? -1 : 1;
            });
        }
        return result;
    }

    async addOrUpdateDefinition(definition, forMeOnly) {
        forMeOnly = Helper.nonNull(forMeOnly, true);

        if (definition instanceof Definition) {
            await this._loadDefinitions();
            if (Helper.isNull(this._definitions[this._definitionKeys[definition.getKey()]]) ||
                (Helper.isNotNull(definition.getId()) && this._definitions[definition.getId()].getId() === definition.getId())) {
                if (forMeOnly) {
                    let user = new User();
                    user.id = UserManager.getInstance().getUserData().id;
                    definition.user = user;
                }

                await definition.save();
                this._definitions[definition.getId()] = definition;
                this._definitionKeys[definition.getKey()] = definition.getId();
            }
        }
    }

    prepareText(text, ignoredKey?) {
        if (Helper.isNull(text) || text.trim() === "") {
            return document.createTextNode(text);
        }
        console.log(this.sortedKeywords);
        this.sortedKeywords.forEach(word => {
            const definition = this._definitions[this.keywords[word]];
            if (definition.getKey() !== ignoredKey) {
                let regexs = definition.getRegexs();
                try {
                    regexs.forEach(regex => {
                      //Füge Link nur dann ein, wenn noch kein anderer Link vorhanden ist.
                      if(!text.includes("definition-link")){

                        text = text.replace(regex, "<span class = 'definition-link' data-definition-id='" + definition.getId() + "' >$1</span>");
                      } //else {
                        //let inner=text.substring(text.indexOf(">")+1,text.lastIndexOf("<"));
                        //console.log(inner);
                        //let myregex:String = regex.toString();
                        //myregex = myregex.substring(myregex.indexOf("(")+1, myregex.indexOf(")"));
                        //console.log(myregex);
                        //console.log("====");
                        //if(inner.length < myregex.length){
                          //console.log(text);
                          //text = text.replace(regex, "<span class = 'definition-link' data-definition-id='" + definition.getId() + "' >$1</span>");
                          //console.log(text);
                        //}
                      //}
                    });
                } catch (e) {
                    console.error("Definition regex error", regexs, e);
                }

            }
        });
        let span = document.createElement("span");
        span.innerHTML = text;

        span.querySelectorAll("span.definition-link").forEach(definitionElement => {
            definitionElement.addEventListener("click", (e) => {
                if (Helper.isNotNull(this._definitions[(definitionElement as HTMLElement).dataset["definitionId"]])) {
                    new ShowDefinitionDialog(this._definitions[(definitionElement as HTMLElement).dataset["definitionId"]]).show();
                    e.preventDefault();
                    e.stopPropagation();
                }
            });
        });

        return span;
    }

    /**
     *
     * @return {Lexicon}
     */
    static getInstance() {
        if (Helper.isNull(this._instance)) {
            this._instance = new Lexicon();
        }
        return this._instance;
    };
}

App.addInitialization((app) => {
    app.ready(() => {
        Translator.addTranslationCallback(async (baseElement) => {
            if (typeof baseElement !== "undefined") {
                await Lexicon.getInstance()._loadDefinitions();
                Lexicon.getInstance().updateDefinitions(baseElement);
            }
        })
    });
});
