import {MBBModel} from "./MBBModel";
import {BaseDatabase} from "cordova-sites-database/dist/cordova-sites-database";

export class WikiEntry extends MBBModel {

    name: string;
    description: string;

    constructor() {
        super();
        this.name = null;
        this.description = null;
    }

    static getColumnDefinitions() {
        let columns = super.getColumnDefinitions();
        columns["name"] = BaseDatabase.TYPES.STRING;
        columns["description"] ={type:  BaseDatabase.TYPES.MEDIUMTEXT, escapeHTML: false};
        return columns;
    }
}

BaseDatabase.addModel(WikiEntry);
