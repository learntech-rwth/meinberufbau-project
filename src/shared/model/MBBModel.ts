import {AccessEasySyncModel} from "cordova-sites-user-management/dist/shared";

export class MBBModel extends AccessEasySyncModel{

    static select(where, order, limit, offset, relations){
        return this.find(where, order, limit, offset, relations);
    }

    static selectOne(where, order, offset, relations){
        return this.findOne(where, order, offset, relations);
    }
}
AccessEasySyncModel.ACCESS_MODIFY="admin";