import {MBBModel} from "./MBBModel";
import {BaseDatabase} from "cordova-sites-database/dist/cordova-sites-database";
import {Course} from "./Course";
import {FileMedium} from "cordova-sites-easy-sync/dist/shared";

export class Exercise extends MBBModel {

    public static readonly STAGES = {ONE: 1, TWO: 2, THREE: 3, OTHERS: 4}

    generatingData: Object;
    course: Course;
    progress: number;
    name: string;
    isGenerating: boolean;
    elementType: string;
    stage: number;
    description: string;
    image: FileMedium

    constructor() {
        super();

        this.stage = Exercise.STAGES.ONE;
        this.generatingData = {};
        this.course = null;
        this.progress = null;
        this.name = null;
        this.isGenerating = false;
        this.description = null;
        this.image = null;

        this.elementType = null;
    }

    getStage() {
        return this.stage;
    }

    setStage(stage: number) {
        this.stage = stage;
    }

    setCourse(course) {
        this.course = course;
    }

    setGeneratingData(generatingData) {
        this.generatingData = generatingData;
    }

    setElementType(type) {
        this.elementType = type;
    }

    getElementType() {
        return this.elementType;
    }

    setName(name) {
        this.name = name;
    }

    getName() {
        return this.name;
    }

    static getColumnDefinitions() {
        let columns = super.getColumnDefinitions();
        columns["elementType"] = {type: BaseDatabase.TYPES.STRING};
        columns["generatingData"] = {
            type: BaseDatabase.TYPES.MY_JSON,
            nullable: true
        };
        columns["stage"] = {
            type: BaseDatabase.TYPES.INTEGER,
            default: 1
        };
        columns["name"] = BaseDatabase.TYPES.STRING;
        columns["isGenerating"] = {
            type: BaseDatabase.TYPES.BOOLEAN,
            default: false
        };
        columns["description"] = BaseDatabase.TYPES.TEXT;
        return columns;
    }

    static getRelationDefinitions() {
        let relations = super.getRelationDefinitions();
        relations["course"] = {
            target: Course.getSchemaName(),
            type: "many-to-one",
            joinColumn: true,
            inverseSide: "exercises"
        };
        relations["image"] = {
            target: FileMedium.getSchemaName(),
            type: "one-to-one",
            joinColumn: true,
            nullable: true,
            sync: true
        };
        return relations;
    }
}

BaseDatabase.addModel(Exercise);
