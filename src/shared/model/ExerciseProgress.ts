import {BaseDatabase} from "cordova-sites-database/dist/cordova-sites-database";
import {Exercise} from "./Exercise";
import {AccessEasySyncModel, UserSyncPartialModel} from "cordova-sites-user-management/dist/shared";

export class ExerciseProgress extends UserSyncPartialModel {

    element;
    elementId: number;
    timeNeeded;
    state;
    data;
    _savePromise: Promise<any>;
    runNumber;
    isDone: boolean;

    constructor() {
        super();
        this.element = null;
        this.elementId = null;
        this.timeNeeded = 0;
        this.state = {};
        this.data = null;
        this._savePromise = Promise.resolve(this);
        this.runNumber = 0;
        this.isDone = false;
    }

    setState(state){
        this.state = state;
    }

    getState(){
        return this.state;
    }

    getData(){
        return this.data;
    }

    setData(data){
        this.data = data;
    }

    static getColumnDefinitions() {
        let columns = super.getColumnDefinitions();
        columns["state"] = BaseDatabase.TYPES.MY_JSON;
        columns["timeNeeded"] = BaseDatabase.TYPES.INTEGER;
        columns["runNumber"] = BaseDatabase.TYPES.INTEGER;
        columns["isDone"] = BaseDatabase.TYPES.BOOLEAN;
        // columns["elementId"] = {type:BaseDatabase.TYPES.INTEGER, nullable: true};
        columns["data"] = {
            type: BaseDatabase.TYPES.MY_JSON,
            nullable: true
        };
        return columns;
    }

    static getRelationDefinitions(){
        let relations = super.getRelationDefinitions();
        relations["element"] = {
            target: Exercise.getSchemaName(),
            type: "many-to-one",
            joinColumn: true,
            inverseSide: "progress",
            // cascade: true,
        };
        return relations;
    }

    async save(): Promise<any> {
        this._savePromise = (super.save).call(this, ...arguments);
        return this._savePromise;
    }

}
ExerciseProgress.NEED_USER = false;
AccessEasySyncModel.ACCESS_MODIFY="loggedIn";

BaseDatabase.addModel(ExerciseProgress);