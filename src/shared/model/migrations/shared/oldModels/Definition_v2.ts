import {Helper} from "js-helper/dist/shared";
import {BaseDatabase} from "cordova-sites-database/dist/cordova-sites-database";
import {UserSyncModel} from "cordova-sites-user-management/dist/shared";
import {FileMedium} from "cordova-sites-easy-sync/dist/shared";

export class Definition extends UserSyncModel {

    static SAVE_PATH;

    keywords: [];
    key: string;
    description: string;
    descriptionSimpleGerman: string
    image: FileMedium;
    imageDescription: string;
    _regexs;

    constructor() {
        super();
        this.keywords = [];
        this.key = null;
        this.description = null;
        this.descriptionSimpleGerman = null;
        this.image = null;
        this.imageDescription = null;

        this._regexs = {};
    }

    setKeywords(keywords) {
        this.keywords = keywords;
    }

    getKeywords() {
        return this.keywords;
    }

    setDescription(description) {
        this.description = description;
    }

    getDescription() {
        return this.description;
    }

    setKey(key) {
        this.key = key;
    }

    getKey() {
        return this.key;
    }

    setImage(image) {
        this.image = image;
    }

    getImage(): FileMedium {
        return this.image;
    }

    setImageDescription(imageDescription) {
        this.imageDescription = imageDescription;
    }

    getImageDescription() {
        return this.imageDescription;
    }

    getRegexs() {
        let keywords = Helper.nonNull(Helper.cloneJson(this.keywords), []);
        keywords.push(this.key);
        keywords.forEach(keyword => {
            if (keyword && keyword.trim() !== "" && Helper.isNull(this._regexs[keyword])) {
                // this._regexs[keyword] = new RegExp("^(([^<]*)|([^<]*<[^>]*>)*)\\b("+Helper.escapeRegExp(keyword)+")\\b", "gi");
                this._regexs[keyword] = new RegExp("(" + Helper.escapeRegExp(keyword) + ")", "gi");
            }
        });
        return Helper.toArray(this._regexs);
    }

    static getColumnDefinitions() {
        let columns = super.getColumnDefinitions();
        columns["key"] = {type: BaseDatabase.TYPES.STRING, unique: true};
        columns["keywords"] = BaseDatabase.TYPES.SIMPLE_JSON;
        columns["description"] = BaseDatabase.TYPES.STRING;
        columns["descriptionSimpleGerman"] = {type: BaseDatabase.TYPES.STRING, nullable: true};
        // columns["image"] = {type: BaseDatabase.TYPES.MEDIUMTEXT, nullable: true};
        columns["imageDescription"] = {type: BaseDatabase.TYPES.STRING, nullable: true};
        return columns;
    }

    static getRelationDefinitions() {
        let relations = super.getRelationDefinitions();
        relations["image"] = {
            target: FileMedium.getSchemaName(),
            type: "one-to-one",
            cascade: true,
            nullable: true,
            joinColumn: true,
        }
        return relations;
    }
}

Definition.ACCESS_MODIFY = false;
Definition.SAVE_PATH = "/definition";