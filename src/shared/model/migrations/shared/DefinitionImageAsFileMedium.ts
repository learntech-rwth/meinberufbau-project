import {MigrationInterface, QueryRunner} from "typeorm";
import {Helper, MigrationHelper} from "js-helper";
import {Definition} from "./oldModels/Definition_v1";
import {FileMedium} from "cordova-sites-easy-sync/dist/shared";

export class DefinitionImageAsFileMedium1000000013000 implements MigrationInterface {
    down(queryRunner: QueryRunner): Promise<any> {
        return Promise.resolve(undefined);
    }

    async up(queryRunner: QueryRunner): Promise<any> {
        let definitionImages: any = null;
        if (MigrationHelper.isServer()) {
            definitionImages = await queryRunner.query("SELECT id, image FROM definition WHERE image IS NOT NULL");
            await Helper.asyncForEach(definitionImages, async def => {
                def.res = await queryRunner.manager.save(FileMedium.getSchemaName(), {
                    createdAt: new Date(),
                    updatedAt: new Date(),
                    version: 1,
                    deleted: 0,
                    saveOffline: 1,
                    src: def.image
                });
            }, true);
        }

        await MigrationHelper.updateModel(queryRunner, Definition)

        if (MigrationHelper.isServer()){
            await Helper.asyncForEach(definitionImages, async def => {
                await queryRunner.query("UPDATE definition SET imageId = "+def.res.id+" WHERE id = "+def.id);
            }, true)
            await queryRunner.query("UPDATE definition SET updatedAt = now()");
        }
    }
}