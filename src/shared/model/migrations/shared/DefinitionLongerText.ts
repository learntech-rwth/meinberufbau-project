import {MigrationInterface, QueryRunner} from "typeorm";
import {MigrationHelper} from "js-helper";
import {Definition} from "./oldModels/Definition_v3";

export class DefinitionLongerText1000000015000 implements MigrationInterface {
    down(queryRunner: QueryRunner): Promise<any> {
        return Promise.resolve(undefined);
    }

    async up(queryRunner: QueryRunner): Promise<any> {
        await MigrationHelper.updateModel(queryRunner, Definition)
    }
}