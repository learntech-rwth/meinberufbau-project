import {MigrationInterface, QueryRunner, TableColumn, TableForeignKey} from "typeorm";
import {BaseDatabase} from "cordova-sites-database/dist/BaseDatabase";

export class AddImageAndDescription1000000018000 implements MigrationInterface {

    async up(queryRunner: QueryRunner): Promise<any> {
        await this.addImageAndDescription(queryRunner);
    }

    async addImageAndDescription(queryRunner: QueryRunner) {
        await queryRunner.addColumn("exercise", new TableColumn({
            "name":"description",
            "type": BaseDatabase.TYPES.TEXT,
            "isNullable": true,
        }));
        await queryRunner.addColumn("exercise", new TableColumn({
            "name":"imageId",
            "type": BaseDatabase.TYPES.INTEGER,
            "isNullable": true,
        }));
        await queryRunner.createForeignKey("exercise", new TableForeignKey({
            columnNames: ["imageId"],
            referencedColumnNames: ["id"],
            referencedTableName: "file_medium"
        }))
    }

    down(queryRunner: QueryRunner): Promise<any> {
        return undefined;
    }
}
