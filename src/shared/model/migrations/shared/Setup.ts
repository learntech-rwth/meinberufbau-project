import {MigrationInterface, QueryRunner, Table} from "typeorm";
import {BaseDatabase} from "cordova-sites-database/dist/BaseDatabase";

export class Setup1000000002000 implements MigrationInterface {

    _isServer(): boolean {
        return (typeof document !== "object")
    }

    async up(queryRunner: QueryRunner): Promise<any> {
        await this._addCourse(queryRunner);
        await this._addDefinition(queryRunner);
        await this._addExercise(queryRunner);
        await this._addExerciseProgress(queryRunner);
        await this._addWrongAnswer(queryRunner);
    }

    async _addCourse(queryRunner: QueryRunner) {
        let courseTable = new Table({
            name: "course",
            columns: [
                {
                    name: "id",
                    isPrimary: true,
                    type: BaseDatabase.TYPES.INTEGER,
                    isGenerated: this._isServer(),
                    generationStrategy: "increment" as "increment"
                    // isGenerated: this._isServer(),
                },
                {
                    name: "createdAt",
                    type: BaseDatabase.TYPES.DATE,
                },
                {
                    name: "updatedAt",
                    type: BaseDatabase.TYPES.DATE,
                },
                {
                    name: "version",
                    type: BaseDatabase.TYPES.INTEGER,
                },
                {
                    name: "deleted",
                    type: BaseDatabase.TYPES.BOOLEAN,
                },
                {
                    name: "name",
                    type: BaseDatabase.TYPES.STRING,
                },
                {
                    name: "activated",
                    type: BaseDatabase.TYPES.BOOLEAN
                },
                {
                    name: "progress",
                    type: BaseDatabase.TYPES.FLOAT
                },
                {
                    name: "icon",
                    type: (this._isServer() ? BaseDatabase.TYPES.MEDIUMTEXT : BaseDatabase.TYPES.TEXT)
                }
            ]
        });
        return await queryRunner.createTable(courseTable, true)
    }

    async _addDefinition(queryRunner: QueryRunner) {
        let definition = new Table({
            name: "definition",
            columns: [
                {
                    name: "id",
                    isPrimary: true,
                    type: BaseDatabase.TYPES.INTEGER,
                    isGenerated: this._isServer(),
                    generationStrategy: "increment" as "increment"
                },
                {
                    name: "createdAt",
                    type: BaseDatabase.TYPES.DATE,
                },
                {
                    name: "updatedAt",
                    type: BaseDatabase.TYPES.DATE,
                },
                {
                    name: "version",
                    type: BaseDatabase.TYPES.INTEGER,
                },
                {
                    name: "deleted",
                    type: BaseDatabase.TYPES.BOOLEAN,
                },
                {
                    name: "userId",
                    type: BaseDatabase.TYPES.INTEGER,
                    isNullable: true
                },
                {
                    name: "key",
                    type: BaseDatabase.TYPES.STRING,
                    isUnique: true,
                },
                {
                    name: "keywords",
                    type: BaseDatabase.TYPES.TEXT,
                },
                {
                    name: "description",
                    type: BaseDatabase.TYPES.STRING,
                },
                {
                    name: "image",
                    type: (this._isServer() ? BaseDatabase.TYPES.MEDIUMTEXT : BaseDatabase.TYPES.TEXT),
                },
                {
                    name: "imageDescription",
                    type: BaseDatabase.TYPES.STRING,
                    isNullable: true
                }
            ],
            indices: [
                {
                    name: "IDX_definition_userId",
                    columnNames: ["userId"]
                },
            ],
            foreignKeys: [
                {
                    name: "FK_definition_userId",
                    columnNames: ["userId"],
                    referencedTableName: "user",
                    referencedColumnNames: ["id"],
                },
            ]
        });
        return await queryRunner.createTable(definition, true)
    }

    async _addFiles(queryRunner: QueryRunner) {
      console.log("DONE");
        let files = new Table({
            name: "files",
            columns: [
                {
                    name: "id",
                    isPrimary: true,
                    type: BaseDatabase.TYPES.INTEGER,
                    isGenerated: this._isServer(),
                    generationStrategy: "increment" as "increment"
                },
                {
                    name: "createdAt",
                    type: BaseDatabase.TYPES.DATE,
                },
                {
                    name: "updatedAt",
                    type: BaseDatabase.TYPES.DATE,
                },
                {
                    name: "version",
                    type: BaseDatabase.TYPES.INTEGER,
                },
                {
                    name: "deleted",
                    type: BaseDatabase.TYPES.BOOLEAN,
                },
                {
                    name: "userId",
                    type: BaseDatabase.TYPES.INTEGER,
                    isNullable: true
                },
                {
                    name: "key",
                    type: BaseDatabase.TYPES.STRING,
                    isUnique: true,
                },
                {
                    name: "keywords",
                    type: BaseDatabase.TYPES.TEXT,
                },
                {
                    name: "description",
                    type: BaseDatabase.TYPES.STRING,
                },
                {
                    name: "image",
                    type: (this._isServer() ? BaseDatabase.TYPES.MEDIUMTEXT : BaseDatabase.TYPES.TEXT),
                },
                {
                    name: "imageDescription",
                    type: BaseDatabase.TYPES.STRING,
                    isNullable: true
                }
            ],
            indices: [
                {
                    name: "IDX_files_userId",
                    columnNames: ["userId"]
                },
            ],
            foreignKeys: [
                {
                    name: "FK_files_userId",
                    columnNames: ["userId"],
                    referencedTableName: "user",
                    referencedColumnNames: ["id"],
                },
            ]
        });
        return await queryRunner.createTable(files, true)
    }

    async _addExercise(queryRunner: QueryRunner){

        let exercise = new Table({
            name: "exercise",
            columns: [
                {
                    name: "id",
                    isPrimary: true,
                    type: BaseDatabase.TYPES.INTEGER,
                    isGenerated: this._isServer(),
                    generationStrategy: "increment"
                },
                {
                    name: "createdAt",
                    type: BaseDatabase.TYPES.DATE,
                },
                {
                    name: "updatedAt",
                    type: BaseDatabase.TYPES.DATE,
                },
                {
                    name: "version",
                    type: BaseDatabase.TYPES.INTEGER,
                },
                {
                    name: "deleted",
                    type: BaseDatabase.TYPES.BOOLEAN,
                },
                {
                    name: "elementType",
                    type: BaseDatabase.TYPES.STRING,
                },
                {
                    name: "name",
                    type: BaseDatabase.TYPES.STRING,
                },
                {
                    name: "isGenerating",
                    type: BaseDatabase.TYPES.BOOLEAN,
                },
                {
                    name: "generatingData",
                    type: this._isServer()?BaseDatabase.TYPES.MEDIUMTEXT: BaseDatabase.TYPES.TEXT,
                },
                {
                    name: "courseId",
                    type: BaseDatabase.TYPES.INTEGER,
                    isNullable: true
                },
            ],
            indices: [
                {
                    name: "IDX_exercise_courseId",
                    columnNames: ["courseId"]
                },
            ],
            foreignKeys: [
                {
                    name: "FK_exercise_courseId",
                    columnNames: ["courseId"],
                    referencedTableName: "course",
                    referencedColumnNames: ["id"],
                },
            ]
        });
        return await queryRunner.createTable(exercise, true)
    }

    async _addExerciseProgress(queryRunner: QueryRunner) {

        let columns = [
            {
                name: "id",
                isPrimary: true,
                type: BaseDatabase.TYPES.INTEGER,
                isGenerated: this._isServer(),
                generationStrategy: "increment" as "increment"
            },
            {
                name: "createdAt",
                type: BaseDatabase.TYPES.DATE,
            },
            {
                name: "updatedAt",
                type: BaseDatabase.TYPES.DATE,
            },
            {
                name: "version",
                type: BaseDatabase.TYPES.INTEGER,
            },
            {
                name: "deleted",
                type: BaseDatabase.TYPES.BOOLEAN,
            },
            {
                name: "userId",
                type: BaseDatabase.TYPES.INTEGER,
                isNullable: true
            },
            {
                name: "state",
                type: (this._isServer() ? BaseDatabase.TYPES.MEDIUMTEXT : BaseDatabase.TYPES.TEXT),
            },
            {
                name: "timeNeeded",
                type: BaseDatabase.TYPES.INTEGER
            },
            {
                name: "runNumber",
                type: BaseDatabase.TYPES.INTEGER,
            },
            {
                name: "data",
                type: (this._isServer() ? BaseDatabase.TYPES.MEDIUMTEXT : BaseDatabase.TYPES.TEXT),
                isNullable: true
            },
            {
                name: "isDone",
                type: BaseDatabase.TYPES.BOOLEAN,
            },
            {
                name: "elementId",
                type: BaseDatabase.TYPES.INTEGER,
                isNullable: true
            },
        ];

        let definition = new Table({
            name: "exercise_progress",
            columns: columns,
            indices: [
                {
                    name: "IDX_exercise_progress_userId",
                    columnNames: ["userId"]
                },
                {
                    name: "IDX_exercise_progress_elementId",
                    columnNames: ["elementId"]
                },
            ],
            foreignKeys: [
                {
                    name: "FK_exercise_progress_userId",
                    columnNames: ["userId"],
                    referencedTableName: "user",
                    referencedColumnNames: ["id"],
                },
                {
                    name: "FK_exercise_progress_elementId",
                    columnNames: ["elementId"],
                    referencedTableName: "exercise",
                    referencedColumnNames: ["id"],
                },
            ]
        });
        return await queryRunner.createTable(definition, true)
    }

    async _addWrongAnswer(queryRunner: QueryRunner) {

        // let table = MigrationHelper.createTableFromModelClass(WrongAnswer);
        // await queryRunner.createTable(table);

        let wrongAnswer = new Table({
            name: "wrong_answer",
            columns: [
                {
                    name: "id",
                    isPrimary: true,
                    type: BaseDatabase.TYPES.INTEGER,
                    isGenerated: this._isServer(),
                    generationStrategy: "increment"
                },
                {
                    name: "createdAt",
                    type: BaseDatabase.TYPES.DATE,
                },
                {
                    name: "updatedAt",
                    type: BaseDatabase.TYPES.DATE,
                },
                {
                    name: "version",
                    type: BaseDatabase.TYPES.INTEGER,
                },
                {
                    name: "deleted",
                    type: BaseDatabase.TYPES.BOOLEAN,
                },
                {
                    name: "userId",
                    type: BaseDatabase.TYPES.INTEGER,
                    isNullable: true
                },
                {
                    name: "field",
                    type: BaseDatabase.TYPES.STRING,
                },
                {
                    name: "given",
                    type: BaseDatabase.TYPES.STRING,
                    isNullable: true,
                },
                {
                    name: "expected",
                    type: BaseDatabase.TYPES.STRING,
                },
                {
                    name: "occurredAt",
                    type: BaseDatabase.TYPES.DATE
                },
                {
                    name: "exerciseProgressId",
                    type: BaseDatabase.TYPES.INTEGER,
                    isNullable: true
                },
            ],
            indices: [
                {
                    name: "IDX_wrong_answer_userId",
                    columnNames: ["userId"]
                },
                {
                    name: "IDX_wrong_answer_exerciseProgressId",
                    columnNames: ["exerciseProgressId"]
                },
            ],
            foreignKeys: [
                {
                    name: "FK_wrong_answer_userId",
                    columnNames: ["userId"],
                    referencedTableName: "user",
                    referencedColumnNames: ["id"],
                },
                {
                    name: "FK_wrong_answer_exerciseProgressId",
                    columnNames: ["exerciseProgressId"],
                    referencedTableName: "exercise_progress",
                    referencedColumnNames: ["id"],
                },
            ]
        });
        return await queryRunner.createTable(wrongAnswer, true)
    }

    down(queryRunner: QueryRunner): Promise<any> {
        return undefined;
    }
}
