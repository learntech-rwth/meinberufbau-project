import {MigrationInterface, QueryRunner, Table, TableColumn} from "typeorm";
import {BaseDatabase} from "cordova-sites-database/dist/BaseDatabase";
import {MigrationHelper} from "js-helper/dist/shared/MigrationHelper";
import {Exercise} from "../../Exercise";

export class AddStageToExercises1000000017000 implements MigrationInterface {

    _isServer(): boolean {
        return (typeof document !== "object")
    }

    async up(queryRunner: QueryRunner): Promise<any> {
        await this.addStagesToExercises(queryRunner);
    }

    async addStagesToExercises(queryRunner: QueryRunner) {
        await queryRunner.addColumn("exercise", new TableColumn({
            "name":"stage",
            "type": BaseDatabase.TYPES.INTEGER,
            "default": 1,
        }));
    }

    down(queryRunner: QueryRunner): Promise<any> {
        return undefined;
    }
}
