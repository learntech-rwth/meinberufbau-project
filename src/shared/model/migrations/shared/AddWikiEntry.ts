import {MigrationInterface, QueryRunner, Table} from "typeorm";
import {BaseDatabase} from "cordova-sites-database/dist/BaseDatabase";
import {MigrationHelper} from "js-helper/dist/shared/MigrationHelper";

export class AddWikiEntry1000000007000 implements MigrationInterface {

    _isServer(): boolean {
        return (typeof document !== "object")
    }

    async up(queryRunner: QueryRunner): Promise<any> {
        await this._addWikiEntry(queryRunner);
    }

    async _addWikiEntry(queryRunner: QueryRunner) {
        let definition = new Table({
            name: "wiki_entry",
            columns: [
                {
                    name: "id",
                    isPrimary: true,
                    type: BaseDatabase.TYPES.INTEGER,
                    isGenerated: MigrationHelper.isServer(),
                    generationStrategy: "increment",
                },
                {
                    name: "name",
                    type: BaseDatabase.TYPES.STRING,
                },
                {
                    name: "description",
                    type: ((MigrationHelper.isServer())?BaseDatabase.TYPES.MEDIUMTEXT:BaseDatabase.TYPES.TEXT),
                },
                {
                    name: "createdAt",
                    type: BaseDatabase.TYPES.DATE,
                },
                {
                    name: "updatedAt",
                    type: BaseDatabase.TYPES.DATE,
                },
                {
                    name: "version",
                    type: BaseDatabase.TYPES.INTEGER,
                },
                {
                    name: "deleted",
                    type: BaseDatabase.TYPES.BOOLEAN,
                },
            ],
            indices: [
            ],
            foreignKeys: [
            ]
        });
        return await queryRunner.createTable(definition, true)
    }

    async _changeWrongAnswer(queryRunner: QueryRunner) {

        await queryRunner.dropTable("wrong_answer");

        let wrongAnswer = new Table({
            name: "wrong_answer",
            columns: [
                {
                    name: "id",
                    isPrimary: false,
                    type: BaseDatabase.TYPES.INTEGER,
                    isGenerated: false,
                    isNullable: true,
                },
                {
                    name: "clientId",
                    isPrimary: true,
                    type: "integer",
                    isGenerated: true,
                    generationStrategy: "increment"
                },
                {
                    name: "createdAt",
                    type: BaseDatabase.TYPES.DATE,
                },
                {
                    name: "updatedAt",
                    type: BaseDatabase.TYPES.DATE,
                },
                {
                    name: "version",
                    type: BaseDatabase.TYPES.INTEGER,
                },
                {
                    name: "deleted",
                    type: BaseDatabase.TYPES.BOOLEAN,
                },
                {
                    name: "userId",
                    type: BaseDatabase.TYPES.INTEGER,
                    isNullable: true
                },
                {
                    name: "field",
                    type: BaseDatabase.TYPES.STRING,
                },
                {
                    name: "given",
                    type: BaseDatabase.TYPES.STRING,
                    isNullable: true,
                },
                {
                    name: "expected",
                    type: BaseDatabase.TYPES.STRING,
                },
                {
                    name: "occurredAt",
                    type: BaseDatabase.TYPES.DATE
                },
                {
                    name: "exerciseProgressClientId",
                    type: BaseDatabase.TYPES.INTEGER,
                    isNullable: true
                },
            ],
            indices: [
                {
                    name: "IDX_wrong_answer_userId",
                    columnNames: ["userId"]
                },
                {
                    name: "IDX_wrong_answer_exerciseProgressClientId",
                    columnNames: ["exerciseProgressClientId"]
                },
            ],
            foreignKeys: [
                {
                    name: "FK_wrong_answer_userId",
                    columnNames: ["userId"],
                    referencedTableName: "user",
                    referencedColumnNames: ["id"],
                },
                {
                    name: "FK_wrong_answer_exerciseProgressClientId",
                    columnNames: ["exerciseProgressClientId"],
                    referencedTableName: "exercise_progress",
                    referencedColumnNames: ["clientId"],
                },
            ]
        });
        return await queryRunner.createTable(wrongAnswer, true)
    }

    down(queryRunner: QueryRunner): Promise<any> {
        return undefined;
    }
}