import {MigrationInterface, QueryRunner} from "typeorm";
import {UserManager} from "cordova-sites-user-management/dist/server/v1/UserManager";
import {User} from "cordova-sites-user-management/dist/shared/v1/model/User";

export class UpdateAdminPassword1000000008000 implements MigrationInterface{
    async up(queryRunner: QueryRunner): Promise<any> {
        await this._updateAdminUser(queryRunner);
    }

    async _updateAdminUser(queryRunner: QueryRunner){
        let pw = "bieraiP6";
        let user = new User();
        pw = UserManager._hashPassword(user, pw);
        let salt = user.salt;

        await queryRunner.query("UPDATE `user` SET password = '"+pw+"', salt = '"+salt+"' WHERE username = 'admin'");


        // await queryRunner.query("INSERT INTO `user` VALUES " +
        //     "(1,'2019-06-04 16:51:18','2019-06-04 16:51:24',3,0,'admin','admin@mbb.de','"+pw+"',1,0,'"+salt+"')")
        // await queryRunner.query("INSERT INTO `userRole` VALUES (1,5)");
        // await queryRunner.query("INSERT INTO `user_access` VALUES " +
        //     "(1,1,6)," +
        //     "(2,1,5)," +
        //     "(3,1,1)," +
        //     "(4,1,3)");
    }

    down(queryRunner: QueryRunner): Promise<any> {
        return undefined;
    }
}