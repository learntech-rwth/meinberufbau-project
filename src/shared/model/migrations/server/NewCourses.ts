import {MigrationInterface, QueryRunner} from "typeorm";

export class NewCourses1000000016000 implements MigrationInterface {
    down(queryRunner: QueryRunner): Promise<any> {
        return Promise.resolve(undefined);
    }

    async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("INSERT INTO `course` VALUES " +
            "(8,now(),now(),1,0,'Weitere Tiefbaubereiche',0,0,'tiefbau')," +
            "(9,now(),now(),1,0,'Weitere Hochbaubereiche',0,0,'hochbau')," +
            "(10,now(),now(),1,0,'Fliesen-, Platten-, Mosaikbau',0,0,'fliesen')," +
            "(11,now(),now(),1,0,'Stuckateurarbeiten',0,0,'stukkateur')," +
            "(12,now(),now(),1,0,'Zimmerarbeiten',0,0,'zimmer')," +
            "(13,now(),now(),1,0,'Weitere Ausbaubereiche',0,0,'ausbau');"
        )
    }
}
