import {MigrationInterface, QueryRunner} from "typeorm";

export class DeleteData0900000000000 implements MigrationInterface{
    async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.dropTable("wrong_answer", true);
        await queryRunner.dropTable("exercise_progress", true);
        await queryRunner.dropTable("definition", true);
        await queryRunner.dropTable("exercise", true);
        await queryRunner.dropTable("course", true);
    }

    down(queryRunner: QueryRunner): Promise<any> {
        return undefined;
    }
}