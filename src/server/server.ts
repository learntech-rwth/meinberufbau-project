import "dotenv/config";
import {EasySyncServerDb} from "cordova-sites-easy-sync/dist/server";

import * as path from "path";

import * as express from 'express';
import {routes} from './routes';

//Importiere Models. Muss getan werden, da Models sich selbstständig bei Datenbank registrieren
import "cordova-sites-user-management/dist/shared";
import "../shared/model/Exercise"
import "../shared/model/Course"
import "../shared/model/Definition"
import "../shared/model/WrongAnswer"
import "../shared/model/ExerciseProgress"
import "../shared/model/WikiEntry"

//Andere Imports
import {UserManager} from "cordova-sites-user-management/dist/server";
import {SetupUserManagement1000000001000} from "cordova-sites-user-management/dist/shared"
import {DeleteUserManagement1000000000000} from "cordova-sites-user-management/dist/shared"
import {Setup1000000002000} from "../shared/model/migrations/shared/Setup";
import {Data1000000005000} from "../shared/model/migrations/server/Data";
import {DeleteData0900000000000} from "../shared/model/migrations/server/DeleteData";
import {DefinitionImageNullable1000000005000} from "../shared/model/migrations/shared/DefinitionImageNullable";
import {AddWikiEntry1000000007000} from "../shared/model/migrations/shared/AddWikiEntry";
import {UpdateAdminPassword1000000008000} from "../shared/model/migrations/server/UpdateAdminPassword";
import {AddFileMediumMigration1000000011000} from "cordova-sites-easy-sync/dist/shared"
import {ServerFileMedium} from "cordova-sites-easy-sync/dist/server";
import {DefinitionImageAsFileMedium1000000013000} from "../shared/model/migrations/shared/DefinitionImageAsFileMedium";
import {DefinitionSimpleGerman1000000014000} from "../shared/model/migrations/shared/DefinitionSimpleGerman";
import {DefinitionLongerText1000000015000} from "../shared/model/migrations/shared/DefinitionLongerText";
import {NewCourses1000000016000} from "../shared/model/migrations/server/NewCourses";
import {AddStageToExercises1000000017000} from "../shared/model/migrations/shared/AddStageToExercises";
import {AddImageAndDescription1000000018000} from "../shared/model/migrations/shared/AddImageAndDescription";

const port = process.env.PORT || 3000;
process.env.JWT_SECRET = process.env.JWT_SECRET || "mySecretioöqwe78034hjiodfu80ä^";

//Ordner (und Namenspräfix) für Bilder, Videos, Sonstiges, was per FileMedium hochgeladen wird
ServerFileMedium.SAVE_PATH = __dirname+"/uploads/img_";

//Setzen von Datenbank-Optionen
EasySyncServerDb.CONNECTION_PARAMETERS = {
    "type": "mysql",
    "host": process.env.MYSQL_HOST || "localhost",
    "port": process.env.MYSQL_PORT || "3306",
    "username": process.env.MYSQL_USER || "root",
    "password": process.env.MYSQL_PASSWORD || "",
    "database": process.env.MYSQL_DATABASE || "mbb",
    "synchronize": false, //Nicht automatisches Synchronisieren, sondern per Migrations. Kann sonst zu Datenverlust führen!
    "migrationsRun": true,
    "migrations": [
        DeleteData0900000000000,
        DeleteUserManagement1000000000000,
        SetupUserManagement1000000001000,
        Setup1000000002000,
        Data1000000005000,
        DefinitionImageNullable1000000005000,
        AddWikiEntry1000000007000,
        UpdateAdminPassword1000000008000,
        AddFileMediumMigration1000000011000,
        DefinitionImageAsFileMedium1000000013000,
        DefinitionSimpleGerman1000000014000,
        DefinitionLongerText1000000015000,
        NewCourses1000000016000,
        AddStageToExercises1000000017000,
        AddImageAndDescription1000000018000,
    ],
    "logging": false,
};

UserManager.PEPPER = process.env.PEPPER || "mySecretPepper";
UserManager.REGISTRATION_IS_ACTIVATED = true;
UserManager.REGISTRATION_DEFAULT_ROLE_IDS = [4]; //Ist die Rolle von einem normalen User

//Erstelle Express-App
const app = express();
app.use(express.json({limit: "50mb"}));

//Allow Origin setzen bevor rest passiert
app.use(function (req, res, next) {

    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', '*');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type,Authorization');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', "true");

    // Pass to next layer of middleware
    next();
});

app.use('/api', routes);

app.use(express.static(path.resolve(path.dirname(process.argv[1]), "public")));
app.use("/uploads", express.static(path.resolve(path.dirname(process.argv[1]), "uploads")));

//Handle errors
app.use(function (err, req, res, next) {
    console.error(err);
    res.status(err.status || 500);
    if (err instanceof Error) {
        res.json({error: err.message});
    } else {
        res.json({error: err});
    }
});

EasySyncServerDb.getInstance()._connectionPromise.then(async () => {
    app.listen(port, () => {
        console.log('Server started on Port: ' + port);
    });
});
