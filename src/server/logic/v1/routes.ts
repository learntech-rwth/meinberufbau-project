import * as express from 'express';
import {userRoutes, syncRoutes, UserManager} from "cordova-sites-user-management/dist/server";
import {DefinitionController} from "./controller/DefinitionController";
import {Definition} from "../../../shared/model/Definition";
import {StatisticController} from "./controller/StatisticController";
import {UserController} from "./controller/UserController";

const routerV1 = express.Router();

const errorHandler = (fn, context) => {
    return (req, res, next) => {
        const resPromise = fn.call(context, req,res,next);
        if (resPromise && resPromise.catch){
            resPromise.catch(err => next(err));
        }
    }
};

//Hinzufügen der Default-Routes für synchronisation und user
routerV1.use("/sync", syncRoutes);
routerV1.use("/user", userRoutes);

//Hinzufügen vom Speichern von Definition
routerV1.post(Definition.SAVE_PATH, errorHandler(UserManager.setUserFromToken, UserManager), errorHandler(DefinitionController.modifyModel, DefinitionController));

//Hinzufügen zur Anzeige von Statistik. Aktuell ist zugehörige Seite deaktiviert. Lässt nur Anfragen zu, wenn User das Admin-Recht hat
routerV1.get("/statistics", errorHandler(UserManager.checkAccess("admin"), UserManager), errorHandler(StatisticController.getDefaultStatistic, StatisticController));

//Hinzufügen von Auflistung von Users
routerV1.get("/listUsers", errorHandler(UserManager.needToken, UserManager), errorHandler(UserController.listUsers, UserController));

export {routerV1};
