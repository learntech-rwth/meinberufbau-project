import {User} from "cordova-sites-user-management/dist/shared/v1/model/User";
import {Helper} from "js-helper";
import {Like} from "typeorm";

export class UserController {

    //Rechteüberprüfung bereits in der Route geschene
    static async listUsers(req, res){
        let userSearchName = Helper.nonNull(req.query.username, "");

        let users = await User.find({username: Like("%"+userSearchName+"%")});

        //Verwandelt User-Daten in JSON. Dank .toJSON()-Funktion von User wird nur ID und Username übertragen
        res.json({
            success: true,
            data: users
        });
    }
}
